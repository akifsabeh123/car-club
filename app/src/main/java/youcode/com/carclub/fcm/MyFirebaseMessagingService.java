package youcode.com.carclub.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.SharedPrefsUtils;
import youcode.com.carclub.ui.splash.SplashScreen;

/**
 * Created by Desk1 on 8/24/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sendNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"));

    }


    private void sendNotification(String title, String messageBody) {
        Intent intent = new Intent(this, SplashScreen.class);
        intent.putExtra("notification", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, "PORSCHE", NotificationManager.IMPORTANCE_HIGH);

            notificationChannel.setDescription("Porsche Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            notificationChannel.setSound(defaultSoundUri, audioAttributes);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(R.drawable.pc_logo)
                        .setPriority(NotificationCompat.PRIORITY_MAX);

       /* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            notificationBuilder.setColor(getResources().getColor(R.color.red));
        } else
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);*/
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

        SharedPrefsUtils.setBooleanPreference(this, Constants.NOTIFICATION_AVAILABLE, true);

        Intent broadcast = new Intent("new-notification");
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcast);
    }

}
