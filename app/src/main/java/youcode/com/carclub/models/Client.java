package youcode.com.carclub.models;

/**
 * Created by Akif sabeh on 5/22/2019.
 */
public class Client {
    private String code;
    private String phone;
    private long date;
    private String amountPaid;

    public Client() {
    }

    public Client(String code, String phone, long date, String amountPaid) {
        this.code = code;
        this.phone = phone;
        this.date = date;
        this.amountPaid = amountPaid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }
}
