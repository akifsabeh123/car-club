package youcode.com.carclub.models;

/**
 * Created by Akif
 * Since  10/24/2017
 * Company : Youcode
 */

public class EventPurchaseUser {
    private String name;
    private String id;

    public EventPurchaseUser() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
