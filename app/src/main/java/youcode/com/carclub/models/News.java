package youcode.com.carclub.models;

/**
 * Created by Desk1 on 8/22/2017.
 */

public class News {

    private String newsId;
    private String imgUrl;
    private String address;
    private String title;
    private String description;

    public News() {
    }

    public News(String newsId, String imgUrl, String address, String title, String description) {
        this.newsId = newsId;
        this.imgUrl = imgUrl;
        this.address = address;
        this.title = title;
        this.description = description;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
