package youcode.com.carclub.models;

/**
 * Created by Akif sabeh on 5/20/2019.
 */
public class UserSubscription {
    private long date;
    private String phone;

    public UserSubscription() {
    }

    public UserSubscription(long date, String phone) {
        this.date = date;
        this.phone = phone;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
