package youcode.com.carclub.models;

/**
 * Created by Desk1 on 6/16/2017.
 */

public class SpinnerData {
    private String color;
    private String text;

    public SpinnerData(String text, String color) {
        this.text = text;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
