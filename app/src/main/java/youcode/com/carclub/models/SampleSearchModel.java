package youcode.com.carclub.models;

import ir.mirrajabi.searchdialog.core.Searchable;

/**
 * Created by Akif sabeh on 5/21/2019.
 */
public class SampleSearchModel implements Searchable {
    private String mTitle;
    private String id;

    public SampleSearchModel(String title,String id) {
        mTitle = title;
        this.id = id;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    public SampleSearchModel setTitle(String title) {
        mTitle = title;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}