package youcode.com.carclub.models;

/**
 * Created by Desk1 on 8/22/2017.
 */

public class Product {

    private String productId;
    private String imgUrl;
    private long price;
    private String address;
    private String title;
    private String description;

    public Product() {
    }

    public Product(String productId, String imgUrl, long price, String address, String title, String description) {
        this.productId = productId;
        this.imgUrl = imgUrl;
        this.price = price;
        this.address = address;
        this.title = title;
        this.description = description;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
