package youcode.com.carclub.models;

import java.util.List;

/**
 * Created by Akif sabeh on 1/24/2019.
 */
public class EventPayments {
    private Event event;
    private List<EventTicket> eventTickets;

    public EventPayments() {
    }

    public EventPayments(Event event, List<EventTicket> eventTickets) {
        this.event = event;
        this.eventTickets = eventTickets;
    }


    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public List<EventTicket> getEventTickets() {
        return eventTickets;
    }

    public void setEventTickets(List<EventTicket> eventTickets) {
        this.eventTickets = eventTickets;
    }

}
