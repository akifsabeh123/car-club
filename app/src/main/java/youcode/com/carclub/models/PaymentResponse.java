package youcode.com.carclub.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentResponse implements Parcelable {


    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("customer_email")
    private String customer_email;
    @Expose
    @SerializedName("authorization_code")
    private String authorization_code;
    @Expose
    @SerializedName("merchant_reference")
    private String merchant_reference;
    @Expose
    @SerializedName("response_message")
    private String response_message;
    @Expose
    @SerializedName("command")
    private String command;
    @Expose
    @SerializedName("fort_id")
    private String fort_id;
    @Expose
    @SerializedName("eci")
    private String eci;
    @Expose
    @SerializedName("language")
    private String language;
    @Expose
    @SerializedName("customer_ip")
    private String customer_ip;
    @Expose
    @SerializedName("expiry_date")
    private String expiry_date;
    @Expose
    @SerializedName("payment_option")
    private String payment_option;
    @Expose
    @SerializedName("access_code")
    private String access_code;
    @Expose
    @SerializedName("merchant_identifier")
    private String merchant_identifier;
    @Expose
    @SerializedName("signature")
    private String signature;
    @Expose
    @SerializedName("card_holder_name")
    private String card_holder_name;
    @Expose
    @SerializedName("card_number")
    private String card_number;
    @Expose
    @SerializedName("response_code")
    private String response_code;
    @Expose
    @SerializedName("amount")
    private String amount;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getAuthorization_code() {
        return authorization_code;
    }

    public void setAuthorization_code(String authorization_code) {
        this.authorization_code = authorization_code;
    }

    public String getMerchant_reference() {
        return merchant_reference;
    }

    public void setMerchant_reference(String merchant_reference) {
        this.merchant_reference = merchant_reference;
    }

    public String getResponse_message() {
        return response_message;
    }

    public void setResponse_message(String response_message) {
        this.response_message = response_message;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getFort_id() {
        return fort_id;
    }

    public void setFort_id(String fort_id) {
        this.fort_id = fort_id;
    }

    public String getEci() {
        return eci;
    }

    public void setEci(String eci) {
        this.eci = eci;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCustomer_ip() {
        return customer_ip;
    }

    public void setCustomer_ip(String customer_ip) {
        this.customer_ip = customer_ip;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getPayment_option() {
        return payment_option;
    }

    public void setPayment_option(String payment_option) {
        this.payment_option = payment_option;
    }

    public String getAccess_code() {
        return access_code;
    }

    public void setAccess_code(String access_code) {
        this.access_code = access_code;
    }

    public String getMerchant_identifier() {
        return merchant_identifier;
    }

    public void setMerchant_identifier(String merchant_identifier) {
        this.merchant_identifier = merchant_identifier;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getCard_holder_name() {
        return card_holder_name;
    }

    public void setCard_holder_name(String card_holder_name) {
        this.card_holder_name = card_holder_name;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getResponse_code() {
        return response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeString(this.currency);
        dest.writeString(this.customer_email);
        dest.writeString(this.authorization_code);
        dest.writeString(this.merchant_reference);
        dest.writeString(this.response_message);
        dest.writeString(this.command);
        dest.writeString(this.fort_id);
        dest.writeString(this.eci);
        dest.writeString(this.language);
        dest.writeString(this.customer_ip);
        dest.writeString(this.expiry_date);
        dest.writeString(this.payment_option);
        dest.writeString(this.access_code);
        dest.writeString(this.merchant_identifier);
        dest.writeString(this.signature);
        dest.writeString(this.card_holder_name);
        dest.writeString(this.card_number);
        dest.writeString(this.response_code);
        dest.writeString(this.amount);
    }

    public PaymentResponse() {
    }

    protected PaymentResponse(Parcel in) {
        this.status = in.readString();
        this.currency = in.readString();
        this.customer_email = in.readString();
        this.authorization_code = in.readString();
        this.merchant_reference = in.readString();
        this.response_message = in.readString();
        this.command = in.readString();
        this.fort_id = in.readString();
        this.eci = in.readString();
        this.language = in.readString();
        this.customer_ip = in.readString();
        this.expiry_date = in.readString();
        this.payment_option = in.readString();
        this.access_code = in.readString();
        this.merchant_identifier = in.readString();
        this.signature = in.readString();
        this.card_holder_name = in.readString();
        this.card_number = in.readString();
        this.response_code = in.readString();
        this.amount = in.readString();
    }

    public static final Parcelable.Creator<PaymentResponse> CREATOR = new Parcelable.Creator<PaymentResponse>() {
        @Override
        public PaymentResponse createFromParcel(Parcel source) {
            return new PaymentResponse(source);
        }

        @Override
        public PaymentResponse[] newArray(int size) {
            return new PaymentResponse[size];
        }
    };
}
