package youcode.com.carclub.models;

import java.io.Serializable;

/**
 * Created by Desk1 on 9/7/2017.
 */

public class EventTicket implements Serializable {
    private Event event;
    private int pax;
    private int childPax;
    private int nonMemberPax;
    private int count;
    private long price;
    private boolean status;
    private String transactionNumber;
    private String userId;
    private String username;
    private String car;
    private User user;
    private long purchaseDate;
    private boolean cancelled;
    private String id;
    private String eventId;

    public EventTicket() {
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public int getPax() {
        return pax;
    }

    public void setPax(int pax) {
        this.pax = pax;
    }

    public int getChildPax() {
        return childPax;
    }

    public void setChildPax(int childPax) {
        this.childPax = childPax;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getNonMemberPax() {
        return nonMemberPax;
    }

    public void setNonMemberPax(int nonMemberPax) {
        this.nonMemberPax = nonMemberPax;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(long purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
