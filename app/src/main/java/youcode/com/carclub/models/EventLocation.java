package youcode.com.carclub.models;


import java.io.Serializable;

/**
 * Created by user on 8/23/2017.
 */

public class EventLocation implements Serializable{
    private String address;
    private LatLng latLng;
    private String destination;

    public EventLocation() {
    }

    public EventLocation(String address, LatLng latLng) {
        this.address = address;
        this.latLng = latLng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}