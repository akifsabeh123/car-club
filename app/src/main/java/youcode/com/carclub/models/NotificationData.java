package youcode.com.carclub.models;

/**
 * Created by Desk1 on 8/24/2017.
 */

public class NotificationData {
    String to;
    String priority;

    public NotificationData(String to, String priority) {
        this.to = to;
        this.priority = priority;
    }

    public NotificationData() {
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
