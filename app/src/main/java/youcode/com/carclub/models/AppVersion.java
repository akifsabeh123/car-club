package youcode.com.carclub.models;

/**
 * Created by Akif sabeh on 12/12/2018.
 */
public class AppVersion {

    private String version;
    private String url;
    private int code;

    public AppVersion() {
    }

    public AppVersion(String version, String url) {
        this.version = version;
        this.url = url;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
