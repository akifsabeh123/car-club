package youcode.com.carclub.models;

import java.util.List;

/**
 * Created by Akif
 * Since  10/24/2017
 * Company : Youcode
 */

public class EventPurchases {
    private String name;
    private List<User> purchaseUserList;

    public EventPurchases() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getPurchaseUserList() {
        return purchaseUserList;
    }

    public void setPurchaseUserList(List<User> purchaseUserList) {
        this.purchaseUserList = purchaseUserList;
    }

    @Override
    public String toString() {
        return "EventPurchases{" +
                "name='" + name + '\'' +
                ", purchaseUserList=" + purchaseUserList +
                '}';
    }
}
