package youcode.com.carclub.models;

/**
 * Created by user on 8/22/2017.
 */

public class PlaceLocation {
    private String name;
    private String address;
    private long longitude;
    private long latitude;

    public PlaceLocation() {
    }

    public PlaceLocation(String name, String address, long longitude, long latitude) {
        this.name = name;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getLongitude() {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }

    public long getLatitude() {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }
}
