package youcode.com.carclub.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Desk1 on 8/22/2017.
 */

public class Event implements  Parcelable {
    private String eventId;
    private long invertedDate;
    private String title;
    private String description;
    private String imgUrl;
    private String price;
    private String priceNonMember;
    private String childPrice;
    private String transactionNumber;
    private int type;
    private int eventType;
    private boolean status;
    private From from;
    private To to;
    private long confirmation;
    private String route;
    private long date;
    private String menu;
    private int invitationType;

    public Event() {
        setTo(new To());
        setFrom(new From());
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public long getInvertedDate() {
        return invertedDate;
    }

    public void setInvertedDate(long invertedDate) {
        this.invertedDate = invertedDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceNonMember() {
        return priceNonMember;
    }

    public void setPriceNonMember(String priceNonMember) {
        this.priceNonMember = priceNonMember;
    }

    public String getChildPrice() {
        return childPrice;
    }

    public void setChildPrice(String childPrice) {
        this.childPrice = childPrice;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public long getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(long confirmation) {
        this.confirmation = confirmation;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public int getInvitationType() {
        return invitationType;
    }

    public void setInvitationType(int invitationType) {
        this.invitationType = invitationType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.eventId);
        dest.writeLong(this.invertedDate);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.imgUrl);
        dest.writeString(this.price);
        dest.writeString(this.priceNonMember);
        dest.writeString(this.childPrice);
        dest.writeString(this.transactionNumber);
        dest.writeInt(this.type);
        dest.writeInt(this.eventType);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.from, flags);
        dest.writeParcelable(this.to, flags);
        dest.writeLong(this.confirmation);
        dest.writeString(this.route);
        dest.writeLong(this.date);
        dest.writeString(this.menu);
        dest.writeInt(this.invitationType);
    }

    protected Event(Parcel in) {
        this.eventId = in.readString();
        this.invertedDate = in.readLong();
        this.title = in.readString();
        this.description = in.readString();
        this.imgUrl = in.readString();
        this.price = in.readString();
        this.priceNonMember = in.readString();
        this.childPrice = in.readString();
        this.transactionNumber = in.readString();
        this.type = in.readInt();
        this.eventType = in.readInt();
        this.status = in.readByte() != 0;
        this.from = in.readParcelable(From.class.getClassLoader());
        this.to = in.readParcelable(To.class.getClassLoader());
        this.confirmation = in.readLong();
        this.route = in.readString();
        this.date = in.readLong();
        this.menu = in.readString();
        this.invitationType = in.readInt();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}
