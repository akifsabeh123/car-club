package youcode.com.carclub.models;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

/**
 * Created by Desk1 on 8/21/2017.
 */

public class User implements Serializable {

    private String name;
    private String familyName;
    private String fullName;
    private String email;
    private String mobile;
    private String username;
    @Exclude
    private String password;
    private String carName;
    private String carModel;
    private String carColor;
    private boolean autoRenew;
    private String token;
    private boolean admin;
    private String id;
    private String porscheCode;
    private SubscriptionStatus subscriptionStatus;

    public User() {
    }

    public User(String name, String familyName, String fullName, String email, String mobile, String username, String password, String carName, String carModel, String carColor, boolean autoRenew, String token, boolean admin, String id, String porscheCode, SubscriptionStatus subscriptionStatus) {
        this.name = name;
        this.familyName = familyName;
        this.fullName = fullName;
        this.email = email;
        this.mobile = mobile;
        this.username = username;
        this.password = password;
        this.carName = carName;
        this.carModel = carModel;
        this.carColor = carColor;
        this.autoRenew = autoRenew;
        this.token = token;
        this.admin = admin;
        this.id = id;
        this.porscheCode = porscheCode;
        this.subscriptionStatus = subscriptionStatus;
    }

    public User(String name, String familyName, String email, String mobile, String username, String password, String carName, String carModel, String carColor, boolean autoRenew) {
        this.name = name;
        this.familyName = familyName;
        this.email = email;
        this.mobile = mobile;
        this.username = username;
        this.password = password;
        this.carName = carName;
        this.carModel = carModel;
        this.carColor = carColor;
        this.autoRenew = autoRenew;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public boolean isAutoRenew() {
        return autoRenew;
    }

    public void setAutoRenew(boolean autoRenew) {
        this.autoRenew = autoRenew;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPorscheCode() {
        return porscheCode;
    }

    public void setPorscheCode(String porscheCode) {
        this.porscheCode = porscheCode;
    }

    public SubscriptionStatus getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(SubscriptionStatus subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
