package youcode.com.carclub.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by Akif sabeh on 6/19/2019.
 */
public class From implements Parcelable {
    private String meetingPoint;
    private long meetingTime;
    private double lat;
    private double lng;

    public From() {
    }

    public From(String meetingPoint, long meetingTime, double lat, double lng) {
        this.meetingPoint = meetingPoint;
        this.meetingTime = meetingTime;
        this.lat = lat;
        this.lng = lng;
    }

    public String getMeetingPoint() {
        return meetingPoint;
    }

    public void setMeetingPoint(String meetingPoint) {
        this.meetingPoint = meetingPoint;
    }

    public long getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(long meetingTime) {
        this.meetingTime = meetingTime;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Exclude
    public boolean isLocationEmpty() {
        return lat == 0 || lng == 0;
    }
    @Exclude
    public boolean isMeetingPointEmpty() {
        return meetingPoint.trim().isEmpty();
    }
    @Exclude
    public boolean isMeetingTimeEmpty() {
        return meetingTime == 0;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.meetingPoint);
        dest.writeLong(this.meetingTime);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lng);
    }

    protected From(Parcel in) {
        this.meetingPoint = in.readString();
        this.meetingTime = in.readLong();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
    }

    public static final Parcelable.Creator<From> CREATOR = new Parcelable.Creator<From>() {
        @Override
        public From createFromParcel(Parcel source) {
            return new From(source);
        }

        @Override
        public From[] newArray(int size) {
            return new From[size];
        }
    };
}
