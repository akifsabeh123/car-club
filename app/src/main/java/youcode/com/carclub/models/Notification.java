package youcode.com.carclub.models;

/**
 * Created by Akif sabeh on 6/10/2019.
 */
public class Notification {
    private String id;
    private String title;
    private String message;
    private long date;

    public Notification() {
    }

    public Notification(String id, String title, String message, long date) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
