package youcode.com.carclub.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Akif
 * Since  11/2/2017
 * Company : Youcode
 */

public class FireBaseItem implements Parcelable {
    private String title;
    private int imageId;
    private int type;

    public FireBaseItem(String title, int imageId, int type) {
        this.title = title;
        this.imageId = imageId;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeInt(this.imageId);
        dest.writeInt(this.type);
    }

    protected FireBaseItem(Parcel in) {
        this.title = in.readString();
        this.imageId = in.readInt();
        this.type = in.readInt();
    }

    public static final Parcelable.Creator<FireBaseItem> CREATOR = new Parcelable.Creator<FireBaseItem>() {
        @Override
        public FireBaseItem createFromParcel(Parcel source) {
            return new FireBaseItem(source);
        }

        @Override
        public FireBaseItem[] newArray(int size) {
            return new FireBaseItem[size];
        }
    };
}
