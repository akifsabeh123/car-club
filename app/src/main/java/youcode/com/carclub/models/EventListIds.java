package youcode.com.carclub.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akif
 * Since  10/24/2017
 * Company : Youcode
 */

public class EventListIds {
    private String key;
    private List<String> list = new ArrayList<>();

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
