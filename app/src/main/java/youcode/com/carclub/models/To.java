package youcode.com.carclub.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by Akif sabeh on 6/19/2019.
 */
public class  To implements Parcelable {
    private String departurePoint;
    private long departureTime;
    private double lat;
    private double lng;

    public To(String departurePoint, long departureTime, double lat, double lng) {
        this.departurePoint = departurePoint;
        this.departureTime = departureTime;
        this.lat = lat;
        this.lng = lng;
    }

    public To() {
    }

    public String getDeparturePoint() {
        return departurePoint;
    }

    public void setDeparturePoint(String departurePoint) {
        this.departurePoint = departurePoint;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Exclude
    public boolean isLocationEmpty() {
        return lat == 0 || lng == 0;
    }
    @Exclude
    public boolean isDeparturePointEmpty() {
        return departurePoint.trim().isEmpty();
    }
    @Exclude
    public boolean isDepartureTimeEmpty() {
        return departureTime == 0;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.departurePoint);
        dest.writeLong(this.departureTime);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lng);
    }

    protected To(Parcel in) {
        this.departurePoint = in.readString();
        this.departureTime = in.readLong();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
    }

    public static final Parcelable.Creator<To> CREATOR = new Parcelable.Creator<To>() {
        @Override
        public To createFromParcel(Parcel source) {
            return new To(source);
        }

        @Override
        public To[] newArray(int size) {
            return new To[size];
        }
    };
}
