package youcode.com.carclub.models;

/**
 * Created by Akif sabeh on 11/30/2018.
 */
public class SubscriptionStatus {

    private boolean status;
    private long date;

    public SubscriptionStatus() {
    }

    public SubscriptionStatus(boolean status, long date) {
        this.status = status;
        this.date = date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

}
