package youcode.com.carclub.purchases;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventTicket;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PurchasesFragment.PurchaseFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PurchasesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PurchasesFragment extends Fragment {
    private TextView txt_no_data;
    private SwipeRefreshLayout swipeRefreshLayout;
    private PurchaseFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ValueEventListener valueEventListener;
    private Query query;
  //  private PurchasesRecyclerAdapter<RecyclerView.ViewHolder> eventsRecyclerAdapter;

    private PurchasesAdapter purchasesAdapter;


    public PurchasesFragment() {
        // Required empty public constructor
    }

    public static PurchasesFragment newInstance() {
        PurchasesFragment fragment = new PurchasesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tickets, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout = view.findViewById(R.id.swipe);
        txt_no_data = view.findViewById(R.id.txt_no_data);
        recyclerView = view.findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        purchasesAdapter = new PurchasesAdapter(mListener);
        query = FirebaseDatabase.getInstance().getReference().child(Constants.REF_ITEM_PURCHASES).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).orderByChild("status").equalTo(true);
      //  eventsRecyclerAdapter = new PurchasesRecyclerAdapter<>(EventTicket.class, R.layout.item_purchases, PurchasesRecyclerAdapter.MyViewHolder.class, query, mListener);
        recyclerView.setAdapter(purchasesAdapter);



        swipeRefreshLayout.setOnRefreshListener(() -> {
            getData();
            Completable.timer(1, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        swipeRefreshLayout.setRefreshing(false);
                    });
        });
            getData();

    }

    private void getData() {
        if (valueEventListener != null) {
            query.removeEventListener(valueEventListener);
            purchasesAdapter.clear();
        }

        valueEventListener = query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                purchasesAdapter.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    try {
                        EventTicket eventTicket = snapshot.getValue(EventTicket.class);
                        if (eventTicket != null && eventTicket.isStatus()) {
                            FirebaseDatabase.getInstance().getReference().child(Constants.REF_PRODUCTS)
                                    .child(eventTicket.getEventId())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                Event event = dataSnapshot.getValue(Event.class);
                                                if (event.isStatus()) {
                                                    eventTicket.setEvent(event);
                                                    purchasesAdapter.addItem(eventTicket);
                                                    hideNoData();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                        }
                    } catch (Exception e) {

                    }
                }
                if (!dataSnapshot.exists() || dataSnapshot.getChildrenCount() == 0)
                    showNoData();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PurchaseFragmentInteractionListener) {
            mListener = (PurchaseFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void hideNoData() {
        txt_no_data.setVisibility(View.GONE);
    }

    private void showNoData() {
        txt_no_data.setVisibility(View.VISIBLE);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface PurchaseFragmentInteractionListener {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // query.removeEventListener(valueEventListener);
    }

}
