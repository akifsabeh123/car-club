package youcode.com.carclub.purchases;

/**
 * Created by Desk1 on 6/13/2017.
 */

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventTicket;

public class PurchasesAdapter extends RecyclerView.Adapter<PurchasesAdapter.MyViewHolder> {
    private PurchasesFragment.PurchaseFragmentInteractionListener onFragmentInteractionListenerl;
    private List<EventTicket> eventTickets;


    public PurchasesAdapter(PurchasesFragment.PurchaseFragmentInteractionListener onFragmentInteractionListenerl) {
        this.onFragmentInteractionListenerl = onFragmentInteractionListenerl;
        eventTickets = new ArrayList<>();
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_purchases,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int i) {
        EventTicket event = eventTickets.get(i);
        viewHolder.txt_title.setText(viewHolder.itemView.getContext().getString(R.string.label_event_title, event.getEvent().getTitle()));
        viewHolder.txt_count.setText(viewHolder.itemView.getContext().getString(R.string.label_count, event.getCount()));
        viewHolder.txt_price.setText(viewHolder.itemView.getContext().getString(R.string.label_price_purchased, event.getPrice()));
        Glide.with(viewHolder.itemView.getContext()).load(event.getEvent().getImgUrl()).into(viewHolder.img);

    }

    @Override
    public int getItemCount() {
        return eventTickets.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_title, txt_count, txt_price;
        private ImageView img;

        public MyViewHolder(View view) {
            super(view);
            txt_title = view.findViewById(R.id.txt_title);
            txt_count = view.findViewById(R.id.txt_count);
            txt_price = view.findViewById(R.id.txt_price);
            img = view.findViewById(R.id.img);


        }
    }
    public void addItem(EventTicket eventTicket) {
        this.eventTickets.add(eventTicket);
        notifyDataSetChanged();
    }

    public void clear() {
        this.eventTickets.clear();
        notifyDataSetChanged();
    }

}