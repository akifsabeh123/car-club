package youcode.com.carclub.purchases;

/**
 * Created by Desk1 on 6/13/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;

import youcode.com.carclub.R;
import youcode.com.carclub.models.EventTicket;

public class PurchasesRecyclerAdapter<T extends RecyclerView.ViewHolder> extends FirebaseRecyclerAdapter<EventTicket, PurchasesRecyclerAdapter.MyViewHolder> {
    private PurchasesFragment.PurchaseFragmentInteractionListener  onFragmentInteractionListenerl;

    /**
     * @param modelClass      Firebase will marshall the data at a location into an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list. You will be responsible for populating an
     *                        instance of the corresponding view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                        combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public PurchasesRecyclerAdapter(Class<EventTicket> modelClass, int modelLayout, Class<MyViewHolder> viewHolderClass, Query ref, PurchasesFragment.PurchaseFragmentInteractionListener onFragmentInteractionListenerl) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.onFragmentInteractionListenerl = onFragmentInteractionListenerl;


    }

    @Override
    protected void populateViewHolder(MyViewHolder viewHolder, EventTicket event, int position) {
        viewHolder.txt_title.setText(viewHolder.itemView.getContext().getString(R.string.label_event_title, event.getEvent().getTitle()));
        viewHolder.txt_count.setText(viewHolder.itemView.getContext().getString(R.string.label_count, event.getCount()));
        viewHolder.txt_price.setText(viewHolder.itemView.getContext().getString(R.string.label_price_purchased, event.getPrice()));
        Glide.with(viewHolder.itemView.getContext()).load(event.getEvent().getImgUrl()).into(viewHolder.img);


    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_title, txt_count, txt_price;
        private ImageView img;

        public MyViewHolder(View view) {
            super(view);
            txt_title = view.findViewById(R.id.txt_title);
            txt_count = view.findViewById(R.id.txt_count);
            txt_price = view.findViewById(R.id.txt_price);
            img = view.findViewById(R.id.img);


        }
    }

}