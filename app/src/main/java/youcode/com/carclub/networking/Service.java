package youcode.com.carclub.networking;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;
import youcode.com.carclub.BuildConfig;
import youcode.com.carclub.general.Constants;


public class Service {
    private UserApi userApi;

    public Service() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();

                    // Customize the request
                    Request request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .header("Authorization", "key=" + Constants.SERVER_KEY)
                            .build();

                    okhttp3.Response response = chain.proceed(request);
                    response.cacheResponse();
                    // Customize or return the response
                    return response;
                })

                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        userApi = retrofit.create(UserApi.class);
    }


    public UserApi getUserApi() {

        return userApi;
    }


    public interface UserApi {

        @POST(Constants.PATH_FCM_NOTIFICATION)
        Single<JsonElement> sendNotification(@Body RequestBody requestBody);


      /*  @POST(Constants.PATH_FCM_NOTIFICATION)
        @FormUrlEncoded
        retrofit2.Call<Object> sendMessage(@Field("to") String topic, @Field("priority") String priority*//*, @Field("data") String notificationModel*//*);*/

    }


}
