package youcode.com.carclub.networking;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.gson.JsonElement;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import youcode.com.carclub.general.Constants;


/**
 * Created by Desk1 on 5/11/2017.
 */

public class NetworkOperations {

    public NetworkOperations() {
    }

    private final Service service = new Service();
    private final String TAG = NetworkOperations.class.getSimpleName();

    @SuppressLint("CheckResult")
    public void sendNotification(String topic) {
        String android = "{\n\t\"to\" : \"/topics/events\",\n\t\"priority\": \"high\",\n\t\"data\": {\n\t\t\"body\":\"A new event has been added!\",\n \"sound\":\"enabled\",\n \"badge\":\"0\"\n \n}\n\n\n\n\n \n}";
        String ios = "{ \n\"to\":\"/topics/events-ios\",\n \"priority\": \"high\",\n\"notification\": {\n \"title\": \"car club\",\n \"body\": \"A new event has been added!\",\n \"sound\":\"default\",\n \"badge\":\"0\"\n \n}\n\n\n\n\n \n}";

        Single.concat(postNotification(android), postNotification(ios)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .retry(10)
                .subscribe(s1 -> {
                    handleResponse(s1);
                }, throwable -> {
                    handleError(throwable);
                });
    }

    @SuppressLint("CheckResult")
    public void sendNotification(String title, String message, NetworkListener networkListener){
        String android = "{\n\t\"to\" : \"" + Constants.TOPIC_ANDROID + "\",\n\t\"priority\": \"high\",\n\t\"data\": {\n \"title\": \"" + title + "\",\n \"body\": \"" + message + "\",\n \"sound\":\"enabled\",\n \"badge\":\"0\"\n \n}\n\n\n\n\n \n}";
        String ios = "{ \n\"to\":\"" + Constants.TOPIC_IOS + "\",\n \"priority\": \"high\",\n\"notification\": {\n \"title\": \"" + title + "\",\n \"body\": \"" + message + "\",\n \"sound\":\"enabled\",\n \"badge\":\"0\"\n \n}\n\n\n\n\n \n}";
        Single.concat(postNotification(android), postNotification(ios)).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .retry(10)
                .toList()
                .subscribe(s1 -> {
                    networkListener.onComplete();
                }, throwable -> {
                    networkListener.onFail(throwable.getMessage());
                });
    }

    private void handleResponse(JsonElement s) {
        Log.i(TAG, "Notification successfully sent " + s);
    }

    private void handleError(Throwable throwable) {
        Log.e(TAG, throwable.getMessage());
    }

    private Single<JsonElement> postNotification(String s) {
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, s);
        return service.getUserApi().sendNotification(body);
    }

    public interface NetworkListener{
        void onComplete();
        void onFail(String message);
    }
}
