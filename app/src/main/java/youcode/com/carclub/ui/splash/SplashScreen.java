package youcode.com.carclub.ui.splash;

/**
 * Created by Desk1 on 6/14/2017.
 */


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import youcode.com.carclub.general.BaseActivity;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.NotificationUtil;
import youcode.com.carclub.general.SharedPrefsUtils;
import youcode.com.carclub.ui.login.LoginActivityV1;
import youcode.com.carclub.ui.main.Main2Activity;
import youcode.com.carclub.ui.payment.PaymentFragment;

public class SplashScreen extends BaseActivity implements PaymentFragment.OnFragmentInteractionListener {


    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        NotificationUtil.cancellAll(this);

        Completable.timer(1, TimeUnit.SECONDS)
                .subscribe(() -> validateUser());
    }

    private void validateUser() {
        boolean subscribed = SharedPrefsUtils.getBooleanPreference(this, Constants.USER_SUBSCRIBED, false);
        if (FirebaseAuth.getInstance().getCurrentUser() != null && subscribed)
            startActivity(new Intent(SplashScreen.this, Main2Activity.class).putExtra("notification", getIntent().getBooleanExtra("notification", false)));
        else
            startActivity(new Intent(SplashScreen.this, LoginActivityV1.class));
    }

    @Override
    public void onBillingComplete() {

    }

    @Override
    public void onPaymentVisible() {

    }
}