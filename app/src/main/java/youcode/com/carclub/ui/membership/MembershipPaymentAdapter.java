package youcode.com.carclub.ui.membership;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.models.User;

/**
 * Created by Desk1 on 8/31/2017.
 */

public class MembershipPaymentAdapter extends RecyclerView.Adapter<MembershipPaymentAdapter.MembershipViewholder> {

    private List<User> userList;
    private MembershipUserInterface membershipUserInterface;

    public MembershipPaymentAdapter(MembershipUserInterface membershipUserInterface) {
        userList = new ArrayList<>();
        this.membershipUserInterface = membershipUserInterface;
    }

    @Override
    public MembershipViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MembershipViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item_payment, parent, false));
    }

    @Override
    public void onBindViewHolder(MembershipViewholder holder, int position) {
        User user = userList.get(position);
        holder.txt_username.setText(user.getUsername());
        holder.txt_car.setText(user.getCarName());
        holder.img_delete.setOnClickListener(view -> {
            deleteUser(position);
            membershipUserInterface.onMemberDeleted(user);
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public boolean addUser(User user) {
        boolean canAdd = true;
        for (User u : userList) {
            if (u.getUsername().equals(user.getUsername())) {
                canAdd = false;
                return canAdd;
            }
        }
        userList.add(user);
       notifyDataSetChanged();
        return canAdd;
    }

    public class MembershipViewholder extends RecyclerView.ViewHolder {
        private TextView txt_username, txt_car;
        private ImageView img_delete;

        public MembershipViewholder(View itemView) {
            super(itemView);
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_car = (TextView) itemView.findViewById(R.id.txt_car);
            img_delete = (ImageView) itemView.findViewById(R.id.img_delete);
        }
    }

    private void deleteUser(int position) {
        userList.remove(position);
        notifyDataSetChanged();
    }
}
