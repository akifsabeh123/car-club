package youcode.com.carclub.ui.edit;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import youcode.com.carclub.R;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Payment;
import youcode.com.carclub.ui.payment.PaymentInterface;
import youcode.com.carclub.ui.payment.PaymentPresenter;

public class EditPaymentActivity extends AppCompatActivity implements PaymentInterface, AppCompatRadioButton.OnCheckedChangeListener {

    private ProgressDialog progressDialog;
    private PaymentPresenter paymentPresenter;

    private EditText et_name_on_card, et_card_number, et_cvv, et_billing1, et_billing2, et_billing3, et_po;
    private AppCompatRadioButton rb_visa;
    private AppCompatRadioButton rb_master;

    private Payment payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_payment);
        init();
        Completable.timer(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> paymentPresenter.getPayment());

    }

    private void init() {

        et_name_on_card = findViewById(R.id.et_name_on_card);
        et_card_number = findViewById(R.id.et_card_number);
        et_cvv = findViewById(R.id.et_cvv);
        et_billing1 = findViewById(R.id.et_billing1);
        et_billing2 = findViewById(R.id.et_billing2);
        et_billing3 = findViewById(R.id.et_billing3);
        et_po = findViewById(R.id.et_po);


        rb_visa = findViewById(R.id.rb_visa);
        rb_master = findViewById(R.id.rb_master);


        progressDialog = Utils.customizeProgressDialog(this, getString(R.string.message_please_wait));
        paymentPresenter = new PaymentPresenter(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_done:
                updatePayment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updatePayment() {
        if (valid()) {
            if (payment == null)
                payment = new Payment();
            payment.setCardName(et_name_on_card.getText().toString());
            payment.setCardNumber(et_card_number.getText().toString());
            payment.setBillingAddress1(et_billing1.getText().toString());
            payment.setBillingAddress2(et_billing2.getText().toString());
            payment.setBillingAddress3(et_billing3.getText().toString());
            payment.setCvv(et_cvv.getText().toString());
            payment.setPoBox(et_po.getText().toString());
            payment.setVisa(rb_visa.isChecked());
            paymentPresenter.updatePayment(payment);
        }
    }

    private boolean valid() {
        boolean valid = true;

     /*   if (Utils.isNullOrEmpty(et_name_on_card.getText().toString())) {
            valid = false;
            et_name_on_card.setError(getString(R.string.error_empty_field));
        }

        if (Utils.isNullOrEmpty(et_card_number.getText().toString())) {
            valid = false;
            et_card_number.setError(getString(R.string.error_empty_field));
        }

        if (Utils.isNullOrEmpty(et_cvv.getText().toString())) {
            valid = false;
            et_cvv.setError(getString(R.string.error_empty_field));
        }*/


        return valid;

    }


    @Override
    public void showWait() {
        progressDialog.show();

    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();

    }

    @Override
    public void paymentComplete() {
        finish();

    }

    @Override
    public void paymentError(String errorMessage) {

        Utils.showToast(this, errorMessage, Toast.LENGTH_LONG);

    }

    @Override
    public void getPaymentSuccess(Payment payment) {
        this.payment = payment;
        init(payment);
    }

    private void init(Payment payment) {
        et_name_on_card.setText(payment.getCardName());
        et_card_number.setText(payment.getCardNumber());
        et_cvv.setText(payment.getCvv());
        et_billing1.setText(payment.getBillingAddress1());
        et_billing2.setText(payment.getBillingAddress2());
        et_billing3.setText(payment.getBillingAddress3());
        et_po.setText(payment.getPoBox());
        if (payment.isVisa())
            rb_visa.setChecked(true);
        else
            rb_master.setChecked(true);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            switch (compoundButton.getId()) {
                case R.id.rb_visa:
                    rb_master.setChecked(false);
                    break;
                case R.id.rb_master:
                    rb_visa.setChecked(false);
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        paymentPresenter.onStop();
    }
}
