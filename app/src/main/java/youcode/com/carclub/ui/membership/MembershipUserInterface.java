package youcode.com.carclub.ui.membership;

import youcode.com.carclub.models.User;

/**
 * Created by Desk1 on 8/31/2017.
 */

public interface MembershipUserInterface {

    void onMemberSelected(User user);

    void onMemberDeleted(User user);
}
