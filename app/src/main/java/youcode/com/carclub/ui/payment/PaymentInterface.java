package youcode.com.carclub.ui.payment;

import youcode.com.carclub.general.GeneralInterface;
import youcode.com.carclub.models.Payment;

/**
 * Created by user on 8/22/2017.
 */

public interface PaymentInterface extends GeneralInterface {

    void paymentComplete();

    void paymentError(String errorMessage);

    void getPaymentSuccess(Payment payment);
}
