package youcode.com.carclub.ui.membership;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import youcode.com.carclub.R;
import youcode.com.carclub.models.User;
import youcode.com.carclub.views.DividerItemDecoration;

/**
 * A simple {@link Fragment} subclass.
 */
public class RenewMembershipDialog extends DialogFragment implements MembershipUserInterface {

    private final int REQUEST_USER = 1;
    public static final String EXTRA_USER = "extra_user";


    private TextView txt_balance, txt_add_members;
    private Button btn_purchase;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private MembershipPaymentAdapter membershipPaymentAdapter;


    public RenewMembershipDialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_renew_membership_dialog, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_Alert);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txt_balance = (TextView) view.findViewById(R.id.txt_balance);
        txt_add_members = (TextView) view.findViewById(R.id.txt_add_members);
        btn_purchase = (Button) view.findViewById(R.id.btn_purchase);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(getContext());
        membershipPaymentAdapter = new MembershipPaymentAdapter(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(membershipPaymentAdapter);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(ContextCompat.getDrawable(getContext(),
                        R.drawable.item_decorator)));

        txt_balance.setText(getString(R.string.label_balance, "$300"));
        txt_add_members.setOnClickListener(view1 -> showAddMembersActivity());
        btn_purchase.setOnClickListener(view1 -> dismissThis());
    }

    private void dismissThis() {
        dismiss();
    }

    private void showAddMembersActivity() {
        startActivityForResult(new Intent(getContext(), AddMembersActivity.class), REQUEST_USER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_USER && resultCode == Activity.RESULT_OK) {
            User user = (User) data.getSerializableExtra(EXTRA_USER);
            if (membershipPaymentAdapter.addUser(user))
                txt_balance.setText(getString(R.string.label_balance, "$" + (300 * (membershipPaymentAdapter.getItemCount() + 1))));
        }
    }

    @Override
    public void onMemberSelected(User user) {

    }

    @Override
    public void onMemberDeleted(User user) {
        txt_balance.setText(getString(R.string.label_balance, "$" + (300 * (membershipPaymentAdapter.getItemCount() + 1))));
    }
}
