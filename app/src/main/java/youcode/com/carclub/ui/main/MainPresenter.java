package youcode.com.carclub.ui.main;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import youcode.com.carclub.BuildConfig;
import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.AppVersion;
import youcode.com.carclub.models.FireBaseItem;
import youcode.com.carclub.models.User;

/**
 * Created by Akif
 * Since  11/2/2017
 * Company : Youcode
 */

public class MainPresenter {

    private MainInterface mainInterface;

    public MainPresenter(MainInterface mainInterface) {
        this.mainInterface = mainInterface;
    }

    public void getData() {
        mainInterface.showWait();
        getDataSingle().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fireBaseItems -> {
                    mainInterface.removeWait();
                    mainInterface.getItemsSuccess(fireBaseItems);
                }, throwable -> {
                    mainInterface.removeWait();
                    mainInterface.getItemsFailure(throwable.getMessage());
                });

    }

    private Single<List<FireBaseItem>> getDataSingle() {
        List<FireBaseItem> fireBaseItems = new ArrayList<>();
        fireBaseItems.add(new FireBaseItem("Events", R.drawable.img_background_event, Constants.TYPE_EVENT));
        fireBaseItems.add(new FireBaseItem("Products", R.drawable.img_background_products, Constants.TYPE_PRODUCT));
        fireBaseItems.add(new FireBaseItem("News", R.drawable.img_background_news, Constants.TYPE_NEWS));
        //fireBaseItems.add(new FireBaseItem("Purchase History", R.drawable.img_background_purchase_history, Constants.TYPE_HISTORY));
        fireBaseItems.add(new FireBaseItem("Profile & Billing", R.drawable.img_background_profile_billing, Constants.TYPE_PROFILE_BILLING));

        return Single.just(fireBaseItems);
    }

    void checkIfAdmin() {
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("admin")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                            boolean isAddmin = (boolean) dataSnapshot.getValue();
                            mainInterface.isAdmin(isAddmin);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    void checkIfShouldUpdate() {

        DatabaseReference databaseReference =
                FirebaseDatabase.getInstance().getReference().child(Constants.REF_ANDROID)
                        .child(Constants.REF_APP_VERSION);
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //AppVersion appVersion = new AppVersion();
                if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {

                    AppVersion appVersion = dataSnapshot.getValue(AppVersion.class);
                   /* for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        if (dataSnapshot1.getKey().equals(Constants.REF_VERSION))
                            appVersion.setVersion((String) dataSnapshot1.getValue());
                        else if (dataSnapshot1.getKey().equals(Constants.REF_URL))
                            appVersion.setUrl((String) dataSnapshot1.getValue());
                        else if (dataSnapshot1.getKey().equals(Constants.REF_CODE))
                            appVersion.setCode((Integer) dataSnapshot1.getValue());
                    }*/

                    if (BuildConfig.VERSION_CODE < appVersion.getCode()) {
                        mainInterface.shouldUpdate(appVersion.getUrl());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void getUser() {
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                            User user = dataSnapshot.getValue(User.class);
                            String code = user.getPorscheCode();
                            FirebaseDatabase.getInstance().getReference().child(Constants.REF_CLIENTS)
                                    .child(code)
                                    .child("date")
                                    .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            long date = (long) dataSnapshot.getValue();
                                            mainInterface.getDateSuccess(date);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
}
