package youcode.com.carclub.ui.ticket;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.EventTicket;

/**
 * Created by Desk1 on 9/7/2017.
 */

public class TicketsAdapter extends RecyclerView.Adapter<TicketsAdapter.TicketsViewHolder> {

    private List<EventTicket> eventTicketList;
    private TicketsFragment.TicketFragmentInteractionListener onFragmentInteractionListenerl;

    public TicketsAdapter(TicketsFragment.TicketFragmentInteractionListener onFragmentInteractionListenerl) {
        this.onFragmentInteractionListenerl = onFragmentInteractionListenerl;
        eventTicketList = new ArrayList<>();
    }

    @Override
    public TicketsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TicketsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ticket, parent, false));
    }

    @Override
    public void onBindViewHolder(TicketsViewHolder viewHolder, int position) {
        EventTicket event = eventTicketList.get(position);
        viewHolder.txt_ticket_number.setText(viewHolder.itemView.getContext().getString(R.string.label_number, position + 1));
        viewHolder.txt_date_date.setText(Utils.getDateFromMillis(event.getEvent().getDate(), Constants.FORMAT_DAY));
        viewHolder.txt_date_time.setText(Utils.getDateFromMillis(event.getEvent().getDate(), Constants.FORMAT_TIME));
        viewHolder.txt_location_from.setText(event.getEvent().getFrom().getMeetingPoint());
        viewHolder.txt_location_to.setText(event.getEvent().getTo().getDeparturePoint());
        viewHolder.txt_pax.setText(String.valueOf(event.getPax() + event.getNonMemberPax() + event.getChildPax()));
        viewHolder.txt_cost.setText(viewHolder.itemView.getContext().getString(R.string.label_price_raw, String.valueOf(event.getPrice())));
        viewHolder.txt_event_name.setText(event.getEvent().getTitle());
        int finalPosition = position + 1;
        viewHolder.itemView.setOnClickListener(view -> {
            if (!event.isCancelled())
                onFragmentInteractionListenerl.onTicketClick(event, finalPosition);
        });
        if (event.getTransactionNumber() != null && event.getTransactionNumber().equals(Constants.CASH))
            viewHolder.txt_cash.setVisibility(View.VISIBLE);
        else
            viewHolder.txt_cash.setVisibility(View.GONE);

        viewHolder.txt_date.setText(Utils.getDateFromMillis(event.getEvent().getDate(), Constants.FORMAT_HALF_DATE));

        if (event.isCancelled()) {
            viewHolder.left_view.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.red));
        } else
            viewHolder.left_view.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.getContext(), R.color.colorAccent));

    }

    @Override
    public int getItemCount() {
        return eventTicketList.size();
    }

    public void clear() {
        this.eventTicketList.clear();
        notifyDataSetChanged();
    }


    public class TicketsViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_ticket_number, txt_date_date, txt_date_time, txt_location_from, txt_location_to, txt_pax, txt_cost, txt_event_name, txt_cash, txt_date;
        private FrameLayout left_view;


        public TicketsViewHolder(View view) {
            super(view);
            txt_ticket_number = view.findViewById(R.id.txt_ticket_number);
            txt_date_date = view.findViewById(R.id.txt_date_date);
            txt_date_time = view.findViewById(R.id.txt_date_time);
            txt_location_from = view.findViewById(R.id.txt_location_from);
            txt_location_to = view.findViewById(R.id.txt_location_to);
            txt_pax = view.findViewById(R.id.txt_pax);
            txt_cost = view.findViewById(R.id.txt_cost);
            txt_event_name = view.findViewById(R.id.txt_event_name);
            txt_cash = view.findViewById(R.id.txt_cash);
            txt_date = view.findViewById(R.id.txt_date);
            left_view = view.findViewById(R.id.left_view);

        }
    }

    public void addItem(EventTicket eventTicket) {
        this.eventTicketList.add(eventTicket);
        Collections.sort(this.eventTicketList, (o1, o2) -> ((Long) o1.getEvent().getDate()).compareTo(o2.getEvent().getDate()));
        notifyDataSetChanged();
    }
}
