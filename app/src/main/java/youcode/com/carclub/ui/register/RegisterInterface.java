package youcode.com.carclub.ui.register;

import java.util.List;

import youcode.com.carclub.general.GeneralInterface;

/**
 * Created by Desk1 on 8/21/2017.
 */

public interface RegisterInterface extends GeneralInterface {

    void registerFailure(String error);

    void createUserTableSuccess();

    void createUserTableFailure(String message);

    void getCarsSuccess(List<String> list);

}
