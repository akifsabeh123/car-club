package youcode.com.carclub.ui.main;

import java.util.List;

import youcode.com.carclub.general.GeneralInterface;
import youcode.com.carclub.models.FireBaseItem;

/**
 * Created by Akif
 * Since  11/2/2017
 * Company : Youcode
 */

public interface MainInterface extends GeneralInterface {

    void getItemsSuccess(List<FireBaseItem> fireBaseItemList);

    void getItemsFailure(String errorMessage);

    void isAdmin(boolean isAdmin);

    void shouldUpdate(String url);

    void getDateSuccess(long date);
}
