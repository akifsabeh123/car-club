package youcode.com.carclub.ui.reports;

import java.util.List;

import youcode.com.carclub.general.GeneralInterface;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventPayments;
import youcode.com.carclub.models.User;

/**
 * Created by Akif
 * Since  10/24/2017
 * Company : Youcode
 */

public interface ReportsInterface extends GeneralInterface {

    void getEventDataSuccess(List<EventPayments> eventPurchases);

    void getDataFailure(String errorMessage);

    void getUsersSuccess(List<User> users);

    void getEventsSuccess(List<Event> events);

    void getSingleEventAnalyticsSuccess(EventPayments eventPayments);
}
