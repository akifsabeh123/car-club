package youcode.com.carclub.ui.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.FireBaseItem;

/**
 * Created by Akif
 * Since  11/2/2017
 * Company : Youcode
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainViewHolder> {


    private MainAdapterInterface mainAdapterInterface;
    private List<FireBaseItem> fireBaseItems;

    public MainAdapter(MainAdapterInterface mainAdapterInterface) {
        this.mainAdapterInterface = mainAdapterInterface;
        fireBaseItems = new ArrayList<>();
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        FireBaseItem fireBaseItem = fireBaseItems.get(position);
        Context context = holder.itemView.getContext();

        Picasso.with(context).load(fireBaseItem.getImageId()).into(holder.img);
        holder.txt_title.setText(fireBaseItem.getTitle());
        String query = "";
        switch (position) {
            case 0:
            case 3:
            case 4:
                query = Constants.REF_EVENTS;
                break;
            case 1:
                query = Constants.REF_PRODUCTS;
                break;
            case 2:
                query = Constants.REF_NEWS;
                break;
        }

        String finalQuery = query;
        holder.itemView.setOnClickListener(view -> mainAdapterInterface.onItemClick(holder.frameLayout, fireBaseItem, finalQuery));
    }

    @Override
    public int getItemCount() {
        return fireBaseItems.size();
    }

    public void addItems(List<FireBaseItem> fireBaseItemList) {
        fireBaseItems.addAll(fireBaseItemList);
        notifyDataSetChanged();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder {
        private final ImageView img;
        private final TextView txt_title;
        private FrameLayout frameLayout;

        public MainViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            frameLayout = (FrameLayout) itemView.findViewById(R.id.frTransition);
        }
    }

    public interface MainAdapterInterface {

        void onItemClick(View view, FireBaseItem fireBaseItem, String query);

    }

    public void performClick(){

    }
}
