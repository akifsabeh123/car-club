package youcode.com.carclub.ui.membership;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.models.User;

/**
 * Created by Desk1 on 8/31/2017.
 */

public class AddMemberAdapter extends RecyclerView.Adapter<AddMemberAdapter.MembershipViewholder> {

    private List<User> userList;
    private MembershipUserInterface membershipUserInterface;

    public AddMemberAdapter(MembershipUserInterface membershipUserInterface) {
        userList = new ArrayList<>();
        this.membershipUserInterface = membershipUserInterface;
    }

    @Override
    public MembershipViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MembershipViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MembershipViewholder holder, int position) {
        User user = userList.get(position);
        holder.txt_username.setText(user.getUsername());
        holder.txt_car.setText(user.getCarName());
        holder.itemView.setOnClickListener(view -> membershipUserInterface.onMemberSelected(user));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void addUser(User user) {
            userList.clear();
            userList.add(user);
            notifyItemInserted(userList.size());
    }

    public class MembershipViewholder extends RecyclerView.ViewHolder {
        private TextView txt_username, txt_car;


        public MembershipViewholder(View itemView) {
            super(itemView);
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            txt_car = (TextView) itemView.findViewById(R.id.txt_car);
        }
    }
}
