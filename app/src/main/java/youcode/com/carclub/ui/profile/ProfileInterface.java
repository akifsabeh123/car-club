package youcode.com.carclub.ui.profile;

import java.util.List;

import youcode.com.carclub.general.GeneralInterface;
import youcode.com.carclub.models.User;

/**
 * Created by user on 8/22/2017.
 */

public interface ProfileInterface extends GeneralInterface {

    void getUserSuccess(User user);

    void getUserFailure(String errorMessage);

    void updateUserComplete();

    void getDateSuccess(long date);

    void getCarsSuccess(List<String> list);
}

