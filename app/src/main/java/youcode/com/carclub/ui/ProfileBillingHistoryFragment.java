package youcode.com.carclub.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import youcode.com.carclub.R;
import youcode.com.carclub.purchases.PurchasesFragment;
import youcode.com.carclub.ui.profile.ProfileFragment;
import youcode.com.carclub.ui.ticket.TicketsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileBillingHistoryFragment extends Fragment  {


    private SectionsPagerAdapter mSectionsPagerAdapter;

    private TabLayout tabLayout;

    private ViewPager mViewPager;


    public static ProfileBillingHistoryFragment newInstance() {
        return new ProfileBillingHistoryFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_billing_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = view.findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = view.findViewById(R.id.tabs);

        tabLayout.setupWithViewPager(mViewPager);


        tabLayout.setupWithViewPager(mViewPager);


    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if (position == 0)
                return ProfileFragment.newInstance();
            else if (position == 1)
                return PurchasesFragment.newInstance();
            else
                return TicketsFragment.newInstance();

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Profile";
                case 1:
                    return "Purchases";
                case 2:
                    return "Tickets";
            }
            return null;
        }
    }
}
