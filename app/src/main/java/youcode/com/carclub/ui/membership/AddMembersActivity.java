package youcode.com.carclub.ui.membership;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.User;
import youcode.com.carclub.views.DividerItemDecoration;

public class AddMembersActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, AddMembersInterface, MembershipUserInterface {

    private ProgressDialog progressDialog;
    private AddMembersPresenter addMembersPresenter;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private AddMemberAdapter addMemberAdapter;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_members);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.label_add_members));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

    }

    private void init() {
        searchView = (SearchView) findViewById(R.id.searchView);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.message_please_wait));
        addMembersPresenter = new AddMembersPresenter(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(this);
        addMemberAdapter = new AddMemberAdapter(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(addMemberAdapter);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(ContextCompat.getDrawable(this,
                        R.drawable.item_decorator)));
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        addMembersPresenter.searchUser(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void showWait() {

        progressDialog.show();

    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }

    @Override
    public void getUserSuccess(User user) {
        addMemberAdapter.addUser(user);
    }

    @Override
    public void getUserFailure(String errorMessage) {
        Utils.showToast(this, errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void noUserFound() {
        Utils.showToast(this, getString(R.string.error_no_user_found), Toast.LENGTH_LONG);
    }

    @Override
    protected void onStop() {
        super.onStop();
        addMembersPresenter.onStop();
    }

    @Override
    public void onMemberSelected(User user) {

        Intent intent = new Intent();
        intent.putExtra(RenewMembershipDialog.EXTRA_USER, user);
        setResult(Activity.RESULT_OK, intent);
        finish();

    }

    @Override
    public void onMemberDeleted(User user) {

    }
}
