package youcode.com.carclub.ui.ticket;


import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;
import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.profile.ProfileInterface;
import youcode.com.carclub.ui.profile.ProfilePresenter;

import static youcode.com.carclub.general.Constants.DIRECTORY;
import static youcode.com.carclub.general.Constants.RC_CAMERA_AND_LOCATION;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventTicketDialog#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventTicketDialog extends DialogFragment implements ProfileInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ImageView img_close, img_event, img_save;
    private TextView txt_price, txt_pax, txt_cost, txt_event_name, txt_name, txt_car, txt_from, txt_to, txt_description, txt_event_date, txt_ticket_number, txt_pax_kids,
            txt_non_member_pax,txt_purchase_date;
    private LinearLayout ll_view;
    private ProfilePresenter profilePresenter;
    private int ticketNumber = 1;


    private EventTicket event;


    public EventTicketDialog() {

    }


    public static EventTicketDialog newInstance(EventTicket eventTicket, int ticketNumber) {
        EventTicketDialog fragment = new EventTicketDialog();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, eventTicket);
        args.putSerializable(ARG_PARAM2, ticketNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_Alert);
        if (getArguments() != null) {
            event = (EventTicket) getArguments().getSerializable(ARG_PARAM1);
            ticketNumber = getArguments().getInt(ARG_PARAM2, 1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event_ticket_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        img_close = view.findViewById(R.id.img_close);
        img_event = view.findViewById(R.id.img_event);
        //txt_price = view.findViewById(R.id.txt_price);
        txt_pax = view.findViewById(R.id.txt_pax);
        txt_pax_kids = view.findViewById(R.id.txt_pax_kids);
        txt_cost = view.findViewById(R.id.txt_cost);
        txt_event_name = view.findViewById(R.id.txt_event_name);
        txt_name = view.findViewById(R.id.txt_name);
        txt_non_member_pax = view.findViewById(R.id.txt_non_member_pax);
        txt_purchase_date = view.findViewById(R.id.txt_purchase_date);
        txt_car = view.findViewById(R.id.txt_car);
        txt_from = view.findViewById(R.id.txt_from);
        txt_to = view.findViewById(R.id.txt_to);
        txt_description = view.findViewById(R.id.txt_description);
        txt_event_date = view.findViewById(R.id.txt_event_date);
        txt_ticket_number = view.findViewById(R.id.txt_ticket_number);
        img_save = view.findViewById(R.id.img_save);
        ll_view = view.findViewById(R.id.ll_view);

        img_close.setOnClickListener(view1 -> dismiss());
        img_save.setOnClickListener(view1 -> saveTicket());

        profilePresenter = new ProfilePresenter(this);
        profilePresenter.getUser();
        init(event);

        if (!EasyPermissions.hasPermissions(getContext(), Constants.STORAGE_PERMISSIONS))
            EasyPermissions.requestPermissions(this, getString(R.string.storage_rationale),
                    RC_CAMERA_AND_LOCATION, Constants.STORAGE_PERMISSIONS);
    }

    private void init(EventTicket event) {

        Glide.with(getContext()).load(event.getEvent().getImgUrl()).into(img_event);
        txt_cost.setText(getString(R.string.label_price_raw, String.valueOf((event.getPrice()))));
       // txt_price.setText(getString(R.string.label_price_raw, event.getEvent().getPrice()));
        txt_pax.setText(String.valueOf(event.getPax()));
        txt_non_member_pax.setText(String.valueOf(event.getNonMemberPax()));
        txt_pax_kids.setText(String.valueOf(event.getChildPax()));
        txt_event_name.setText(event.getEvent().getTitle());
        txt_from.setText(event.getEvent().getFrom().getMeetingPoint());
        txt_to.setText(event.getEvent().getTo().getDeparturePoint());
        txt_description.setText(event.getEvent().getDescription());
        txt_event_date.setText(Utils.getDateFromMillis(event.getEvent().getDate(), Constants.FORMAT_FULL_DATE));
        txt_purchase_date.setText(Utils.getDateFromMillis(event.getPurchaseDate(), Constants.FORMAT_FULL_DATE));
        txt_ticket_number.setText(String.format("Ticket Number %d", ticketNumber));

    }

    private void saveTicket() {
        store(getScreenShot(), event.getEvent().getEventId() + ".jpg");
    }

    private void store(Bitmap bm, String fileName) {
        String dirPath = Environment.getExternalStorageDirectory() + File.separator + DIRECTORY;
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
            addImageToGallery(file.getAbsolutePath(), getContext());
            Utils.showToast(getContext(), getString(R.string.message_screenshot_saved), Toast.LENGTH_SHORT);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.showToast(getContext(), getString(R.string.error_unknown), Toast.LENGTH_SHORT);
        }
    }

    public static void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    private Bitmap getScreenShot() {
        ll_view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(ll_view.getDrawingCache());
        ll_view.setDrawingCacheEnabled(false);
        return bitmap;
    }

    @Override
    public void showWait() {

    }

    @Override
    public void removeWait() {

    }

    @Override
    public void getUserSuccess(User user) {
        if (getContext() != null) {
            txt_name.setText(user.getName() + " " + user.getFamilyName());
            txt_car.setText(user.getCarName());
        }

    }

    @Override
    public void getUserFailure(String errorMessage) {

    }

    @Override
    public void updateUserComplete() {

    }

    @Override
    public void getDateSuccess(long date) {

    }

    @Override
    public void getCarsSuccess(List<String> list) {

    }
}
