package youcode.com.carclub.ui.register;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.User;

/**
 * Created by Desk1 on 8/21/2017.
 */

public class RegisterPresenter {

    private RegisterInterface registerInterface;
    private FirebaseAuth mAuth;

    public RegisterPresenter(RegisterInterface registerInterface) {
        this.registerInterface = registerInterface;
        mAuth = FirebaseAuth.getInstance();
    }

    public void registerUser(User user) {
        registerInterface.showWait();
        mAuth.createUserWithEmailAndPassword(user.getUsername() + Constants.EMAIL_COMPLETION, user.getPassword())
                .addOnCompleteListener(task -> {

                    if (task.isSuccessful()) {
                        /*if (!user.getUsername().toLowerCase().equals(Constants.ADMINISTRATOR))
                            FirebaseMessaging.getInstance().subscribeToTopic(Constants.TOPIC_EVENTS);*/
                        createUserTable(user, task.getResult().getUser().getUid());
                    } else {
                        registerInterface.removeWait();
                        registerInterface.registerFailure(Utils.getError(task.getException().getMessage()));
                    }


                });
    }

    private void handleTaskCompletion(Task<Void> task) {
        if (task.isSuccessful())
            registerInterface.createUserTableSuccess();
        else

            registerInterface.createUserTableFailure(task.getException().getMessage());
    }

    private void createUserTable(User user, String uid) {
        FirebaseInstanceId firebaseInstanceId = FirebaseInstanceId.getInstance();
        user.setToken(firebaseInstanceId.getToken());
        user.setId(uid);
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS)
                .child(uid)
                .setValue(user).addOnCompleteListener(task -> {
            registerInterface.removeWait();
            handleTaskCompletion(task);
        });
    }

    public void getCars() {
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_CARS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<String> list = new ArrayList<>();
                        for(DataSnapshot snapshot : dataSnapshot.getChildren())
                            list.add((String) snapshot.getValue());
                       registerInterface.getCarsSuccess(list);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
}
