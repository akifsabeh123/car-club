package youcode.com.carclub.ui.cash;

import android.annotation.SuppressLint;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Client;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.User;

/**
 * Created by Akif sabeh on 2/1/2019.
 */
public class PayByCashPresenter {

    private PayByCashInterface payByCashInterface;

    public PayByCashPresenter(PayByCashInterface payByCashInterface) {
        this.payByCashInterface = payByCashInterface;
    }

    @SuppressLint("CheckResult")
    void getEvents() {
        payByCashInterface.showWait();
        getEventsListMaybe().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(events -> {
                    payByCashInterface.removeWait();
                    payByCashInterface.getEventsSuccess(events);
                }, throwable -> {
                    payByCashInterface.removeWait();
                    payByCashInterface.onError(throwable.getMessage());
                });
    }

    @SuppressLint("CheckResult")
    void getUsers() {
        payByCashInterface.showWait();
        getAllUsers().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(events -> {
                    payByCashInterface.removeWait();
                    payByCashInterface.getUsersSuccess(events);
                }, throwable -> {
                    payByCashInterface.removeWait();
                    payByCashInterface.onError(throwable.getMessage());
                });

    }

    private Single<List<Event>> getEventsListMaybe() {
        return Single.create(e -> FirebaseDatabase.getInstance().getReference().child(Constants.REF_EVENTS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Event> events = new ArrayList<>();
                        if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                                events.add(dataSnapshot1.getValue(Event.class));
                        }
                        e.onSuccess(events);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                }));
    }

    private Single<List<User>> getUserssListMaybe() {
        return Single.create(e -> FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<User> events = new ArrayList<>();
                        if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                                events.add(dataSnapshot1.getValue(User.class));
                        }
                        e.onSuccess(events);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                }));
    }


    private Single<List<Client>> getAllUsers() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.REF_CLIENTS);
        ref.keepSynced(true);
        return Single.create(e ->

                ref
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                List<Client> events = new ArrayList<>();
                                if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                        Client client = dataSnapshot1.getValue(Client.class);
                                        client.setCode(dataSnapshot1.getKey());
                                        events.add(client);
                                    }
                                }
                                e.onSuccess(events);

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                e.onError(databaseError.toException());
                            }
                        }));
    }

    @SuppressLint("CheckResult")
    public void update(EventTicket eventTicket, Client client) {
        payByCashInterface.showWait();


        findClient(client)
                .subscribeOn(Schedulers.io())
                .subscribe(selectedUser -> {
                    eventTicket.setUserId(selectedUser.getId());
                    String key = FirebaseDatabase.getInstance().getReference().child(Constants.REF_PURCHASES)
                            .child(selectedUser.getId()).push().getKey();
                    FirebaseDatabase.getInstance().getReference().child(Constants.REF_PURCHASES)
                            .child(selectedUser.getId()).child(key)
                            .setValue(eventTicket)
                            .addOnCompleteListener(task -> {
                                if (!task.isSuccessful()) {
                                    payByCashInterface.removeWait();
                                    payByCashInterface.onError(task.getException().getMessage());
                                } else {
                                    payByCashInterface.onComplete();
                                   /* String reference = FirebaseDatabase.getInstance().getReference().child(Constants.REF_PURCHASES)
                                            .child(selectedUser.getId()).child(key).toString().replace("https://porsche-58786.firebaseio.com/", "");
                                    DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(Constants.ANALYTICS)
                                            .child(ProductType.EVENT.toString()).child(eventTicket.getEvent().getEventId());
                                    databaseReference1.child(key).setValue(reference).addOnCompleteListener(task1 -> {
                                        payByCashInterface.removeWait();
                                        if (task1.isSuccessful())
                                            payByCashInterface.onComplete();
                                        else
                                            payByCashInterface.onError(task.getException().getMessage());
                                    });*/
                                }
                            });


                });


    }

    private Single<User> findClient(Client client) {
        return Single.create(e -> {
            FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS)
                    .orderByChild("porscheCode")
                    .equalTo(client.getCode())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists() && dataSnapshot.getChildren() != null) {
                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                    User user = dataSnapshot1.getValue(User.class);
                                    e.onSuccess(user);
                                    return;
                                }
                            } else {
                                User user = new User();
                                user.setId(client.getCode());
                                user.setMobile(client.getPhone());
                                e.onSuccess(user);
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            e.onError(databaseError.toException());
                        }
                    });
        });
    }
}
