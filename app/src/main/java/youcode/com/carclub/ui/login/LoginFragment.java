package youcode.com.carclub.ui.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;

import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Client;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment implements View.OnClickListener, LoginInterface {
    private ProgressDialog progressDialog;
    private LoginPresenter loginPresenter;

    private Button btn_sign_in;
    private TextView txt_create_user;
    private EditText et_username, et_password;

    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn_sign_in = (Button) view.findViewById(R.id.btn_sign_in);
        et_username = (EditText) view.findViewById(R.id.et_username);
        et_password = (EditText) view.findViewById(R.id.et_password);
        txt_create_user = (TextView) view.findViewById(R.id.txt_create_account);

        btn_sign_in.setOnClickListener(this);
        txt_create_user.setOnClickListener(this);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.message_logging_in));
        //loginPresenter = new LoginPresenter(this,getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NewsFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_in:
                loginUser(et_username.getText().toString(), et_password.getText().toString());
                break;
            case R.id.txt_create_account:
                createAccount();
        }
    }

    private void loginUser(String username, String password) {
        if (valid(username, password))
            loginPresenter.loginUser(username, password);
    }

    private boolean valid(String username, String password) {
        boolean valid = true;

        if (Utils.isNullOrEmpty(username)) {
            et_username.setError(getString(R.string.error_empty_field));
            valid = false;
        }
        if (Utils.isNullOrEmpty(password)) {
            et_password.setError(getString(R.string.error_empty_field));
            valid = false;
        }
        return valid;
    }

    private void createAccount() {
        mListener.createAccount();
    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }

    @Override
    public void loginSuccess() {
        mListener.loginSuccess();
    }

    @Override
    public void loginFailure(String errorMessage) {
        mListener.loginFailure(errorMessage);
    }

    @Override
    public void checkIfUserExistComplete(boolean exist) {

    }

    @Override
    public void checkIfUserExistError(String message) {

    }

    @Override
    public void updateUI(int stateCodeSent) {

    }

    @Override
    public void showMobileError(String error) {

    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void updateUI(int stateSigninSuccess, FirebaseUser user) {

    }

    @Override
    public void updateUI(int stateSigninSuccess, PhoneAuthCredential credential) {

    }

    @Override
    public void showCodeError(String error) {

    }

    @Override
    public void getCarsSuccess(List<String> list) {

    }

    @Override
    public void getClientSuccess(Client client) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void loginSuccess();

        void createAccount();

        void loginFailure(String errorMssage);
    }
}
