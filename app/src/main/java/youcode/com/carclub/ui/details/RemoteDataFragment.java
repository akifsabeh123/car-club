package youcode.com.carclub.ui.details;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Event;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RemoteDataFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RemoteDataFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RemoteDataFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private RecyclerView recyclerView;

    private OnFragmentInteractionListener mListener;

    private FirebaseRecyclerAdapter<Event, MyViewHolder> adapter;

    public RemoteDataFragment() {

    }


    public static RemoteDataFragment newInstance(String query) {
        RemoteDataFragment fragment = new RemoteDataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, query);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Query query;
        if (getArguments() != null)
            query = FirebaseDatabase.getInstance().getReference().child(getArguments().getString(ARG_PARAM1)).orderByChild("status").equalTo(true);
        else
            query = FirebaseDatabase.getInstance().getReference().child(Constants.REF_EVENTS).orderByChild("status").equalTo(true);

        adapter = new FirebaseRecyclerAdapter<Event, MyViewHolder>(Event.class, R.layout.event_list_item, MyViewHolder.class, query) {
            @Override
            protected void populateViewHolder(MyViewHolder viewHolder, Event model, int position) {
                viewHolder.txt_title.setText(model.getTitle());
                viewHolder.txt_description.setText(model.getDescription());
                if (model.getType() == Constants.TYPE_EVENT || model.getType() == Constants.TYPE_NEWS) {
                    viewHolder.txt_price.setVisibility(View.GONE);
                    //viewHolder.txt_address.setText(model.getEventLocation().getAddress());
                } else if (model.getType() == Constants.TYPE_PRODUCT) {
                    viewHolder.txt_price.setVisibility(View.VISIBLE);
                    viewHolder.txt_address.setText(model.getDescription());
                    viewHolder.txt_price.setText(String.format("Price : $%s", model.getPrice()));
                }
                Glide.with(getContext()).load(model.getImgUrl()).thumbnail(0.25f).into(viewHolder.img);

                viewHolder.itemView.setOnClickListener(view -> mListener.RemoteDataFragmentInteraction(model, model.getType()));
            }

        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_remote_data, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayout.VERTICAL);
        dividerItemDecoration.setDrawable(getContext().getResources().getDrawable(R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);
        //recyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getContext(),R.drawable.)));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void RemoteDataFragmentInteraction(Event event, int type);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView txt_title, txt_description, txt_address, txt_price;
        private final ImageView img;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_description = itemView.findViewById(R.id.txt_description);
            txt_address = itemView.findViewById(R.id.txt_address);
            txt_price = itemView.findViewById(R.id.txt_price);
            img = itemView.findViewById(R.id.img);
        }
    }

}
