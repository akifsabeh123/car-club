package youcode.com.carclub.ui.events;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.newItems.AddEventActivity;
import youcode.com.carclub.ui.profile.ProfileInterface;
import youcode.com.carclub.ui.profile.ProfilePresenter;
import youcode.com.carclub.views.CustomTabLayout;

public class FragmentTabbedEvents extends Fragment implements ProfileInterface {


    private SectionsPagerAdapter mSectionsPagerAdapter;

    private CustomTabLayout tabLayout;
    private OnFragmentInteractionListener mListener;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private FloatingActionButton fab_add_event;

    private ProfilePresenter profilePresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public static FragmentTabbedEvents newInstance() {
        FragmentTabbedEvents fragment = new FragmentTabbedEvents();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tabbed_events, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) view.findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (CustomTabLayout) view.findViewById(R.id.tabs);

        fab_add_event = (FloatingActionButton) view.findViewById(R.id.fab_add_event);
        tabLayout.setupWithViewPager(mViewPager);
        mListener.onFragmentTabbedEventsVisible();
        fab_add_event.setOnClickListener(view1 -> {
            AddEventActivity.startActivity(getActivity(), mViewPager.getCurrentItem() + 1);
        });

        getCurrentUser();
    }

    private void getCurrentUser() {
        profilePresenter = new ProfilePresenter(this);
        profilePresenter.getUser();
    }

    @Override
    public void showWait() {

    }

    @Override
    public void removeWait() {

    }

    @Override
    public void getUserSuccess(User user) {

       /* if (user.isAdmin()) {
            fab_add_event.setVisibility(View.VISIBLE);
            FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.TOPIC_EVENTS);
            mListener.isAdmin(true);
        } else {*/
            fab_add_event.setVisibility(View.GONE);
            FirebaseMessaging.getInstance().subscribeToTopic(Constants.TOPIC_EVENTS);
            mListener.isAdmin(false);
        //}

    }

    @Override
    public void getUserFailure(String errorMessage) {

    }

    @Override
    public void updateUserComplete() {

    }

    @Override
    public void getDateSuccess(long date) {

    }

    @Override
    public void getCarsSuccess(List<String> list) {

    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_fragment_tabbed_events, container, false);
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if (position == 0)
                return EventsFragment.newInstance();
            else if (position == 1)
                return ProductsFragment.newInstance();
            else
                return NewsFragment.newInstance();

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "EVENTS";
                case 1:
                    return "PRODUCTS";
                case 2:
                    return "NEWS";
            }
            return null;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NewsFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentTabbedEventsVisible();

        void isAdmin(boolean isAdmin);
    }

    public boolean moveTabToFirst() {
        if (mViewPager != null)
            if (mViewPager.getCurrentItem() != 0) {
                mViewPager.setCurrentItem(0);
                return false;
            }
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        profilePresenter.onStop();
    }
}
