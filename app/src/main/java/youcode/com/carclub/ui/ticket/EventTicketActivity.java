package youcode.com.carclub.ui.ticket;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;
import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.profile.ProfileInterface;
import youcode.com.carclub.ui.profile.ProfilePresenter;

import static youcode.com.carclub.general.Constants.DIRECTORY;
import static youcode.com.carclub.general.Constants.RC_CAMERA_AND_LOCATION;

public class EventTicketActivity extends AppCompatActivity implements ProfileInterface {

    public static final String EXTRA_EVENT = "extra_event";
    public static final String EXTRA_PAX = "extra_pax";
    private ImageView img_event;
    private TextView txt_name, txt_car_name, txt_location, txt_date, txt_pax;
    private LinearLayout ll_view;

    private EventTicket event;

    private ProfilePresenter profilePresenter;

    private ProgressDialog progressDialog;


    public static void startActivity(Activity odlActivity, EventTicket event) {
        Intent intent = new Intent(odlActivity, EventTicketActivity.class);
        intent.putExtra(EXTRA_EVENT, event);
        odlActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_event_ticket);
        init();
    }

    private void init() {
        img_event = (ImageView) findViewById(R.id.img_event);
        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_car_name = (TextView) findViewById(R.id.txt_car_name);
        txt_location = (TextView) findViewById(R.id.txt_location);
        txt_date = (TextView) findViewById(R.id.txt_date);
        txt_pax = (TextView) findViewById(R.id.txt_pax);
        ll_view = (LinearLayout) findViewById(R.id.ll_view);

        progressDialog = Utils.customizeProgressDialog(this, getString(R.string.message_please_wait));

        profilePresenter = new ProfilePresenter(this);
        event = (EventTicket) getIntent().getSerializableExtra(EXTRA_EVENT);

        getSupportActionBar().setTitle(event.getEvent().getTitle());

        profilePresenter.getUser();

        if (!EasyPermissions.hasPermissions(this, Constants.STORAGE_PERMISSIONS))
            EasyPermissions.requestPermissions(this, getString(R.string.storage_rationale),
                    RC_CAMERA_AND_LOCATION, Constants.STORAGE_PERMISSIONS);
    }

    private void initEvent(User user, EventTicket event) {

        Glide.with(this).load(event.getEvent().getImgUrl()).into(img_event);
        txt_name.setText(user.getUsername());
        txt_car_name.setText(user.getCarName());
        txt_date.setText(Utils.getDateFromMillis(event.getEvent().getDate(), Constants.FORMAT_FULL_DATE));
      //  txt_location.setText(event.getEvent().getEventLocation().getAddress());
        txt_pax.setText(String.valueOf(event.getPax()));

    }

    @Override
    public void showWait() {
        progressDialog.show();

    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();

    }

    @Override
    public void getUserSuccess(User user) {
        initEvent(user, event);
    }

    @Override
    public void getUserFailure(String errorMessage) {
        Utils.showToast(this, errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void updateUserComplete() {

    }

    @Override
    public void getDateSuccess(long date) {

    }

    @Override
    public void getCarsSuccess(List<String> list) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        profilePresenter.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_save:
                saveTicket();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveTicket() {
        store(getScreenShot(), event.getEvent().getEventId() + ".jpg");
    }

    private void store(Bitmap bm, String fileName) {
        String dirPath = Environment.getExternalStorageDirectory() + File.separator + DIRECTORY;
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
            Utils.showToast(this, getString(R.string.message_screenshot_saved), Toast.LENGTH_SHORT);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.showToast(this, getString(R.string.error_unknown), Toast.LENGTH_SHORT);

        }
    }

    private Bitmap getScreenShot() {
        ll_view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(ll_view.getDrawingCache());
        ll_view.setDrawingCacheEnabled(false);
        return bitmap;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ticket, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
