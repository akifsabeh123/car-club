package youcode.com.carclub.ui.events.ui;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import youcode.com.carclub.general.Constants;

/**
 * Created by Akif
 * Since  11/29/2017
 * Company : Youcode
 */

public class EventPresenter {
    private EventInterface eventInterface;

    public EventPresenter(EventInterface eventInterface) {
        this.eventInterface = eventInterface;
    }

    public void checkIfAttending(String eventId) {
        eventInterface.showWait();
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_PURCHASES)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(eventId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        eventInterface.removeWait();
                        if (dataSnapshot.exists() && dataSnapshot.getValue() != null)
                            eventInterface.isAttending(true);
                        else
                            eventInterface.isAttending(false);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        eventInterface.removeWait();
                        eventInterface.checkFailure(databaseError.getMessage());
                    }
                });
    }
}
