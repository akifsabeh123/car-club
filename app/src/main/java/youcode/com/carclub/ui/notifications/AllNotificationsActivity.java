package youcode.com.carclub.ui.notifications;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Notification;

public class AllNotificationsActivity extends DialogFragment {
    private NotificationsRecyclerAdapter notificationsRecyclerAdapter;
    private RecyclerView recyclerView;
    private ImageButton img_add;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_all_notifications, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        img_add = view.findViewById(R.id.img_add);
        initRecycler(view);
        img_add.setOnClickListener(v -> startCreateNotification());
    }


    private void initRecycler(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        Query query = FirebaseDatabase.getInstance().getReference().child(Constants.NOTIFICATIONS);
        notificationsRecyclerAdapter = new NotificationsRecyclerAdapter(Notification.class, R.layout.notification_item, NotificationsRecyclerAdapter.MyViewHolder.class, query, new NotificationsRecyclerAdapter.NotificationsRecyclerAdapterInterface() {
            @Override
            public void onNotificationClick(Notification notification) {

            }
        });
        recyclerView.setAdapter(notificationsRecyclerAdapter);

    }


    private void startCreateNotification() {
        startActivity(new Intent(getContext(), NewNotificationActivity.class));
    }

}
