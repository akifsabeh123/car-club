package youcode.com.carclub.ui.membership;

import youcode.com.carclub.general.GeneralInterface;
import youcode.com.carclub.models.User;

/**
 * Created by Desk1 on 8/30/2017.
 */

public interface AddMembersInterface extends GeneralInterface {

    void getUserSuccess(User user);

    void getUserFailure(String errorMessage);

    void noUserFound();
}
