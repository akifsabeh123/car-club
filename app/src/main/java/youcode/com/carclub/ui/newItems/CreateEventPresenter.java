package youcode.com.carclub.ui.newItems;

import android.net.Uri;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Event;

import static youcode.com.carclub.ui.newItems.AddEventActivity.TYPE_EVENT;
import static youcode.com.carclub.ui.newItems.AddEventActivity.TYPE_NEWS;
import static youcode.com.carclub.ui.newItems.AddEventActivity.TYPE_PRODUCT;

/**
 * Created by user on 8/23/2017.
 */

public class CreateEventPresenter {
    private CreateEventInterface createEventInterface;
    private StorageReference mStorageRef;
    private StorageReference imagesRef;
    private String TAG = CreateEventPresenter.class.getSimpleName();

    public CreateEventPresenter(CreateEventInterface createEventInterface) {
        this.createEventInterface = createEventInterface;
        mStorageRef = FirebaseStorage.getInstance().getReference().child(Constants.REF_IMAGES);
    }

    public void createEvent(Event event, Uri uri, int ITEM_TYPE) {
        createEventInterface.showWait();

        if (uri == null && event.getImgUrl() != null && !event.getImgUrl().isEmpty()) {
            FirebaseDatabase.getInstance().getReference().child(Constants.REF_EVENTS)
                    .child(event.getEventId())
                    .setValue(event)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful())
                            createEventInterface.createSuccess();
                        else
                            createEventInterface.createFailure(task.getException().getMessage());
                    });
        } else {


            uploadImage(uri, new UploadCallBack() {
                @Override
                public void uploadSuccess(String downloadUrl) {
                    String ref = Constants.REF_EVENTS;
                    switch (ITEM_TYPE) {
                        case TYPE_EVENT:
                            ref = Constants.REF_EVENTS;
                            break;
                        case TYPE_PRODUCT:
                            ref = Constants.REF_PRODUCTS;
                            break;
                        case TYPE_NEWS:
                            ref = Constants.REF_NEWS;
                            break;
                    }
                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(ref);
                    String id = databaseReference.push().getKey();
                    event.setEventId(id);
                    event.setImgUrl(downloadUrl);
                    event.setInvertedDate(event.getDate() * -1);

                    databaseReference.child(id)
                            .setValue(event)
                            .addOnCompleteListener(task -> {
                                createEventInterface.removeWait();
                                if (task.isSuccessful())
                                    createEventInterface.createSuccess();
                                else
                                    createEventInterface.createFailure(task.getException().getMessage());
                            });
                }

                @Override
                public void uploadFailure(String errorMessage) {
                    createEventInterface.createFailure(errorMessage);
                }
            });
        }


    }

    public void uploadImage(Uri uri, UploadCallBack uploadCallBack) {
        File file = new File(uri.getPath());
        imagesRef = mStorageRef.child(file.getName());
        imagesRef.putFile(uri)
                /*   .addOnProgressListener(taskSnapshot -> {
                       createEventInterface.uploadMax((int) taskSnapshot.getTotalByteCount());
                       int percentage = (int) Math.round(taskSnapshot.getBytesTransferred() * 100.0 / (int) taskSnapshot.getTotalByteCount());
                       createEventInterface.uploadProgress(percentage);
                   })*/
                .addOnSuccessListener(task -> {
                    uploadCallBack.uploadSuccess(task.getDownloadUrl().toString());
                }).addOnFailureListener(e -> {
            uploadCallBack.uploadFailure(e.getMessage());
        });
    }

    private interface UploadCallBack {
        void uploadSuccess(String downloadUrl);

        void uploadFailure(String errorMessage);

    }

}
