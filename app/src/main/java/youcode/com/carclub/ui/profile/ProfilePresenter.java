package youcode.com.carclub.ui.profile;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.User;

import static youcode.com.carclub.general.Constants.REF_PAYMENT_STATUS;
import static youcode.com.carclub.general.Constants.REF_USERS;

/**
 * Created by user on 8/22/2017.
 */

public class ProfilePresenter {

    private ProfileInterface profileInterface;
    private FirebaseUser firebaseUser;
    private Query query;


    public ProfilePresenter(ProfileInterface profileInterface) {
        this.profileInterface = profileInterface;
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    public void getCars() {
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_CARS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<String> list = new ArrayList<>();
                        for(DataSnapshot snapshot : dataSnapshot.getChildren())
                            list.add((String) snapshot.getValue());
                        profileInterface.getCarsSuccess(list);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getUser() {
        profileInterface.showWait();

        FirebaseDatabase.getInstance().getReference().child(Constants.REF_CARS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<String> list = new ArrayList<>();
                        for(DataSnapshot snapshot : dataSnapshot.getChildren())
                            list.add((String) snapshot.getValue());
                        profileInterface.getCarsSuccess(list);

                        query = FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS)
                                .child(firebaseUser.getUid());

                        query.addValueEventListener(valueEventListener);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            profileInterface.removeWait();
            User user = dataSnapshot.getValue(User.class);
            if (user != null) {
                profileInterface.getUserSuccess(user);
                DatabaseReference databaseReference =
                        FirebaseDatabase.getInstance().getReference().child(Constants.REF_CLIENTS)
                                .child(user.getPorscheCode())
                                .child("date");
                databaseReference.keepSynced(true);
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            long date = (long) dataSnapshot.getValue();
                            profileInterface.getDateSuccess(date);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            profileInterface.removeWait();
            profileInterface.getUserFailure(databaseError.getMessage());
        }
    };

    public void signOutUser() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.TOPIC_EVENTS);
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS).child(firebaseUser.getUid())
                .child(Constants.REF_TOKEN)
                .removeValue();
        FirebaseAuth.getInstance().signOut();
    }

    public void onStop() {
        if (query != null)
            query.removeEventListener(valueEventListener);
    }

    public void updateUser(User user) {
        profileInterface.showWait();
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS).child(user.getId())
                .setValue(user)
                .addOnCompleteListener(task -> {
                    profileInterface.removeWait();
                    if (task.isSuccessful())
                        profileInterface.updateUserComplete();
                    else
                        profileInterface.getUserFailure(task.getException().getMessage());
                });
    }

    void updatePayment() {
        profileInterface.showWait();
        FirebaseDatabase.getInstance().getReference().child(REF_USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(REF_PAYMENT_STATUS)
                .child("status")
                .setValue(true)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                        getUser();
                    else {
                        profileInterface.removeWait();
                        profileInterface.getUserFailure(task.getException().getMessage());
                    }
                });
    }
}
