package youcode.com.carclub.ui.newItems;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.yalantis.ucrop.UCrop;

import java.util.Calendar;

import pub.devrel.easypermissions.EasyPermissions;
import youcode.com.carclub.R;
import youcode.com.carclub.general.BaseActivity;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventLocation;
import youcode.com.carclub.networking.NetworkOperations;
import youcode.com.carclub.utils.DateUtils;

import static youcode.com.carclub.general.Constants.RC_CAMERA_AND_LOCATION;


public class AddEventActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, CreateEventInterface, GoogleApiClient.ConnectionCallbacks {
    public static final String EXTRA_TYPE = "extra_type";

    public static final int TYPE_EVENT = 1;
    public static final int TYPE_PRODUCT = 2;
    public static final int TYPE_NEWS = 3;
    private static final String TAG = AddEventActivity.class.getSimpleName() + "xxxxxxxxxxxxxxxx";
    private static final int REQUEST_CHECK_SETTINGS = 11;

    private TextView txt_pick_up_location, txt_destination_location, txt_pick_time, txt_departure_time, txt_event_date, txt_confirmation_date;
    private EditText et_title, et_description, et_child_price, et_price, et_price_non_member, et_route, et_meeting_point, et_departure_point, et_menu;
    private FrameLayout fr_pick_img;
    private ImageView img_event;
    private LinearLayout ll_from, ll_to, ll_route, ll_menu, ll_confirmation;
    private Spinner sp_menu_cigar;

    private Event event = new Event();

    private Calendar calendar, selectedDateCalendar, departureDateCalendar;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;

    private CreateEventPresenter createEventPresenter;

    private Uri imgUri;

    int max = 0;

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private LocationSettingsRequest.Builder builder;
    private PendingResult<LocationSettingsResult> result;

    public static void startActivity(Activity oldActivity, int type) {
        Intent intent = new Intent(oldActivity, AddEventActivity.class);
        intent.putExtra(EXTRA_TYPE, type);
        oldActivity.startActivity(intent);
    }


    public static final int PLACE_PICKER_REQUEST_TO = 1;
    public static final int PLACE_PICKER_REQUEST_FROM = 2;

    private ProgressDialog progressDialog;
    private ProgressDialog horizontalProgressDialog;

    private NetworkOperations networkOperations;
    private Spinner spinner;

    private RadioGroup radioGroup;
    private RadioButton rb_paid, rb_free;


    private boolean isEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        isEdit = getIntent().getBooleanExtra(Constants.EXTRA_IS_EDIT, false);
        calendar = Calendar.getInstance();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        spinner = findViewById(R.id.spinner);
        sp_menu_cigar = findViewById(R.id.sp_menu_cigar);


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.events_type));


        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    et_child_price.setVisibility(View.VISIBLE);
                    et_child_price.setVisibility(View.VISIBLE);
                    et_price_non_member.setVisibility(View.VISIBLE);
                    ll_from.setVisibility(View.VISIBLE);
                    ll_to.setVisibility(View.VISIBLE);
                    ll_menu.setVisibility(View.VISIBLE);
                    ll_route.setVisibility(View.VISIBLE);
                    ll_confirmation.setVisibility(View.VISIBLE);
                    et_price.setHint(getString(R.string.hint_adult_price));
                    et_price.setVisibility(View.VISIBLE);
                    radioGroup.setVisibility(View.VISIBLE);
                } else {
                    ll_from.setVisibility(View.GONE);
                    ll_to.setVisibility(View.GONE);
                    ll_menu.setVisibility(View.GONE);
                    ll_route.setVisibility(View.GONE);
                    ll_confirmation.setVisibility(View.GONE);
                    et_child_price.setVisibility(View.GONE);
                    et_price_non_member.setVisibility(View.GONE);
                    et_price.setHint(getString(R.string.hint_price));
                    rb_paid.setChecked(true);
                    radioGroup.setVisibility(View.GONE);
                }

                if (i == 1) {
                    et_price.setVisibility(View.VISIBLE);
                } else if (i == 2) {
                    et_price.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        init();
        initLocation();


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.message_please_wait));

        if (!EasyPermissions.hasPermissions(this, Constants.CAMERA_PERMISSIONS)) {


            EasyPermissions.requestPermissions(
                    new pub.devrel.easypermissions.PermissionRequest.Builder(this, RC_CAMERA_AND_LOCATION, Constants.CAMERA_PERMISSIONS)
                            .setRationale(R.string.camera_and_location_rationale)
                            .setPositiveButtonText("OK")
                            .setNegativeButtonText("CANCEL")
                            .setTheme(R.style.AlertDialogTheme)
                            .build());

        }
           /* EasyPermissions.requestPermissions(this, getString(R.string.camera_and_location_rationale),
                    RC_CAMERA_AND_LOCATION, Constants.CAMERA_PERMISSIONS);*/

        if (isEdit) {
            event = getIntent().getParcelableExtra(Constants.EXTRA_EVENT);
            spinner.setEnabled(false);
            initEvent();
        }

    }

    private void initEvent() {

        Glide.with(this).load(event.getImgUrl()).into(img_event);
        et_title.setText(event.getTitle());
        et_description.setText(event.getDescription());
        txt_event_date.setText(Utils.getDateFromMillis(event.getDate(), Constants.FORMAT_FULL_DATE));
        et_meeting_point.setText(event.getFrom().getMeetingPoint());
        et_departure_point.setText(event.getTo().getDeparturePoint());
        txt_pick_up_location.setText(event.getFrom().getMeetingPoint());
        txt_destination_location.setText(event.getTo().getDeparturePoint());
        txt_pick_time.setText(Utils.getDateFromMillis(event.getFrom().getMeetingTime(), DateUtils.LONG_DATE_5));
        txt_departure_time.setText(Utils.getDateFromMillis(event.getTo().getDepartureTime(), DateUtils.LONG_DATE_5));
        et_route.setText(event.getRoute());
        et_menu.setText(event.getMenu());
        txt_confirmation_date.setText(Utils.getDateFromMillis(event.getConfirmation(), Constants.FORMAT_FULL_DATE));
        et_price.setText(event.getPrice());
        et_price_non_member.setText(event.getPriceNonMember());
        et_child_price.setText(event.getChildPrice());
        rb_free.setChecked(event.getEventType() == 1);
        sp_menu_cigar.setSelection(event.getInvitationType());

    }

    private int getSpinnerIndex() {
        return spinner.getSelectedItemPosition() + 1;
    }

    private void initLocation() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        googleApiClient.registerConnectionCallbacks(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
    }

    private void init() {

        ll_from = findViewById(R.id.ll_from);
        ll_to = findViewById(R.id.ll_to);
        ll_route = findViewById(R.id.ll_route);
        ll_menu = findViewById(R.id.ll_menu);
        ll_confirmation = findViewById(R.id.ll_confirmation);


        et_menu = findViewById(R.id.et_menu);
        txt_confirmation_date = findViewById(R.id.txt_confirmation_date);
        et_meeting_point = findViewById(R.id.et_meeting_point);
        et_departure_point = findViewById(R.id.et_departure_point);
        et_route = findViewById(R.id.et_route);
        txt_pick_up_location = findViewById(R.id.txt_pick_up_location);
        txt_destination_location = findViewById(R.id.txt_destination_location);
        txt_departure_time = findViewById(R.id.txt_departure_time);
        txt_event_date = findViewById(R.id.txt_event_date);
        txt_pick_time = findViewById(R.id.txt_pick_time);
        fr_pick_img = findViewById(R.id.fr_image);
        img_event = findViewById(R.id.img_event);
        et_description = findViewById(R.id.et_description);
        et_title = findViewById(R.id.et_title);
        et_price = findViewById(R.id.et_price);
        et_price_non_member = findViewById(R.id.et_price_non_member);
        et_child_price = findViewById(R.id.et_child_price);
        txt_pick_up_location.setOnClickListener(view1 -> requestLocation(PLACE_PICKER_REQUEST_FROM));
        txt_destination_location.setOnClickListener(view1 -> requestLocation(PLACE_PICKER_REQUEST_TO));
        txt_pick_time.setOnClickListener(view1 -> pickDate());
        txt_departure_time.setOnClickListener(view1 -> pickDepartureDate());
        fr_pick_img.setOnClickListener(view1 -> requestImagePick());
        txt_confirmation_date.setOnClickListener(view1 -> pickConfirmationDate());
        createEventPresenter = new CreateEventPresenter(this);

        radioGroup = findViewById(R.id.radioGroup);
        rb_free = findViewById(R.id.rb_free);
        rb_paid = findViewById(R.id.rb_paid);

        rb_free.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                et_price.setEnabled(false);
                et_price.setText("0");
                et_child_price.setText("");
                et_child_price.setEnabled(false);
                et_price_non_member.setText("");
                et_price_non_member.setEnabled(false);
            }
        });

        rb_paid.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                et_price.setEnabled(true);
                et_child_price.setEnabled(true);
                et_price_non_member.setEnabled(true);
                et_price.setText("");
                et_child_price.setText("");
                et_price_non_member.setText("");
            }
        });

        txt_event_date.setOnClickListener(v -> pickEventDate());
    }

    private void pickConfirmationDate() {
        new DatePickerDialog(this, R.style.AlertDialogTheme, (view, year, month, dayOfMonth) -> {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, year);
            calendar1.set(Calendar.MONTH, month);
            calendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            TimePickerDialog timePickerDialog = new TimePickerDialog(AddEventActivity.this, R.style.AlertDialogTheme, (view1, hourOfDay, minute) -> {

                calendar1.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar1.set(Calendar.MINUTE, minute);
                txt_confirmation_date.setText(Utils.getDateFromMillis(calendar1.getTimeInMillis(), Constants.FORMAT_FULL_DATE));
                event.setConfirmation(calendar1.getTimeInMillis());
            }, calendar.get(Calendar.HOUR_OF_DAY), Calendar.MINUTE, DateFormat.is24HourFormat(AddEventActivity.this));

            timePickerDialog.show();


        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    private void pickEventDate() {
        new DatePickerDialog(this, R.style.AlertDialogTheme, (view, year, month, dayOfMonth) -> {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.set(Calendar.YEAR, year);
            calendar1.set(Calendar.MONTH, month);
            calendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            event.setDate(calendar1.getTimeInMillis());
            txt_event_date.setText(Utils.getDateFromMillis(calendar1.getTimeInMillis(), Constants.FORMAT_HALF_DATE));

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    private void requestImagePick() {
       /* ImagePicker.setIsSquare(true);
        ImagePicker.pickImage(this);*/
        ImagePicker.create(this)
                .single()// Activity or Fragment
                .start();
    }

    private void pickDate() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(AddEventActivity.this, R.style.AlertDialogTheme, (view, hourOfDay, minute) -> {
            if (selectedDateCalendar == null)
                selectedDateCalendar = Calendar.getInstance();
            selectedDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            selectedDateCalendar.set(Calendar.MINUTE, minute);
            txt_pick_time.setText(Utils.getDateFromMillis(selectedDateCalendar.getTimeInMillis(), DateUtils.LONG_DATE_5));
        }, calendar.get(Calendar.HOUR_OF_DAY), Calendar.MINUTE, DateFormat.is24HourFormat(AddEventActivity.this));

        timePickerDialog.show();

    }

    private void pickDepartureDate() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(AddEventActivity.this, R.style.AlertDialogTheme, (view, hourOfDay, minute) -> {
            if (departureDateCalendar == null)
                departureDateCalendar = Calendar.getInstance();
            departureDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            departureDateCalendar.set(Calendar.MINUTE, minute);
            txt_departure_time.setText(Utils.getDateFromMillis(departureDateCalendar.getTimeInMillis(), DateUtils.LONG_DATE_5));
        }, calendar.get(Calendar.HOUR_OF_DAY), Calendar.MINUTE, DateFormat.is24HourFormat(AddEventActivity.this));

        timePickerDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST_TO || requestCode == PLACE_PICKER_REQUEST_FROM) {
            if (resultCode == RESULT_OK) {
                if (requestCode == PLACE_PICKER_REQUEST_FROM) {
                    Place place = PlacePicker.getPlace(this, data);
                    txt_pick_up_location.setText(place.getName());
                    EventLocation eventLocation = new EventLocation();
                    eventLocation.setAddress(place.getName().toString());
                    event.getFrom().setLat(place.getLatLng().latitude);
                    event.getFrom().setLng(place.getLatLng().longitude);
                    txt_destination_location.setVisibility(View.VISIBLE);
                } else {
                    Place place = PlacePicker.getPlace(this, data);
                    txt_destination_location.setText(place.getName());
                    event.getTo().setLat(place.getLatLng().latitude);
                    event.getTo().setLng(place.getLatLng().longitude);
                }
            }
        }/* else if (resultCode == Activity.RESULT_OK && requestCode == ImagePicker.REQUEST_PICK) {
            Utils.startCrop(this, resultCode, data);
        }*/ else if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images

            Image image = ImagePicker.getFirstImageOrNull(data);
            Utils.startCrop(this, resultCode, image.getPath());
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            imgUri = UCrop.getOutput(data);

            Glide.with(this).load(imgUri).into(img_event);
        }
    }

    private void createEvent() {
        event.setTitle(et_title.getText().toString());
        event.setDescription(et_description.getText().toString());
        if (!Utils.isNullOrEmpty(et_price.getText().toString()))
            event.setPrice(et_price.getText().toString());
        else
            event.setPrice("300");

        if (!Utils.isNullOrEmpty(et_price_non_member.getText().toString()))
            event.setPriceNonMember(et_price_non_member.getText().toString());
        else
            event.setPriceNonMember("0");

        if (!Utils.isNullOrEmpty(et_child_price.getText().toString()))
            event.setChildPrice(et_child_price.getText().toString());
        else
            event.setChildPrice("0");

        event.setType(getSpinnerIndex());

        event.setEventType(rb_paid.isChecked() ? 0 : 1);

        event.setRoute(et_route.getText().toString());

        event.setStatus(true);
        event.getFrom().setMeetingPoint(et_meeting_point.getText().toString());
        event.getTo().setDeparturePoint(et_departure_point.getText().toString());


        if (selectedDateCalendar != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(event.getDate());
            calendar.set(Calendar.HOUR_OF_DAY, selectedDateCalendar.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, selectedDateCalendar.get(Calendar.MINUTE));
            event.getFrom().setMeetingTime(calendar.getTimeInMillis());
        } else
            event.getFrom().setMeetingTime(0);

        if (departureDateCalendar != null) {
            calendar.set(Calendar.HOUR_OF_DAY, departureDateCalendar.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, departureDateCalendar.get(Calendar.MINUTE));
            event.getTo().setDepartureTime(calendar.getTimeInMillis());
        } else
            event.getTo().setDepartureTime(0);
        event.setMenu(et_menu.getText().toString());
        event.setInvitationType(sp_menu_cigar.getSelectedItemPosition());
        if (valid(event))
            createEventPresenter.createEvent(event, imgUri, getSpinnerIndex());


    }

    private boolean valid(Event event) {
        boolean isValid = true;
        et_menu.setError(null);
        et_route.setError(null);
        if (imgUri == null && !isEdit) {
            isValid = false;
            Utils.showToast(this, getString(R.string.error_pick_image), Toast.LENGTH_SHORT);
        } else if (Utils.isNullOrEmpty(event.getTitle())) {
            isValid = false;
            Utils.showToast(this, getString(R.string.error_pick_title), Toast.LENGTH_SHORT);
        } else if (Utils.isNullOrEmpty(event.getDescription())) {
            isValid = false;
            Utils.showToast(this, getString(R.string.error_pick_description), Toast.LENGTH_SHORT);
        }
        if (spinner.getSelectedItemPosition() == 2) {
            if (event.getDate() == 0) {
                isValid = false;
                Utils.showToast(this, getString(R.string.error_pick_date), Toast.LENGTH_SHORT);
            }
        } else if (spinner.getSelectedItemPosition() == 0) {

           /* if (event.getFrom().isLocationEmpty() || event.getFrom().isMeetingPointEmpty() || event.getFrom().isMeetingTimeEmpty()) {
                isValid = false;
                Utils.showToast(this, getString(R.string.fill_all_meeting_form), Toast.LENGTH_SHORT);
            } else*/
            /*if (event.getTo().isLocationEmpty() || event.getTo().isDeparturePointEmpty() || event.getTo().isDepartureTimeEmpty()) {
                isValid = false;
                Utils.showToast(this, getString(R.string.fill_all_departure_form), Toast.LENGTH_SHORT);
            } else*/
            if (event.getDate() == 0) {
                isValid = false;
                Utils.showToast(this, getString(R.string.error_pick_date), Toast.LENGTH_SHORT);
            } else if (et_menu.getText().toString().trim().isEmpty()) {
                isValid = false;
                et_menu.setError(getString(R.string.error_empty_field));
            }/* else if (et_route.getText().toString().trim().isEmpty()) {
                isValid = false;
                et_route.setError(getString(R.string.error_empty_field));
            }*/ else if (event.getConfirmation() == 0) {
                isValid = false;
                Utils.showToast(this, getString(R.string.enter_confirmation_date), Toast.LENGTH_SHORT);
            }

        }
        return isValid;
    }


    private void requestLocation(int rquestCode) {
        displayLocationSettingsRequest(rquestCode);

    }

    private void startPickLocationActivity(int requestCoe) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(AddEventActivity.this), requestCoe);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
            Utils.showToast(this, getString(R.string.error_location), Toast.LENGTH_LONG);
        }
    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Utils.showToast(this, connectionResult.getErrorMessage(), Toast.LENGTH_LONG);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void createSuccess() {
        if (!isEdit) {
            if (networkOperations == null)
                networkOperations = new NetworkOperations();
            if (getSpinnerIndex() == TYPE_EVENT)
                networkOperations.sendNotification(Constants.PATH_TOPIC);
            finish();
        } else {
            setResult(Activity.RESULT_OK);
            finish();
        }


    }

    @Override
    public void createFailure(String errorMessage) {
        Utils.showToast(this, errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void uploadMax(int max) {
        if (max == 0) {
            this.max = max;
            horizontalProgressDialog.setMax(max);
        }
    }

    @Override
    public void uploadProgress(int progress) {
        horizontalProgressDialog.setProgress(progress);
    }

    @Override
    public void uploadStarted() {
        if (horizontalProgressDialog == null)
            horizontalProgressDialog = new ProgressDialog(this);
        horizontalProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        horizontalProgressDialog.show();
    }

    @Override
    public void uploadFinished() {
        max = 0;
        horizontalProgressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_event, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_event:
                createEvent();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "ONCONNECTED");

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "SUSPENDED " + i);
    }

    private void displayLocationSettingsRequest(int requestCode) {
        result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(result1 -> {
            final Status status = result1.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    startPickLocationActivity(requestCode);
                    Log.i(TAG, "All location settings are satisfied.");
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        status.startResolutionForResult(AddEventActivity.this, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        Log.i(TAG, "PendingIntent unable to execute request.");
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Utils.showToast(this, getString(R.string.error_location), Toast.LENGTH_SHORT);
                    break;


            }
        });

    }


}
