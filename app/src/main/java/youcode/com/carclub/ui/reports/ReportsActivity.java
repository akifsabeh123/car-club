package youcode.com.carclub.ui.reports;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import jxl.CellView;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import pub.devrel.easypermissions.EasyPermissions;
import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventPayments;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.User;
import youcode.com.carclub.utils.DateUtils;
import youcode.com.carclub.utils.SubscriptionUtils;

import static youcode.com.carclub.general.Constants.RC_CAMERA_AND_LOCATION;

public class ReportsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, ReportsInterface {
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;
    private String TAG = ReportsActivity.class.getSimpleName();
    private DatabaseReference databaseReference;
    private ReportsPresenter reportsPresenter;
    private ProgressDialog progressDialog;
    private List<Event> events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.listView);
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, getResources().getStringArray(R.array.reports_list_items));

        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
        reportsPresenter = new ReportsPresenter(this);
        progressDialog = Utils.customizeProgressDialog(this, getString(R.string.message_please_wait));

        if (!EasyPermissions.hasPermissions(this, Constants.STORAGE_PERMISSIONS))
            EasyPermissions.requestPermissions(this, getString(R.string.storage_rationale),
                    RC_CAMERA_AND_LOCATION, Constants.STORAGE_PERMISSIONS);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        if (!EasyPermissions.hasPermissions(this, Constants.STORAGE_PERMISSIONS))
            EasyPermissions.requestPermissions(this, getString(R.string.storage_rationale),
                    RC_CAMERA_AND_LOCATION, Constants.STORAGE_PERMISSIONS);
        else {
            switch (i) {
                case 0:
                    reportsPresenter.getAllUsers();
                    break;

                case 1:
                    reportsPresenter.getEventsParticipation();
                    break;

                case 2:
                    reportsPresenter.getEvents(FirebaseDatabase.getInstance().getReference().child(Constants.REF_EVENTS));
                    break;

                case 3:
                    reportsPresenter.getEvents(FirebaseDatabase.getInstance().getReference().child(Constants.REF_PRODUCTS));
                    break;

                case 4:
                    reportsPresenter.getEvents(FirebaseDatabase.getInstance().getReference().child(Constants.REF_NEWS));
                    break;


            }
        }
    }


    private void openFile(String fileName) {
        File file = new File(fileName);
        Uri path = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".youcode.com.carclub.provider", file);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(path , "application/vnd.ms-excel");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
       /* intent.setDataAndType(path, "text/csv");*/
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Utils.showToast(this, "No applications found to open this file", Toast.LENGTH_LONG);
        }
       /* if (intent.resolveActivityInfo(getPackageManager(), 0) != null) {
            startActivity(intent);
        } else {


        }*/
    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }


    @Override
    public void getEventDataSuccess(List<EventPayments> eventPurchases) {
        saveToCSV(eventPurchases);
    }


    @Override
    public void getDataFailure(String errorMessage) {
        Utils.showToast(this, errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void getUsersSuccess(List<User> users) {
      /*  try {
            saveFileToCSV2(toStringArrayHeader(new String[]{"Name", "Family Name", "Mobile", "Membership Code", "Email", "Car", "Model", "Color", " Subscription end date"}), toStringArrayUser(users));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "An error has occured", Toast.LENGTH_LONG).show();
        }*/
        try {
            saveFileToCSV2(toStringArrayHeader(new String[]{"Mobile", "Membership Code", " Subscription end date"}), toStringArrayUser(users));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "An error has occured", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void getEventsSuccess(List<Event> events) {
        this.events = events;
        List<String> strings = new ArrayList<>();
        for (Event event1 : events)
            strings.add(event1.getTitle());
        new MaterialDialog.Builder(this)
                .itemsCallbackSingleChoice(-1, (dialog, itemView, which, text) -> {
                    Event event = events.get(which);
                    reportsPresenter.findAnalyticForSingleEvent(event);
                    return false;
                }).items(strings)
                .show();
        // saveFileToCSV(toStringArrayHeader(new String[]{"Title", "Date", "Location", "Price"}), toStringArrayEvent(events));
    }

    @Override
    public void getSingleEventAnalyticsSuccess(EventPayments eventPayments) {
        List<EventPayments> eventPayments1 = new ArrayList<>();
        eventPayments1.add(eventPayments);
        saveToCSV(eventPayments1);

    }

    private void saveFileToCSV2(List<String[]> header, List<String[]> body) throws Exception {
        String fileName = Utils.getExcelFilePath();
        WritableWorkbook mySecondWbook = null;
        try {

            mySecondWbook = Workbook.createWorkbook(new File(fileName));
            WritableSheet myFirstSheet = mySecondWbook.createSheet("Sheet 1", 0);
            WritableCellFormat cFormat = new WritableCellFormat();
            WritableFont font = new WritableFont(WritableFont.ARIAL, 16, WritableFont.BOLD);
            cFormat.setFont(font);
            for (int j = 0; j < header.size(); j++) {
                String[] headers = header.get(j);
                for (int i = 0; i < headers.length; i++) {
                    Label label = new Label(i, j, headers[i]);
                    myFirstSheet.addCell(label);
                }
                expandColumn(myFirstSheet, header.get(j).length);
            }


            for (int j = 0; j < body.size(); j++) {
                String[] headers = body.get(j);
                for (int i = 0; i < headers.length; i++) {
                    String s = headers[i];
                    if (s == null)
                        s = "";
                  /*  if (isNumeric(s)) {
                        WritableCellFormat writableCellFormat = new WritableCellFormat(NumberFormats.INTEGER);
                       *//* if (s.startsWith("+961"))
                            s = s.replaceFirst("\\+961", "");*//*
                        Number label = new Number(i, j + 1, Double.parseDouble(s), writableCellFormat);
                        myFirstSheet.addCell(label);
                    } else {*/
                    Label label = new Label(i, j + 1, s);
                    myFirstSheet.addCell(label);
                    // }
                }
                //expandColumn(myFirstSheet, header.get(j).length);
            }


            mySecondWbook.write();

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();

        } finally {

            if (mySecondWbook != null) {
                try {
                    mySecondWbook.close();
                } catch (IOException e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                } catch (WriteException e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            new MaterialDialog.Builder(this)
                    .title(getString(R.string.message_report_saved))
                    .positiveText(getString(R.string.action_open))
                    .onPositive((dialog, which) -> openFile(fileName)).show();
        }
    }

    private void expandColumn(WritableSheet sheet, int amountOfColumns) {
        int c = amountOfColumns;
        for (int x = 0; x < c; x++) {
            CellView cell = sheet.getColumnView(x);
            cell.setAutosize(true);
            sheet.setColumnView(x, cell);
        }
    }


    private List<String[]> toStringArrayUser(List<User> users) {
        List<String[]> records = new ArrayList<>();
        Iterator<User> it = users.iterator();
        while (it.hasNext()) {
            User user = it.next();
            long date = user.getSubscriptionStatus() == null ? SubscriptionUtils.findClosestYear() : user.getSubscriptionStatus().getDate();
            //records.add(new String[]{user.getName(), user.getFamilyName(), user.getMobile(), user.getPorscheCode(), user.getEmail(), user.getCarName(), user.getCarModel(), user.getCarColor(), DateUtils.formatTime(date, DateUtils.LONG_DATE_SLASH)});
            records.add(new String[]{user.getMobile(), user.getPorscheCode(), DateUtils.formatTime(date, DateUtils.LONG_DATE_SLASH)});
        }
        records.add(new String[]{"\n"});
        records.add(new String[]{"\n"});
        return records;
    }

    private List<String[]> toStringArrayEvent(List<Event> events) {
        List<String[]> records = new ArrayList<>();
        Iterator<Event> it = events.iterator();
        while (it.hasNext()) {
            Event event = it.next();
            // records.add(new String[]{event.getTitle(), Utils.getDateFromMillis(event.getDate(), Constants.FORMAT_DAY), event.getEventLocation().getAddress(), event.getPrice()});
        }
        records.add(new String[]{"\n"});
        records.add(new String[]{"\n"});
        return records;
    }


    private void saveToCSV(List<EventPayments> eventPurchases) {
      /*  String fileName = Utils.getExcelFilePath();
        File file = new File(fileName);

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CSVWriter csvWriter = null;
        try {
            csvWriter = new CSVWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        try {
            WritableWorkbook mySecondWbook = null;
            String fileName = Utils.getExcelFilePath();
            mySecondWbook = Workbook.createWorkbook(new File(fileName));
            List<String[]> header;
            WritableSheet myFirstSheet = mySecondWbook.createSheet("Sheet 1", 0);
            WritableCellFormat cFormat = new WritableCellFormat();
            WritableFont font = new WritableFont(WritableFont.ARIAL, 16, WritableFont.BOLD);
            cFormat.setFont(font);
            int row = 0;
            for (int z = 0; z < eventPurchases.size(); z++) {
                EventPayments payments = eventPurchases.get(z);
                if (payments.getEvent().getType() == 1)
                    header = toStringArrayHeader(new String[]{"Event name : " + payments.getEvent().getTitle()});
                else
                    header = toStringArrayHeader(new String[]{"Product name : " + payments.getEvent().getTitle()});

                List<EventTicket> eventTickets = payments.getEventTickets();
                Collections.sort(eventTickets, (lhs, rhs) -> {
                    if (lhs.getPurchaseDate() >= rhs.getPurchaseDate()) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
                payments.setEventTickets(eventTickets);
                List<String[]> body = toStringArrayBody(payments.getEventTickets(), payments.getEvent().getType());

                for (int j = 0; j < header.size(); j++) {
                    String[] headers = header.get(j);
                    for (int i = 0; i < headers.length; i++) {
                        Label label = new Label(i, row, headers[i]);
                        myFirstSheet.addCell(label);
                    }
                    expandColumn(myFirstSheet, header.get(j).length);
                }
                row++;


                for (int j = 0; j < body.size(); j++) {
                    String[] headers = body.get(j);
                    for (int i = 0; i < headers.length; i++) {
                        String s = headers[i];
                        if (s == null)
                            s = "";
                      /*  if (isNumeric(s)) {
                            WritableCellFormat writableCellFormat = new WritableCellFormat(NumberFormats.INTEGER);
                            writableCellFormat.setShrinkToFit(false);
                            Number label = new Number(i, row, Double.parseDouble(s),writableCellFormat);
                            myFirstSheet.addCell(label);
                        } else {*/
                        Label label = new Label(i, row, s);
                        myFirstSheet.addCell(label);
                        //}
                    }
                    row++;

                    // csvWriter.writeAll(body);
                }
                row += 2;

            }
            mySecondWbook.write();

            try {
                mySecondWbook.close();
            } catch (IOException e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            } catch (WriteException e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            new MaterialDialog.Builder(this)
                    .title(getString(R.string.message_report_saved))
                    .positiveText(getString(R.string.action_open))
                    .onPositive((dialog, which) -> openFile(fileName)).show();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();

        }
    }

    private static List<String[]> toStringArrayHeader(String[] header) {
        List<String[]> eventHeader = new ArrayList<>();
        eventHeader.add(header);
        return eventHeader;
    }

    private static List<String[]> toStringArrayBody(List<EventTicket> pair, int type) {
        List<String[]> records = new ArrayList<>();

        // adding header record
        if (type == 1)
            records.add(new String[]{"Name", "Family Name", "Mobile", "Car", "Membership Code", "Members", "Non members", "Children", "Price", "Payment number", "Purchase Date", "Status"});
        else
            records.add(new String[]{"Name", "Family Name", "Mobile", "Car", "Membership Code", "Price", "Payment number", "Purchase Date"});

        Iterator<EventTicket> it = pair.iterator();
        while (it.hasNext()) {
            EventTicket eventTicket = it.next();
            if (type == 1)
                records.add(new String[]{eventTicket.getUser().getName(), eventTicket.getUser().getFamilyName(), eventTicket.getUser().getMobile(), eventTicket.getUser().getCarModel(), eventTicket.getUser().getPorscheCode(), eventTicket.getPax() + "", eventTicket.getNonMemberPax() + "", eventTicket.getChildPax() + "", eventTicket.getPrice() + "", eventTicket.getTransactionNumber(), Utils.getDateFromMillis(eventTicket.getPurchaseDate(), Constants.FORMAT_FULL_DATE), eventTicket.isCancelled() ? "Cancelled" : "Confirmed"});
            else
                records.add(new String[]{eventTicket.getUser().getName(), eventTicket.getUser().getFamilyName(), eventTicket.getUser().getMobile(), eventTicket.getUser().getCarModel(), eventTicket.getUser().getPorscheCode(), eventTicket.getPrice() + "", eventTicket.getTransactionNumber(), Utils.getDateFromMillis(eventTicket.getPurchaseDate(), Constants.FORMAT_FULL_DATE)});

        }
        records.add(new String[]{"\n"});
        records.add(new String[]{"\n"});
        return records;
    }

    private boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
