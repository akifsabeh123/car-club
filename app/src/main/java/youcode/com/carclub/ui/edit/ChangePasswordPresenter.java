package youcode.com.carclub.ui.edit;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Desk1 on 9/5/2017.
 */

public class ChangePasswordPresenter {
    private ChangePasswordInterface changePasswordInterface;
    private FirebaseUser firebaseUser;

    public ChangePasswordPresenter(ChangePasswordInterface changePasswordInterface) {
        this.changePasswordInterface = changePasswordInterface;
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    public void changePassword(String oldPassword, String newPassword) {

        changePasswordInterface.showWait();

        AuthCredential credential = EmailAuthProvider
                .getCredential(firebaseUser.getEmail(), oldPassword);

        firebaseUser.reauthenticate(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        firebaseUser.updatePassword(newPassword).addOnCompleteListener(task1 -> {
                            changePasswordInterface.removeWait();
                            if (task1.isSuccessful()) {
                                changePasswordInterface.changePasswordSuccess();
                            } else {
                                changePasswordInterface.changePasswordFailure(task.getException().getMessage());
                            }
                        });
                    } else {
                        changePasswordInterface.removeWait();
                        changePasswordInterface.changePasswordFailure(task.getException().getMessage());
                    }
                });

    }
}
