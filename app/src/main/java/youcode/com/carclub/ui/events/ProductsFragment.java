package youcode.com.carclub.ui.events;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Event;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductFragmentListener} interface
 * to handle interaction events.
 * Use the {@link ProductsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductsFragment extends Fragment {


    private ProductFragmentListener mListener;

    private RecyclerView recyclerView;

    public ProductsFragment() {
        // Required empty public constructor
    }


    public static ProductsFragment newInstance() {
        ProductsFragment fragment = new ProductsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_event, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        Query query = FirebaseDatabase.getInstance().getReference().child(Constants.REF_PRODUCTS).orderByChild(Constants.REF_INVERTED_DATE);
        ProductsRecyclerAdapter productsRecyclerAdapter = new ProductsRecyclerAdapter(Event.class, R.layout.item_product, ProductsRecyclerAdapter.MyViewHolder.class, query, mListener);
        recyclerView.setAdapter(productsRecyclerAdapter);
         /*else {
            Query query = FirebaseDatabase.getInstance().getReference().child(Constants.REF_NEWS);
            NewsRecyclerAdapter newsRecyclerAdapter = new NewsRecyclerAdapter(News.class, R.layout.item_news, NewsRecyclerAdapter.MyViewHolder.class, query,mListener);
            recyclerView.setAdapter(newsRecyclerAdapter);
        }*/

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProductFragmentListener) {
            mListener = (ProductFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NewsFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface ProductFragmentListener {
        void onItemClick(Event event, int type);

        void onBuyClicked(Event event);
    }
}
