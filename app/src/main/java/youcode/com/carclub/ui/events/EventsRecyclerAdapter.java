package youcode.com.carclub.ui.events;

/**
 * Created by Desk1 on 6/13/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.ui.newItems.AddEventActivity;

public class EventsRecyclerAdapter<T extends RecyclerView.ViewHolder> extends FirebaseRecyclerAdapter<Event, EventsRecyclerAdapter.MyViewHolder> {

    private EventsFragment.EventFragmentListener mListener;

    /**
     * @param modelClass      Firebase will marshall the data at a location into an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list. You will be responsible for populating an
     *                        instance of the corresponding view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                        combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     * @param mListener
     */
    public EventsRecyclerAdapter(Class<Event> modelClass, int modelLayout, Class<MyViewHolder> viewHolderClass, Query ref, EventsFragment.EventFragmentListener mListener) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.mListener = mListener;
    }

    @Override
    protected void populateViewHolder(MyViewHolder viewHolder, Event event, int position) {
        Glide.with(viewHolder.itemView.getContext()).load(event.getImgUrl()).into(viewHolder.img1);
        viewHolder.txt_date.setText(Utils.getDateFromMillis(event.getDate(), Constants.FORMAT_MINI_DATE));
        viewHolder.txt_description.setText(event.getDescription());
        //viewHolder.txt_address.setText(event.getEventLocation().getAddress());
        //Glide.with(viewHolder.itemView.getContext()).applyDefaultRequestOptions(Utils.getRequestOptions()).load(Utils.getLocationImage(event.getEventLocation().getLatLng())).into(viewHolder.img_location);
        viewHolder.itemView.setOnClickListener(view -> mListener.onItemClick(event, AddEventActivity.TYPE_EVENT));
        viewHolder.btn_join.setOnClickListener(view -> mListener.onBuyClicked(event));
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img1, img_location;
        View fullView;
        TextView txt_date, txt_description, txt_address;
        Button btn_join;

        public MyViewHolder(View view) {
            super(view);
            fullView = view;
            img1 = (ImageView) view.findViewById(R.id.img1);
            img_location = (ImageView) view.findViewById(R.id.img_location);
            txt_date = (TextView) view.findViewById(R.id.txt_date);
            txt_description = (TextView) view.findViewById(R.id.txt_description);
            txt_address = (TextView) view.findViewById(R.id.txt_address);
            btn_join = (Button) view.findViewById(R.id.btn_join);

        }
    }

}