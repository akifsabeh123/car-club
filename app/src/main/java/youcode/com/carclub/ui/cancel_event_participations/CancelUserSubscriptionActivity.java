package youcode.com.carclub.ui.cancel_event_participations;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventPayments;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.reports.ReportsInterface;
import youcode.com.carclub.ui.reports.ReportsPresenter;

public class CancelUserSubscriptionActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, ReportsInterface, DialogInterface.OnDismissListener, SearchView.OnQueryTextListener, android.widget.SearchView.OnQueryTextListener {
    private ListView listView;
    private ArrayAdapter<Element> arrayAdapter;
    private String TAG = CancelUserSubscriptionActivity.class.getSimpleName();
    private DatabaseReference databaseReference;
    private ReportsPresenter reportsPresenter;
    private ProgressDialog progressDialog;
    private List<Event> events;
    private List<Element> users = new ArrayList<>();
    private List<Element> usersFiltered = new ArrayList<>();
    private EventPayments eventPayments;
    private android.widget.SearchView searchView;
    private MenuItem searchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.listView);
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, usersFiltered);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(this);
        reportsPresenter = new ReportsPresenter(this);
        progressDialog = Utils.customizeProgressDialog(this, getString(R.string.message_please_wait));

        reportsPresenter.getEvents(FirebaseDatabase.getInstance().getReference().child(Constants.REF_EVENTS));

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        EventTicket eventTicket = null;
        Element element = (Element) adapterView.getItemAtPosition(i);
        for (int j = 0; j < users.size(); j++) {
            if (users.get(j).getId().equals(element.getId())) {
                eventTicket = eventPayments.getEventTickets().get(j);
                break;
            }

        }
        if (eventTicket != null) {
            EventTicket finalEventTicket = eventTicket;
            new MaterialDialog.Builder(this)
                    .title("Remove participation")
                    .content("Are you sure you want to remove " + finalEventTicket.getUser().getPorscheCode() + "'s participation ?")
                    .positiveText("Yes")
                    .onPositive((dialog, which) -> {
                        String userRef = finalEventTicket.getUser().getName().isEmpty() ? finalEventTicket.getUser().getPorscheCode() : finalEventTicket.getUser().getId();
                        FirebaseDatabase.getInstance().getReference().child("purchases/" + userRef + "/" + finalEventTicket.getId())
                                .child("cancelled")
                                .setValue(true)
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        Utils.showToast(this, "Success", Toast.LENGTH_SHORT);
                                        finish();
                                    } else {
                                        Utils.showToast(this, task.getException().getMessage(), Toast.LENGTH_SHORT);
                                    }
                                });
                    })
                    .show();
        }

    }


    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }


    @Override
    public void getEventDataSuccess(List<EventPayments> eventPurchases) {

    }


    @Override
    public void getDataFailure(String errorMessage) {
        Utils.showToast(this, errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void getUsersSuccess(List<User> users) {


    }

    @Override
    public void getEventsSuccess(List<Event> events) {
        this.events = events;
        List<String> strings = new ArrayList<>();
        for (Event event1 : events)
            strings.add(event1.getTitle());
        MaterialDialog.Builder dialog = new MaterialDialog.Builder(this)
                .itemsCallbackSingleChoice(-1, (dialog1, itemView, which, text) -> {
                    Event event = events.get(which);
                    reportsPresenter.findAnalyticForSingleEvent(event);
                    return false;
                }).negativeText(getString(R.string.action_cancel))
                .onNegative((dialog1, which) -> finish()).items(strings);
        dialog.cancelable(false);
        dialog.show();
        // saveFileToCSV(toStringArrayHeader(new String[]{"Title", "Date", "Location", "Price"}), toStringArrayEvent(events));
    }

    @Override
    public void getSingleEventAnalyticsSuccess(EventPayments eventPayments) {
        usersFiltered.clear();
        users.clear();
        this.eventPayments = eventPayments;
        for (EventTicket eventTicket : eventPayments.getEventTickets()) {
            if (!eventTicket.isCancelled()) {
                String title = "Membership code : " + eventTicket.getUser().getPorscheCode() + ", Mobile = : " + eventTicket.getUser().getMobile() + ", Purchase date : " + Utils.getDateFromMillis(eventTicket.getPurchaseDate(), Constants.FORMAT_FULL_DATE);
                Element element
                        = new Element(title, eventTicket.getId());
                users.add(element);
                usersFiltered.add(element);
            }
        }
        arrayAdapter.notifyDataSetChanged();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        searchView = (android.widget.SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        usersFiltered.clear();
        if (newText.isEmpty())
            usersFiltered.addAll(users);
        else
            for (Element s : users) {
                if (s.getmText().contains(newText))
                    usersFiltered.add(s);
            }
        arrayAdapter.notifyDataSetChanged();

        // use to enable search view popup text
//        if (TextUtils.isEmpty(newText)) {
//            friendListView.clearTextFilter();
//        }
//        else {
//            friendListView.setFilterText(newText.toString());
//        }

        return true;
    }

    public static class Element {
        private String mText;
        private String mId;

        public Element(String text, String id) {
            mText = text;
            mId = id;
        }

        public String getId() {
            return mId;
        }

        public void setId(String id) {
            mId = id;
        }

        public String getmText() {
            return mText;
        }

        public void setmText(String mText) {
            this.mText = mText;
        }

        @Override
        public String toString() {
            return mText;
        }
    }
}
