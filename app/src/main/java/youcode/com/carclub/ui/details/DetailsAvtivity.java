package youcode.com.carclub.ui.details;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.FireBaseItem;
import youcode.com.carclub.purchases.PurchasesFragment;
import youcode.com.carclub.ui.ProfileBillingHistoryFragment;
import youcode.com.carclub.ui.edit.EditPaymentActivity;
import youcode.com.carclub.ui.edit.EditProfileActivity;
import youcode.com.carclub.ui.events.ui.EventActivity;
import youcode.com.carclub.ui.payment.PaymentInfoFragment;
import youcode.com.carclub.ui.profile.ProfileFragment;
import youcode.com.carclub.ui.ticket.EventTicketDialog;
import youcode.com.carclub.ui.ticket.TicketsFragment;

public class DetailsAvtivity extends AppCompatActivity implements RemoteDataFragment.OnFragmentInteractionListener, TicketsFragment.TicketFragmentInteractionListener, ProfileFragment.OnFragmentInteractionListener, PaymentInfoFragment.OnFragmentInteractionListener, PurchasesFragment.PurchaseFragmentInteractionListener {

    public static final String EXTRA_QUERY = "extra_query";
    public static final String EXTRA_ITEM = "extra_item";

    public static void startDetailsActivity(Activity context, View view, FireBaseItem fireBaseItem, String query) {
        Intent intent = new Intent(context, DetailsAvtivity.class);
        intent.putExtra(EXTRA_QUERY, query);
        intent.putExtra(EXTRA_ITEM, fireBaseItem);
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(context,
                        view,
                        ViewCompat.getTransitionName(view));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            context.startActivity(intent, options.toBundle());
        else
            context.startActivity(intent);
    }

    private ImageView img;
    private TextView txt_title;
    private FireBaseItem fireBaseItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_avtivity);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        fireBaseItem = getIntent().getParcelableExtra(EXTRA_ITEM);
        init();
    }

    private void init() {
        img = (ImageView) findViewById(R.id.img);
        img.setOnClickListener(view -> finishActivity());


        txt_title = (TextView) findViewById(R.id.txt_title);
        Picasso.with(this).load(fireBaseItem.getImageId()).into(img);
        txt_title.setText(fireBaseItem.getTitle());
        Fragment fragment = null;
        switch (fireBaseItem.getType()) {
            case Constants.TYPE_EVENT:
            case Constants.TYPE_NEWS:
            case Constants.TYPE_PRODUCT:
                fragment = RemoteDataFragment.newInstance(getIntent().getStringExtra(EXTRA_QUERY));
                break;
            case Constants.TYPE_HISTORY:
            case Constants.TYPE_PROFILE_BILLING:
                fragment = ProfileBillingHistoryFragment.newInstance();
                break;
        }
        if (fragment != null)
            getSupportFragmentManager().beginTransaction()/*.setCustomAnimations(R.anim.fragment_slide_up, 0)*/.add(R.id.frameLayout, fragment).commit();
    }
/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finishActivity();
                break;


        }
        return super.onOptionsItemSelected(item);
    }*/

    private void finishActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else
            finish();
    }

    @Override
    public void RemoteDataFragmentInteraction(Event event, int type) {
        EventActivity.startActivity(this, event, type);
    }

    @Override
    public void onTicketClick(EventTicket eventTicket,int ticketNumber) {
        showEventTicket(eventTicket,ticketNumber);
    }

    private void showEventTicket(EventTicket eventTicket, int ticketNumber) {
        EventTicketDialog eventTicketDialog = EventTicketDialog.newInstance(eventTicket,ticketNumber);
        eventTicketDialog.show(getSupportFragmentManager(), EventTicketDialog.class.getSimpleName());
    }

    @Override
    public void onPaymentVisible() {

    }

    @Override
    public void showEditPayment() {
        startActivity(new Intent(this, EditPaymentActivity.class));
    }

    @Override
    public void onProfileVisible() {

    }

    @Override
    public void showEditProfile() {
        startActivity(new Intent(this, EditProfileActivity.class));
    }
}
