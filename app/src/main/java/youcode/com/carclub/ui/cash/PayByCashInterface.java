package youcode.com.carclub.ui.cash;

import java.util.List;

import youcode.com.carclub.general.GeneralInterface;
import youcode.com.carclub.models.Client;
import youcode.com.carclub.models.Event;

/**
 * Created by Akif sabeh on 2/1/2019.
 */
public interface PayByCashInterface extends GeneralInterface {
    void getEventsSuccess(List<Event> events);

    void onError(String message);

    void getUsersSuccess(List<Client> list);

    void onComplete();
}
