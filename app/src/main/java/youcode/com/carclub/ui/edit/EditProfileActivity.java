package youcode.com.carclub.ui.edit;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import youcode.com.carclub.R;
import youcode.com.carclub.SpinnerAdapter;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.SpinnerData;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.profile.ProfileInterface;
import youcode.com.carclub.ui.profile.ProfilePresenter;

public class EditProfileActivity extends AppCompatActivity implements ProfileInterface, ChangePasswordDialog.OnFragmentInteractionListener, CompoundButton.OnCheckedChangeListener {

    private ProfilePresenter profilePresenter;
    private ProgressDialog progressDialog;
    private EditText /*txt_full_name,*/ txt_mobile, txt_username;
    private EditText txt_name, txt_family_name, txt_email;
    private Spinner sp_car_name, sp_car_model, sp_car_color;
    ArrayList<SpinnerData> spinnerDataArrayList = new ArrayList<>();
    private User user;
    private LinearLayout ll_change_password;

    private AppCompatRadioButton rb_yes;
    private AppCompatRadioButton rb_no;
    private String[] cars;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_edit_pofile);
        init();
        Completable.timer(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> profilePresenter.getUser());
    }

    private void init() {
        // txt_full_name = (EditText) findViewById(R.id.txt_full_name);
        txt_name = (EditText) findViewById(R.id.txt_name);
        txt_family_name = (EditText) findViewById(R.id.txt_family_name);
        txt_email = (EditText) findViewById(R.id.txt_email);
        txt_mobile = (EditText) findViewById(R.id.txt_mobile);
        txt_username = (EditText) findViewById(R.id.txt_username);
        sp_car_model = (Spinner) findViewById(R.id.sp_car_year);
        sp_car_name = (Spinner) findViewById(R.id.sp_car_model);
        sp_car_color = (Spinner) findViewById(R.id.sp_car_color);
        ll_change_password = (LinearLayout) findViewById(R.id.ll_change_password);

        ll_change_password.setOnClickListener(view -> showChangePasswordDialog());


        sp_car_model.setAdapter(new SpinnerAdapter(
                this,
                Utils.getSpinnerCarDates()));
        Utils.initSpinner(sp_car_color, spinnerDataArrayList);

        rb_yes = (AppCompatRadioButton) findViewById(R.id.rb_yes);
        rb_no = (AppCompatRadioButton) findViewById(R.id.rb_no);


        rb_yes.setOnCheckedChangeListener(this);
        rb_no.setOnCheckedChangeListener(this);


        profilePresenter = new ProfilePresenter(this);
        progressDialog = Utils.customizeProgressDialog(this, getString(R.string.message_please_wait));
    }

    private void showChangePasswordDialog() {
        ChangePasswordDialog changePasswordDialog = ChangePasswordDialog.newInstance();
        changePasswordDialog.show(getSupportFragmentManager(), ChangePasswordDialog.class.getSimpleName());
    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();

    }

    @Override
    public void getUserSuccess(User user) {
        this.user = user;
        init(user);

    }

    @Override
    public void getUserFailure(String errorMessage) {
        Utils.showToast(this, errorMessage, Toast.LENGTH_LONG);

    }

    @Override
    public void updateUserComplete() {
        finish();
    }

    @Override
    public void getDateSuccess(long date) {

    }

    @Override
    public void getCarsSuccess(List<String> list) {
        this.cars = list.toArray(new String[list.size()]);
        sp_car_name.setAdapter(new SpinnerAdapter(
                this,
                cars));
    }


    private void init(User user) {
        txt_name.setText(Utils.isNullOrEmpty(user.getName()) ? "" : user.getName());
        txt_family_name.setText(Utils.isNullOrEmpty(user.getFamilyName()) ? "" : user.getFamilyName());
        txt_email.setText(Utils.isNullOrEmpty(user.getEmail()) ? "" : user.getEmail());
        txt_mobile.setText(user.getMobile());
        txt_username.setText(user.getUsername());

        sp_car_name.setSelection(Utils.findPosition(user.getCarName(), cars));
        sp_car_model.setSelection(Utils.findPosition(user.getCarModel(), Utils.getSpinnerCarDates()));
        sp_car_color.setSelection(Utils.findPosition(user.getCarColor(), Utils.getCarColors()));
        rb_no.setChecked(!user.isAutoRenew());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_done:
                updateUser();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateUser() {
        if (valid()) {
            user.setName(txt_name.getText().toString());
            user.setFamilyName(txt_family_name.getText().toString());
            user.setEmail(txt_email.getText().toString());
            user.setMobile(txt_mobile.getText().toString());
            user.setCarName(cars[sp_car_name.getSelectedItemPosition()]);
            user.setCarModel(Utils.getSpinnerCarDates()[sp_car_model.getSelectedItemPosition()]);
            user.setCarColor(spinnerDataArrayList.get(sp_car_color.getSelectedItemPosition()).getText());
            user.setAutoRenew(rb_yes.isChecked());
            profilePresenter.updateUser(user);
        }
    }

    private boolean valid() {
        boolean valid = true;

        if (Utils.isNullOrEmpty(txt_name.getText().toString())) {
            valid = false;
            txt_name.setError(getString(R.string.error_empty_field));
        }
        if (Utils.isNullOrEmpty(txt_family_name.getText().toString())) {
            valid = false;
            txt_family_name.setError(getString(R.string.error_empty_field));
        }
        if (!isEmailValid(txt_email.getText().toString())) {
            valid = false;
            txt_email.setError(getString(R.string.error_email));
        }

        if (Utils.isNullOrEmpty(txt_mobile.getText().toString())) {
            valid = false;
            txt_mobile.setError(getString(R.string.error_empty_field));
        }

        return valid;
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onChangePasswordSuccess() {

        Utils.showToast(this, getString(R.string.message_successfuly_changed_password), Toast.LENGTH_SHORT);

    }

    @Override
    public void onChangePasswordFailure(String errorMessage) {
        Utils.showToast(this, errorMessage, Toast.LENGTH_SHORT);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        profilePresenter.onStop();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            switch (compoundButton.getId()) {
                case R.id.rb_yes:
                    rb_no.setChecked(false);
                    break;
                case R.id.rb_no:
                    rb_yes.setChecked(false);
                    break;
            }
        }
    }
}
