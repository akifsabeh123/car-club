package youcode.com.carclub.ui.login;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Client;
import youcode.com.carclub.models.Payment;
import youcode.com.carclub.models.User;

/**
 * Created by user on 8/22/2017.
 */

public class LoginPresenter {

    private final String TAG = LoginPresenter.class.getSimpleName();

    public static final int STATE_INITIALIZED = 1;
    public static final int STATE_CODE_SENT = 2;
    public static final int STATE_VERIFY_FAILED = 3;
    public static final int STATE_VERIFY_SUCCESS = 4;
    public static final int STATE_SIGNIN_FAILED = 5;
    public static final int STATE_SIGNIN_SUCCESS = 6;
    public static final int STATE_UPDATE_USER_SUCCESS = 7;
    public static final int STATE_DONE = 8;

    private FirebaseAuth mAuth;
    private LoginInterface loginInterface;

    private boolean mVerificationInProgress = false;
    private String mVerificationId;

    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private Activity activity;

    public LoginPresenter(Activity activity, LoginInterface loginInterface) {
        this.activity = activity;
        this.loginInterface = loginInterface;
        mAuth = FirebaseAuth.getInstance();
        initCallBack();
    }

    private void initCallBack() {
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]

                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                loginInterface.updateUI(STATE_VERIFY_SUCCESS, credential);
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
// This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                loginInterface.removeWait();
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    loginInterface.showMobileError("Invalid phone number.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    loginInterface.showError("you have exceeded the limit");

                    // [END_EXCLUDE]
                }

                // Show a message and update the UI
                // [START_EXCLUDE]
                loginInterface.updateUI(STATE_VERIFY_FAILED);
                // [END_EXCLUDE]
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                loginInterface.removeWait();
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                // [START_EXCLUDE]
                // Update UI
                loginInterface.updateUI(STATE_CODE_SENT);
                // [END_EXCLUDE]
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(String s) {
                super.onCodeAutoRetrievalTimeOut(s);
            }
        };
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");

                        FirebaseUser user = task.getResult().getUser();
                        // [START_EXCLUDE]
                        //loginInterface.updateUI(STATE_SIGNIN_SUCCESS, user);
                        FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS)
                                .child(user.getUid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        if (dataSnapshot.exists() && dataSnapshot.getValue() != null)
                                            loginInterface.updateUI(STATE_DONE, user);
                                        else {
                                            loginInterface.removeWait();
                                            loginInterface.updateUI(STATE_SIGNIN_SUCCESS, user);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        loginInterface.removeWait();
                                        loginInterface.showError(databaseError.getMessage());
                                    }
                                });

                        // [END_EXCLUDE]
                    } else {
                        // Sign in failed, display a message and update the UI
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            // The verification code entered was invalid
                            // [START_EXCLUDE silent]
                            loginInterface.showError("Invalid code.");
                            // [END_EXCLUDE]
                        }
                        // [START_EXCLUDE silent]
                        // Update UI
                        loginInterface.updateUI(STATE_SIGNIN_FAILED);
                        // [END_EXCLUDE]
                    }
                });
    }

    public void loginUser(String username, String password) {
        loginInterface.showWait();
        username = username + Constants.EMAIL_COMPLETION;
        mAuth.signInWithEmailAndPassword(username, password)
                .addOnCompleteListener(task -> {
                    handleTaskCompletion(task);
                });

    }

    private void handleTaskCompletion(Task<AuthResult> task) {
        if (!task.isSuccessful()) {
            loginInterface.removeWait();
            loginInterface.loginFailure(task.getException().getMessage());
        } else {
            updateFirebaseToken();
        }
    }

    public void updateFirebaseToken() {
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constants.REF_TOKEN)
                .setValue(FirebaseInstanceId.getInstance().getToken())
                .addOnCompleteListener(task -> {
                    loginInterface.removeWait();
                    if (!task.isSuccessful())
                        loginInterface.loginFailure(task.getException().getMessage());
                    else {
                        loginInterface.loginSuccess();
                    }
                });
    }

    public void checkIfUserExist(String inputCode, String inputMobile) {
        loginInterface.showWait();
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_CLIENTS)
                .child(inputCode)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        loginInterface.removeWait();
                        boolean exist = false;
                        if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                            Client client = dataSnapshot.getValue(Client.class);
                            String mobile = client.getPhone();
                            exist = mobile.equals(inputMobile);
                            loginInterface.getClientSuccess(client);
                        }

                        loginInterface.checkIfUserExistComplete(exist);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        loginInterface.removeWait();
                        loginInterface.checkIfUserExistError(databaseError.getMessage());
                    }
                });
    }


    private boolean validatePhoneNumber(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            loginInterface.showMobileError("Invalid phone number.");
            return false;
        }

        return true;
    }

    public void startVerification(String number) {
        if (!validatePhoneNumber(number)) {
            return;
        }
        loginInterface.showWait();

        startPhoneNumberVerification(number);
    }

    public void verifyPhone(String code) {
        if (TextUtils.isEmpty(code)) {
            loginInterface.showCodeError("Cannot be empty.");
            return;
        }
        loginInterface.showWait();
        verifyPhoneNumberWithCode(mVerificationId, code);
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                activity,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]
    }

    public void resendCode(String number) {
        resendVerificationCode(number, mResendToken);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                activity, mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    public void updateUser(User user) {
        loginInterface.showWait();
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.REF_USERS)
                .child(user.getId())
                .setValue(user)
                .addOnCompleteListener(task -> {
                    loginInterface.removeWait();
                    if (task.isComplete())
                        loginInterface.updateUI(STATE_UPDATE_USER_SUCCESS);
                    else
                        loginInterface.showError(task.getException().getMessage());
                });
    }

    public void updatePayment(Payment payment) {
        loginInterface.showWait();
        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        payment.setPaymentId(id);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.REF_PAYMENT)
                .child(id);

        databaseReference.setValue(payment)
                .addOnCompleteListener(task -> {
                    loginInterface.removeWait();
                    if (task.isSuccessful())
                        loginInterface.updateUI(STATE_DONE);
                    else
                        loginInterface.showError(task.getException().getMessage());
                });
    }

    public void getCars() {
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_CARS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<String> list = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren())
                            list.add((String) snapshot.getValue());
                        loginInterface.getCarsSuccess(list);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
}
