package youcode.com.carclub.ui.membership;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.User;

/**
 * Created by Desk1 on 8/30/2017.
 */

public class AddMembersPresenter {

    private AddMembersInterface addMembersInterface;
    private FirebaseDatabase firebaseDatabase;
    Query query;
    private FirebaseUser firebaseUser;

    public AddMembersPresenter(AddMembersInterface addMembersInterface) {
        this.addMembersInterface = addMembersInterface;
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    public void searchUser(String nickName) {
        addMembersInterface.showWait();
        query = firebaseDatabase.getReference().child(Constants.REF_USERS).orderByChild(Constants.REF_USER_NAME).equalTo(nickName);
        query.addListenerForSingleValueEvent(valueEventListener);

    }

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            addMembersInterface.removeWait();
            if (dataSnapshot.getValue() == null)
                addMembersInterface.noUserFound();
            else {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    if (!user.getId().equals(firebaseUser.getUid()))
                        addMembersInterface.getUserSuccess(user);
                    else
                        addMembersInterface.noUserFound();
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            addMembersInterface.removeWait();
            addMembersInterface.getUserFailure(databaseError.getMessage());
        }
    };

    public void onStop() {
        if (query != null)
            query.removeEventListener(valueEventListener);
    }
}