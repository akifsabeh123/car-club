package youcode.com.carclub.ui.edit;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChangePasswordDialog.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class ChangePasswordDialog extends DialogFragment implements ChangePasswordInterface {

    private EditText et_old_password;
    private EditText et_new_password;
    private EditText et_confirm_password;
    private Button btn_complete;

    private ChangePasswordPresenter changePasswordPresenter;


    private OnFragmentInteractionListener mListener;

    public ChangePasswordDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_Alert);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_password_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        et_old_password = (EditText) view.findViewById(R.id.et_old_password);
        et_new_password = (EditText) view.findViewById(R.id.et_new_password);
        et_confirm_password = (EditText) view.findViewById(R.id.et_confirm_password);
        btn_complete = (Button) view.findViewById(R.id.btn_complete);

        changePasswordPresenter = new ChangePasswordPresenter(this);

        btn_complete.setOnClickListener(view1 -> changePassword());


    }

    private void changePassword() {
        if (valid()) {
            changePasswordPresenter.changePassword(et_old_password.getText().toString(), et_new_password.getText().toString());
        }
    }

    private boolean valid() {
        boolean valid = true;

        if (Utils.isNullOrEmpty(et_old_password.getText().toString()) || et_old_password.getText().toString().length() < 6) {
            et_old_password.setError(getString(R.string.error_password));
            valid = false;
        }
        if (Utils.isNullOrEmpty(et_new_password.getText().toString()) || et_new_password.getText().toString().length() < 6) {
            et_new_password.setError(getString(R.string.error_password));
            valid = false;
        }
        if (Utils.isNullOrEmpty(et_confirm_password.getText().toString()) || et_confirm_password.getText().toString().length() < 6) {
            et_confirm_password.setError(getString(R.string.error_password));
            valid = false;
        } else if (!et_new_password.getText().toString().equals(et_confirm_password.getText().toString())) {
            et_confirm_password.setError(getString(R.string.error_password_mismatch));
            et_new_password.setError(getString(R.string.error_password_mismatch));
            valid = false;
        }

        return valid;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public static ChangePasswordDialog newInstance() {
        ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog();
        return changePasswordDialog;
    }

    @Override
    public void showWait() {
        mListener.showWait();
    }

    @Override
    public void removeWait() {
        mListener.removeWait();

    }

    @Override
    public void changePasswordSuccess() {
        dismiss();
        mListener.onChangePasswordSuccess();

    }

    @Override
    public void changePasswordFailure(String errorMessage) {
        mListener.onChangePasswordFailure(errorMessage);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onChangePasswordSuccess();

        void onChangePasswordFailure(String errorMessage);

        void showWait();

        void removeWait();
    }
}
