package youcode.com.carclub.ui.ticket;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventTicket;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TicketsFragment.TicketFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TicketsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TicketsFragment extends Fragment {


    private TicketFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private ValueEventListener valueEventListener;
    private Query query;
    private TextView txt_no_data;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TicketsAdapter eventsRecyclerAdapter;

    public TicketsFragment() {
        // Required empty public constructor
    }

    public static TicketsFragment newInstance() {
        TicketsFragment fragment = new TicketsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tickets, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        swipeRefreshLayout = view.findViewById(R.id.swipe);
        txt_no_data = view.findViewById(R.id.txt_no_data);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.REF_PURCHASES);
        // ref.keepSynced(true);
        query = ref.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).orderByChild("status").equalTo(true);
        //  TicketsRecyclerAdapter<RecyclerView.ViewHolder> eventsRecyclerAdapter = new TicketsRecyclerAdapter<>(EventTicket.class, R.layout.item_ticket, TicketsRecyclerAdapter.MyViewHolder.class, query, mListener);
        eventsRecyclerAdapter = new TicketsAdapter(mListener);
        recyclerView.setAdapter(eventsRecyclerAdapter);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            getData();
            Completable.timer(1, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        swipeRefreshLayout.setRefreshing(false);
                    });
        });
        getData();
    }

    private void getData() {
        if (valueEventListener != null) {
            query.removeEventListener(valueEventListener);
            eventsRecyclerAdapter.clear();
        }

        valueEventListener = query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                eventsRecyclerAdapter.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    try {
                        EventTicket eventTicket = snapshot.getValue(EventTicket.class);
                        if (eventTicket != null && eventTicket.isStatus()) {
                            FirebaseDatabase.getInstance().getReference().child(Constants.REF_EVENTS)
                                    .child(eventTicket.getEventId())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                Event event = dataSnapshot.getValue(Event.class);
                                                if (event.isStatus() && event.getDate() > System.currentTimeMillis()) {
                                                    eventTicket.setEvent(event);
                                                    eventsRecyclerAdapter.addItem(eventTicket);
                                                    hideNoData();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                        }
                    } catch (Exception e) {

                    }
                }
                if (!dataSnapshot.exists() || dataSnapshot.getChildrenCount() == 0)
                    showNoData();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void showNoData() {
        txt_no_data.setVisibility(View.VISIBLE);
    }

    private void hideNoData() {
        txt_no_data.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TicketFragmentInteractionListener) {
            mListener = (TicketFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface TicketFragmentInteractionListener {
        void onTicketClick(EventTicket eventTicket, int ticketNumber);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //query.removeEventListener(valueEventListener);
    }
}
