package youcode.com.carclub.ui.payment;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.Payment;

/**
 * Created by user on 8/22/2017.
 */

public class PaymentPresenter {

    private PaymentInterface paymentInterface;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    public PaymentPresenter(PaymentInterface paymentInterface) {
        this.paymentInterface = paymentInterface;
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    public void updatePayment(Payment payment) {
        paymentInterface.showWait();
        String id = firebaseUser.getUid();
        payment.setPaymentId(id);
        databaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.REF_PAYMENT)
                .child(id);

        databaseReference.setValue(payment)
                .addOnCompleteListener(task -> {
                    paymentInterface.removeWait();
                    handleTaskCompletion(task);
                });
    }

    public void getPayment() {

        paymentInterface.showWait();

        FirebaseDatabase.getInstance().getReference().
                child(Constants.REF_PAYMENT)
                .child(firebaseUser.getUid())
                .addValueEventListener(valueEventListener);

    }

    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            paymentInterface.removeWait();
            Payment payment = dataSnapshot.getValue(Payment.class);
            if (payment != null)
                paymentInterface.getPaymentSuccess(payment);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            paymentInterface.removeWait();
            paymentInterface.paymentError(databaseError.getMessage());
        }
    };

    private void handleTaskCompletion(Task<Void> task) {
        if (!task.isSuccessful())
            paymentInterface.paymentError(task.getException().getMessage());
        else
            paymentInterface.paymentComplete();
    }

    public void onStop() {
        if (databaseReference != null)
            databaseReference.removeEventListener(valueEventListener);
    }

    public void purchaseEvent(EventTicket eventTicket) {
        paymentInterface.showWait();
        List<Completable> completables = new ArrayList<>();
        completables.add(buyTicketCompletable(eventTicket));
        completables.add(updatePurchasedEventCompletable(eventTicket));
        Completable.concat(completables)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> paymentInterface.paymentComplete(), throwable -> paymentInterface.paymentError(throwable.getMessage()));
    /*
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_PURCHASES).child(firebaseUser.getUid()).child(eventTicket.getEvent().getEventId())
                .setValue(eventTicket)
                .addOnCompleteListener(task -> {
                    paymentInterface.removeWait();
                    if (task.isSuccessful())
                        paymentInterface.paymentComplete();
                    else
                        paymentInterface.paymentError(task.getException().getMessage());
                });*/
    }

    private Completable buyTicketCompletable(EventTicket eventTicket) {
        return Completable.create(e -> FirebaseDatabase.getInstance().getReference().child(Constants.REF_PURCHASES).child(firebaseUser.getUid()).child(eventTicket.getEvent().getEventId())
                .setValue(eventTicket)
                .addOnCompleteListener(task -> {
                    paymentInterface.removeWait();
                    if (task.isSuccessful())
                        e.onComplete();
                    else
                        e.onError(task.getException());
                }));
    }

    private Completable updatePurchasedEventCompletable(EventTicket eventTicket) {
        return Completable.create(e -> FirebaseDatabase.getInstance().getReference().child(Constants.REF_STATISTICS).child(Constants.REF_EVENTS_PURCHASES).child(eventTicket.getEvent().getEventId()).child(firebaseUser.getUid()).setValue(firebaseUser.getUid())
                .addOnCompleteListener(task -> {
                    paymentInterface.removeWait();
                    if (task.isSuccessful())
                        e.onComplete();
                    else
                        e.onError(task.getException());
                }));
    }
}

