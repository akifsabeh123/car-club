package youcode.com.carclub.ui.cash;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Client;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.SampleSearchModel;
import youcode.com.carclub.ui.payment.PaymentDialogFragment;
import youcode.com.carclub.utils.DateUtils;

public class PayByCashActivity extends AppCompatActivity implements PayByCashInterface, PaymentDialogFragment.PaymentDialogInterface {
    private PayByCashPresenter payByCashPresenter;
    private List<Event> events;
    private Event selectedEvent;
    private List<Client> users;
    private Client selectedUser;
    private ProgressBar progressBar;
    private boolean isGoingToPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_by_cash);
        progressBar = findViewById(R.id.progressBar);
        payByCashPresenter = new PayByCashPresenter(this);
        start();
    }

    private void start() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(getString(R.string.options))
                .itemsCallbackSingleChoice(-1, (dialog1, itemView, which, text) -> {
                    if (which == 0)
                        payByCashPresenter.getEvents();
                    else
                        payByCashPresenter.getUsers();
                    return true;
                })
                .items(R.array.options)
                .show();
        dialog.setOnCancelListener(dialog1 -> finish());


    }

    @Override
    public void getEventsSuccess(List<Event> events) {
        this.events = events;
        List<String> strings = new ArrayList<>();
        for (Event event : events)
            strings.add(event.getTitle());
        new MaterialDialog.Builder(this)
                .cancelable(false)
                .items(strings)
                .itemsCallbackSingleChoice(-1, (dialog, itemView, which, text) -> {
                    selectedEvent = events.get(which);
                    payByCashPresenter.getUsers();
                    return false;
                })
                .negativeText(getString(R.string.action_cancel))
                .onNegative((dialog, which) -> dismissActivity()).show();
    }

    private void dismissActivity() {
        finish();
    }

    @Override
    public void onError(String message) {
        Utils.showToast(this, message, Toast.LENGTH_LONG);
        finish();
    }

    @Override
    public void getUsersSuccess(List<Client> users) {
        this.users = users;
        //List<String> strings = new ArrayList<>();
        ArrayList<SampleSearchModel> items = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            Client client = users.get(i);
            StringBuilder title = new StringBuilder();
            title.append("Membership Number : " + client.getCode()).append("\nMobile :  ").append(client.getPhone());
            // String name = (user.getFullName() == null || user.getFamilyName().isEmpty()) ? user.getName() + " " + user.getFamilyName() : user.getFullName();
            //  strings.add(title.toString());
            items.add(new SampleSearchModel(title.toString(), client.getCode()));
        }


        SimpleSearchDialogCompat simpleSearchDialogCompat = new SimpleSearchDialogCompat(this, "Search...",
                "Ex. Code, Mobile number", null, items,
                (SearchResultListener<SampleSearchModel>) (dialog, item, position) -> {
                    for (Client client : users) {
                        if (client.getCode().equals(item.getId())) {
                            selectedUser = client;

                            showDatePicker();

                            break;
                        }
                    }
                    dialog.dismiss();
                });
        simpleSearchDialogCompat.setCanceledOnTouchOutside(false);
        simpleSearchDialogCompat.show();
        simpleSearchDialogCompat.setOnCancelListener(dialog -> finish());
        simpleSearchDialogCompat.findViewById(R.id.dummy_background)
                .setOnClickListener(v -> {
                });

     /*   new MaterialDialog.Builder(this)
                .cancelable(false)
                .items(strings)
                .itemsCallbackSingleChoice(-1, (dialog, itemView, which, text) -> {
                    selectedUser = users.get(which);
                    showPayment();
                    return false;
                })
                .negativeText(getString(R.string.action_cancel))
                .onNegative((dialog, which) -> dismissActivity()).show();*/
    }

    private void showDatePicker() {
        View v = getLayoutInflater().inflate(R.layout.cash_date_picker, null, false);
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .customView(v, false)
                .show();
        Button done = v.findViewById(R.id.btn_done);
        EditText editText = v.findViewById(R.id.et_amount);
        DatePicker datePicker = v.findViewById(R.id.datePicker);
        done.setOnClickListener(v1 -> {
            if (editText.getText().toString().isEmpty()) {
                editText.setError(getString(R.string.error_empty_field));
                return;
            }
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, datePicker.getYear());
            calendar.set(Calendar.MONTH, datePicker.getMonth());
            calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
            long date = calendar.getTimeInMillis();
            MaterialDialog d = new MaterialDialog.Builder(this)
                    .title("Update subscription")
                    .content("Are you sure you want to update " + selectedUser.getCode() + "'s subscription to " + DateUtils.formatTime(date, DateUtils.LONG_DATE_SLASH) + "?")
                    .positiveText("YES")
                    .negativeText("CANCEL")
                    .onPositive((dialog1, which) -> {
                        String amount = editText.getText().toString();
                        updateSubscription(amount, date);
                        dialog.dismiss();
                    })
                    .onNegative((dialog1, which) -> finish()).show();
            d.setOnCancelListener(dialog1 -> finish());

        });
        dialog.setOnCancelListener(dialog1 -> finish());
    }

    private void updateSubscription(String amount, long date) {
        selectedUser.setDate(date);
        selectedUser.setAmountPaid(amount);
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.REF_CLIENTS).child(selectedUser.getCode())
                .setValue(selectedUser)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Utils.showToast(this, "Successfully paid", Toast.LENGTH_SHORT);

                    } else {
                        Utils.showToast(this, "An error has occurred, please try again", Toast.LENGTH_SHORT);

                    }
                    finish();
                });

    }

    @Override
    public void onComplete() {
        Utils.showToast(this, "Success", Toast.LENGTH_LONG);
        finish();
    }

    private void showPayment() {
        PaymentDialogFragment paymentDialogFragment = PaymentDialogFragment.newInstance(selectedEvent, true);
        paymentDialogFragment.show(getSupportFragmentManager(), PaymentDialogFragment.class.getSimpleName());

    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPurchase(EventTicket eventTicket) {
        Fragment paymentDialogFragment = getSupportFragmentManager().findFragmentByTag(PaymentDialogFragment.class.getSimpleName());
        if (paymentDialogFragment != null) {
            if (paymentDialogFragment instanceof PaymentDialogFragment) {
                isGoingToPay = true;
                ((PaymentDialogFragment) paymentDialogFragment).dismiss();
            }
        }
        payByCashPresenter.update(eventTicket, selectedUser);
    }

    public void onFinish() {
        if (!isGoingToPay) {
            payByCashPresenter.getUsers();
        }
        isGoingToPay = false;
    }
}
