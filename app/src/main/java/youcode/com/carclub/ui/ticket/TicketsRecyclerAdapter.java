package youcode.com.carclub.ui.ticket;

/**
 * Created by Desk1 on 6/13/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.EventTicket;

public class TicketsRecyclerAdapter<T extends RecyclerView.ViewHolder> extends FirebaseRecyclerAdapter<EventTicket, TicketsRecyclerAdapter.MyViewHolder> {
    private TicketsFragment.TicketFragmentInteractionListener onFragmentInteractionListenerl;

    /**
     * @param modelClass      Firebase will marshall the data at a location into an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list. You will be responsible for populating an
     *                        instance of the corresponding view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                        combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public TicketsRecyclerAdapter(Class<EventTicket> modelClass, int modelLayout, Class<MyViewHolder> viewHolderClass, Query ref, TicketsFragment.TicketFragmentInteractionListener onFragmentInteractionListenerl) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.onFragmentInteractionListenerl = onFragmentInteractionListenerl;
    }

    @Override
    protected void populateViewHolder(MyViewHolder viewHolder, EventTicket event, int position) {
        //  if (event.getEvent().getDate() > System.currentTimeMillis()) {
        viewHolder.txt_ticket_number.setText(viewHolder.itemView.getContext().getString(R.string.label_number, position + 1));
        viewHolder.txt_date_date.setText(Utils.getDateFromMillis(event.getEvent().getDate(), Constants.FORMAT_DAY));
        viewHolder.txt_date_time.setText(Utils.getDateFromMillis(event.getEvent().getDate(), Constants.FORMAT_TIME));
       /* viewHolder.txt_location_from.setText(event.getEvent().getEventLocation().getAddress());
        viewHolder.txt_location_to.setText(event.getEvent().getEventLocation().getDestination());*/
        viewHolder.txt_pax.setText(String.valueOf(event.getPax() + event.getNonMemberPax() + event.getChildPax()));
        viewHolder.txt_cost.setText(viewHolder.itemView.getContext().getString(R.string.label_price_raw, String.valueOf(event.getPrice())));
        viewHolder.txt_event_name.setText(event.getEvent().getTitle());
        int finalPosition = position + 1;
        viewHolder.itemView.setOnClickListener(view -> onFragmentInteractionListenerl.onTicketClick(event, finalPosition));
        if (event.getTransactionNumber().equals(Constants.CASH))
            viewHolder.txt_cash.setVisibility(View.VISIBLE);
        else
            viewHolder.txt_cash.setVisibility(View.GONE);

        viewHolder.txt_date.setText(Utils.getDateFromMillis(event.getEvent().getDate(), Constants.FORMAT_HALF_DATE));
       /* } else
            viewHolder.itemView.setVisibility(View.GONE);*/
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_ticket_number, txt_date_date, txt_date_time, txt_location_from, txt_location_to, txt_pax, txt_cost, txt_event_name, txt_cash, txt_date;

        public MyViewHolder(View view) {
            super(view);
            txt_ticket_number = view.findViewById(R.id.txt_ticket_number);
            txt_date_date = view.findViewById(R.id.txt_date_date);
            txt_date_time = view.findViewById(R.id.txt_date_time);
            txt_location_from = view.findViewById(R.id.txt_location_from);
            txt_location_to = view.findViewById(R.id.txt_location_to);
            txt_pax = view.findViewById(R.id.txt_pax);
            txt_cost = view.findViewById(R.id.txt_cost);
            txt_event_name = view.findViewById(R.id.txt_event_name);
            txt_cash = view.findViewById(R.id.txt_cash);
            txt_date = view.findViewById(R.id.txt_date);


        }
    }


}