package youcode.com.carclub.ui.events.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import youcode.com.carclub.R;
import youcode.com.carclub.general.BaseActivity;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.SharedPrefsUtils;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.LatLng;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.newItems.AddEventActivity;
import youcode.com.carclub.ui.payment.PaymentDialogFragment;
import youcode.com.carclub.ui.ticket.EventTicketDialog;
import youcode.com.carclub.utils.DateUtils;

import static youcode.com.carclub.general.Constants.REF_PURCHASES;
import static youcode.com.carclub.ui.newItems.AddEventActivity.EXTRA_TYPE;
import static youcode.com.carclub.ui.newItems.AddEventActivity.TYPE_EVENT;
import static youcode.com.carclub.ui.newItems.AddEventActivity.TYPE_NEWS;
import static youcode.com.carclub.ui.newItems.AddEventActivity.TYPE_PRODUCT;

public class EventActivity extends BaseActivity implements PaymentDialogFragment.PaymentDialogInterface, EventInterface {

    private static String EXTRA_EVENT = "EXTRA_EVENTS";

    public static void startActivity(Activity oldActivity, Event event, int type) {
        Intent intent = new Intent(oldActivity, EventActivity.class);
        intent.putExtra(EXTRA_EVENT, event);
        intent.putExtra(EXTRA_TYPE, type);
        oldActivity.startActivity(intent);
    }

    private TextView txt_title, txt_description, txt_price, txt_price_child, txt_product_price, txt_price_non_member;
    private ImageView img_car, img_location;
    private Button btn_attend;

    private TextView txt_from, txt_to, txt_meeting_point, txt_meeting_time, txt_route, txt_menu, txt_departure_time, txt_confirmation, txt_label_menu, txt_label_route;
    private LinearLayout ll_event_content, ll_from, ll_to, ll_meeting, ll_meeting_time, ll_departure_time;
    private Event event;
    private int type;

    private ProgressDialog progressDialog;
    private EventPresenter eventPresenter;
    private MenuItem menuItem, menu_delete, menu_edit;
    private boolean isAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        isAdmin = SharedPrefsUtils.getBooleanPreference(this, Constants.IS_ADMIN, false);
        init();
    }

    private void init() {
        eventPresenter = new EventPresenter(this);
        progressDialog = Utils.customizeProgressDialog(this, getString(R.string.message_please_wait));

        txt_title = findViewById(R.id.txt_title);
        txt_label_route = findViewById(R.id.txt_label_route);
        txt_description = findViewById(R.id.txt_description);
        txt_price = findViewById(R.id.txt_price);
        txt_price_child = findViewById(R.id.txt_price_child);
        txt_product_price = findViewById(R.id.txt_product_price);
        txt_price_non_member = findViewById(R.id.txt_price_non_member);
        img_car = findViewById(R.id.img_car);
        img_location = findViewById(R.id.img_location);
        btn_attend = findViewById(R.id.btn_attend);
        ll_event_content = findViewById(R.id.ll_event_content);
        ll_from = findViewById(R.id.ll_from);
        ll_to = findViewById(R.id.ll_to);
        ll_meeting = findViewById(R.id.ll_meeting);
        ll_meeting_time = findViewById(R.id.ll_meeting_time);
        ll_departure_time = findViewById(R.id.ll_departure_time);
        txt_label_menu = findViewById(R.id.txt_label_menu);

        txt_from = findViewById(R.id.txt_from);
        txt_to = findViewById(R.id.txt_to);
        txt_meeting_point = findViewById(R.id.txt_meeting_point);
        txt_meeting_time = findViewById(R.id.txt_meeting_time);
        txt_route = findViewById(R.id.txt_route);
        txt_menu = findViewById(R.id.txt_menu);
        txt_departure_time = findViewById(R.id.txt_departure_time);
        txt_confirmation = findViewById(R.id.txt_confirmation);


        event = getIntent().getParcelableExtra(EXTRA_EVENT);
        // eventPresenter.checkIfAttending(event.getEventId());
        type = getIntent().getIntExtra(EXTRA_TYPE, 1);

        txt_title.setText(event.getTitle());
        txt_description.setText(event.getDescription());

        if (event.getFrom().getLat() == 0 && event.getFrom().getLng() == 0) {
            ll_meeting.setVisibility(View.GONE);
        } else
            txt_meeting_point.setText(event.getFrom().getMeetingPoint());
        txt_to.setText(event.getTo().getDeparturePoint());
        if (TextUtils.isEmpty(event.getRoute())) {
            txt_route.setVisibility(View.GONE);
            txt_label_route.setVisibility(View.GONE);
        } else
            txt_route.setText(event.getRoute());
        txt_menu.setText(event.getMenu());
        if (event.getFrom().getMeetingTime() == 0)
            ll_meeting_time.setVisibility(View.GONE);
        else
            txt_meeting_time.setText(DateUtils.formatTime(event.getFrom().getMeetingTime(), DateUtils.LONG_DATE_5));

        if (event.getTo().getDepartureTime() == 0)
            ll_departure_time.setVisibility(View.GONE);
        else
            txt_departure_time.setText(DateUtils.formatTime(event.getTo().getDepartureTime(), DateUtils.LONG_DATE_5));
        txt_confirmation.setText(getString(R.string.kindly_confirm_your_attendance_by, DateUtils.formatTime(event.getConfirmation(), DateUtils.LONG_DATE_3)));
        Glide.with(this).load(event.getImgUrl()).into(img_car);

        btn_attend.setOnClickListener(view -> showPayment());

        if (event.getType() != Constants.TYPE_EVENT) {
            ll_event_content.setVisibility(View.GONE);
            txt_price.setVisibility(View.GONE);
            txt_price_child.setVisibility(View.GONE);
            txt_price_non_member.setVisibility(View.GONE);
        }

        txt_label_menu.setText(getResources().getStringArray(R.array.menu_cigar)[event.getInvitationType()]);

        ll_meeting.setOnClickListener(v -> startMapActivity(new LatLng(event.getFrom().getLat(), event.getFrom().getLng()), event.getFrom().getMeetingPoint()));
        ll_from.setOnClickListener(v -> startMapActivity(new LatLng(event.getFrom().getLat(), event.getFrom().getLng()), event.getFrom().getMeetingPoint()));
        ll_to.setOnClickListener(v -> startMapActivity(new LatLng(event.getTo().getLat(), event.getTo().getLng()), event.getTo().getDeparturePoint()));

        initTitle(type);

    }

    @SuppressLint("CheckResult")
    private void checkSubscription() {
        Completable.timer(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    java.util.Calendar calendar = java.util.Calendar.getInstance();
                    long date = SharedPrefsUtils.getLongPreference(this, Constants.EXPIRY_DATE, calendar.getTimeInMillis());
                    boolean expiredSubscription = (date - calendar.getTimeInMillis()) < 0;
                    if (expiredSubscription) {
                        if (menuItem != null)
                            menuItem.setVisible(false);
                        btn_attend.setText(getString(R.string.label_membership_expired));
                        btn_attend.setEnabled(false);
                    }
                });

    }

    private void showPayment() {
        if (event.getEventType() == 1) {
            new MaterialDialog.Builder(this)
                    .title(getString(R.string.number_of_tickers))
                    .items(getResources().getStringArray(R.array.single_double))
                    .itemsCallbackSingleChoice(-1, (dialog, itemView, which, text) -> {
                        FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            User user = dataSnapshot.getValue(User.class);
                                            EventTicket eventTicket = new EventTicket();
                                            eventTicket.setStatus(true);
                                            eventTicket.setPax(which + 1);
                                            eventTicket.setCount(which + 1);
                                            eventTicket.setUsername(user.getName());
                                            eventTicket.setUserId(user.getId());
                                            eventTicket.setCar(user.getCarName() + " " + user.getCarModel());
                                            eventTicket.setPrice(0);
                                            eventTicket.setPurchaseDate(System.currentTimeMillis());
                                            eventTicket.setCancelled(false);
                                            eventTicket.setNonMemberPax(0);
                                            eventTicket.setChildPax(0);
                                            eventTicket.setEvent(event);
                                            String refKey = FirebaseDatabase.getInstance().getReference().child(REF_PURCHASES).push().getKey();

                                            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(REF_PURCHASES).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                    .child(refKey);
                                            databaseReference.setValue(eventTicket)
                                                    .addOnCompleteListener(task -> {
                                                        if (task.isSuccessful()) {
                                                            Utils.showToast(EventActivity.this, "Success", Toast.LENGTH_SHORT);
                                                            finish();
                                                        } else {
                                                            Utils.showToast(EventActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT);
                                                        }
                                                    });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Utils.showToast(EventActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT);
                                    }
                                });


                        return false;
                    }).positiveText(getString(R.string.action_done)).show();
        } else {
            PaymentDialogFragment paymentDialogFragment = PaymentDialogFragment.newInstance(event, false);
            paymentDialogFragment.show(getSupportFragmentManager(), PaymentDialogFragment.class.getSimpleName());
        }
    }

    private void initTitle(int type) {
        switch (type) {
            case TYPE_EVENT:
                findViewById(R.id.ll_event_price).setVisibility(View.VISIBLE);
                getSupportActionBar().setTitle(getString(R.string.label_event));
                if (!Utils.isNullOrEmpty(event.getPrice()))
                    txt_price.setText(getString(R.string.label_adult_price, event.getPrice()));
                if (!Utils.isNullOrEmpty(event.getChildPrice()))
                    txt_price_child.setText(getString(R.string.label_child_price, event.getChildPrice()));
                if (!Utils.isNullOrEmpty(event.getPriceNonMember()))
                    txt_price_non_member.setText(getString(R.string.label_non_member_price, event.getPriceNonMember()));
                txt_title.append("\n Date : " + Utils.getDateFromMillis(event.getDate(), Constants.FORMAT_HALF_DATE));
                if (event.getDate() < System.currentTimeMillis() || event.getConfirmation() < System.currentTimeMillis()) {
                    if (menuItem != null)
                        menuItem.setVisible(false);
                    btn_attend.setText(getString(R.string.label_event_expired));
                    btn_attend.setEnabled(false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        btn_attend.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_edittext_grey));
                    }
                } else
                    checkSubscription();
                break;
            case TYPE_PRODUCT:
                getSupportActionBar().setTitle(getString(R.string.label_product));
                img_location.setVisibility(View.GONE);
                btn_attend.setText(getString(R.string.label_buy));
                txt_product_price.setVisibility(View.VISIBLE);
                if (!Utils.isNullOrEmpty(event.getPrice()))
                    txt_product_price.setText(String.format("Price : $%s", event.getPrice()));
                break;
            case TYPE_NEWS:
                getSupportActionBar().setTitle(getString(R.string.label_news));
                btn_attend.setVisibility(View.GONE);
                img_location.setVisibility(View.GONE);
                txt_title.append("\n Date : " + Utils.getDateFromMillis(event.getDate(), Constants.FORMAT_HALF_DATE));
                // btn_attend.setText(getString(R.string.label_details));
                break;
        }
    }

    private boolean lessThan5Days(long date) {
        int days = (int) ((date - System.currentTimeMillis()) / (1000 * 60 * 60 * 24));
        return days < 5;
    }

    private void startMapActivity(LatLng latLng, String address) {
        String geoUri = "http://maps.google.com/maps?q=loc:" + latLng.getLatitude() + "," + latLng.getLongitude() + " (" + address + ")";

        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(geoUri));
        startActivity(intent);
    }


    @Override
    public void onPurchase(EventTicket eventTicket) {
        EventTicketDialog eventTicketDialog = EventTicketDialog.newInstance(eventTicket, 1);
        eventTicketDialog.show(getSupportFragmentManager(), EventTicketDialog.class.getSimpleName());
    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }

    @Override
    public void isAttending(boolean isAttending) {
        btn_attend.setText(isAttending ? getString(R.string.label_attending) : getString(R.string.label_attend));
        if (!isAttending && type == TYPE_EVENT)
            btn_attend.setOnClickListener(view -> {
                PaymentDialogFragment paymentDialogFragment = PaymentDialogFragment.newInstance(event, false);
                paymentDialogFragment.show(getSupportFragmentManager(), PaymentDialogFragment.class.getSimpleName());
            });
    }

    @Override
    public void checkFailure(String message) {
        Utils.showToast(getApplicationContext(), message, Toast.LENGTH_LONG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_join_event, menu);
        menuItem = menu.findItem(R.id.menu_join);
        menu_delete = menu.findItem(R.id.menu_delete);
        menu_edit = menu.findItem(R.id.menu_edit);
        if (event != null && event.getType() == TYPE_EVENT) {
            if (event.getDate() > System.currentTimeMillis())
                menuItem.setVisible(true);
        }
        if (isAdmin) {
            menu_delete.setVisible(true);
            if (event.getType() == Constants.TYPE_EVENT)
                menu_edit.setVisible(true);

        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_join:
                showPayment();
                break;

            case R.id.menu_delete:
                showDeleteOption();
                break;
            case R.id.menu_edit:
                showEdit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void showDeleteOption() {
        new MaterialDialog.Builder(this)
                .title("Alert")
                .content("Are you sure you want to delete?")
                .positiveText("yes")
                .negativeText("cancel")
                .onPositive((dialog, which) -> {
                    String eventType = "";
                    if (event.getType() == Constants.TYPE_EVENT)
                        eventType = Constants.REF_EVENTS;
                    else if (event.getType() == Constants.TYPE_PRODUCT)
                        eventType = Constants.REF_PRODUCTS;
                    else if (event.getType() == Constants.TYPE_NEWS)
                        eventType = Constants.REF_NEWS;
                    FirebaseDatabase.getInstance().getReference().child(eventType).child(event.getEventId()).child("status").setValue(false)
                            .addOnCompleteListener(task -> {
                                if (task.isSuccessful())
                                    finish();
                                else
                                    Utils.showToast(EventActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT);
                            });
                }).show();
    }

    private void showEdit() {
        startActivityForResult(new Intent(this, AddEventActivity.class).putExtra(Constants.EXTRA_IS_EDIT, true).putExtra(Constants.EXTRA_EVENT, event), 99);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99 && resultCode == Activity.RESULT_OK)
            finish();
    }
}
