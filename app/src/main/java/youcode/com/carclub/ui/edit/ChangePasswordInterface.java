package youcode.com.carclub.ui.edit;

import youcode.com.carclub.general.GeneralInterface;

/**
 * Created by Desk1 on 9/5/2017.
 */

public interface ChangePasswordInterface extends GeneralInterface {

    void changePasswordSuccess();

    void changePasswordFailure(String errorMessage);


}
