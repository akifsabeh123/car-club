package youcode.com.carclub.ui.payment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.Payment;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.cash.PayByCashActivity;
import youcode.com.carclub.ui.payfort.PayfortActivity;
import youcode.com.carclub.utils.ProductType;

import static youcode.com.carclub.general.Constants.REF_ITEM_PURCHASES;
import static youcode.com.carclub.general.Constants.REF_PURCHASES;
import static youcode.com.carclub.ui.newItems.AddEventActivity.TYPE_EVENT;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PaymentDialogFragment.PaymentDialogInterface} interface
 * to handle interaction events.
 * Use the {@link PaymentDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentDialogFragment extends DialogFragment implements NumberPicker.OnValueChangeListener, PaymentInterface {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String MEMBERS_COUNT = "MEMBERS_COUNT";
    private NumberPicker numberPicker, numberPickerChild, numberPickerNonMember;
    private TextView txt_price, txt_price_non_member, txt_price_child, txt_payment_title, numberPickerChildTitle, numberPickerAdultTitle, numberPickerAdultNonMemberTitle,
            txt_total_number_of_people;
    private View numberPickerSeparator;
    private Button btn_purchase;

    private EventTicket eventTicket = new EventTicket();


    private PaymentDialogInterface mListener;

    private PaymentPresenter paymentPresenter;

    private ProgressDialog progressDialog;
    private long currentPrice = 0;
    private LinearLayout ll_child, ll_non_member;
    private long adultPrice;
    private long childPrice;
    private long adultPriceNonMember;
    private User user;
    private boolean isCash;
    int membersCount = -1;
    private Event event;

    public PaymentDialogFragment() {
        // Required empty public constructor
    }


    public static PaymentDialogFragment newInstance(Event event, boolean cash) {
        PaymentDialogFragment fragment = new PaymentDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, event);
        args.putBoolean(ARG_PARAM2, cash);
        fragment.setArguments(args);
        return fragment;
    }


    public static PaymentDialogFragment newInstance(Event event, boolean cash, int members) {
        PaymentDialogFragment fragment = new PaymentDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, event);
        args.putBoolean(ARG_PARAM2, cash);
        args.putInt(MEMBERS_COUNT, members);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_Alert);

        if (getArguments() != null) {
            this.event = ((Event) getArguments().getParcelable(ARG_PARAM1));
            eventTicket.setEventId(event.getEventId());
            eventTicket.setPax(1);
            isCash = getArguments().getBoolean(ARG_PARAM2);
            membersCount = getArguments().getInt(MEMBERS_COUNT, -1);
        }
        FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            user = dataSnapshot.getValue(User.class);
                            eventTicket.setUsername(user.getName());
                            eventTicket.setUserId(user.getId());
                            eventTicket.setCar(user.getCarName() + " " + user.getCarModel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            currentPrice = Long.valueOf(event.getPrice());
        } catch (NumberFormatException e) {
            Utils.showToast(getContext(), getString(R.string.error_number_format), Toast.LENGTH_SHORT);
        }
        numberPicker = view.findViewById(R.id.numberPicker);
        txt_price_non_member = view.findViewById(R.id.txt_price_non_member);
        numberPickerChild = view.findViewById(R.id.numberPickerChild);
        numberPickerSeparator = view.findViewById(R.id.numberPickerSeparator);
        numberPickerChildTitle = view.findViewById(R.id.numberPickerChildTitle);
        numberPickerAdultTitle = view.findViewById(R.id.numberPickerAdultTitle);
        numberPickerNonMember = view.findViewById(R.id.numberPickerNonMember);
        numberPickerAdultNonMemberTitle = view.findViewById(R.id.numberPickerAdultNonMemberTitle);
        txt_total_number_of_people = view.findViewById(R.id.txt_total_number_of_people);
        ll_child = view.findViewById(R.id.ll_child);
        ll_non_member = view.findViewById(R.id.ll_non_member);
        if (event.getType() == TYPE_EVENT) {
            if (event.getPriceNonMember() != null && !event.getPriceNonMember().isEmpty() && !event.getPriceNonMember().equalsIgnoreCase("0") && membersCount < 0)
                ll_non_member.setVisibility(View.VISIBLE);
            if (event.getChildPrice() != null && !event.getChildPrice().isEmpty() && !event.getChildPrice().equalsIgnoreCase("0") && membersCount < 0)
                ll_child.setVisibility(View.VISIBLE);
            //ll_child.setVisibility(View.VISIBLE);
            numberPickerSeparator.setVisibility(View.VISIBLE);
            numberPickerAdultTitle.setVisibility(View.VISIBLE);
            numberPickerAdultNonMemberTitle.setVisibility(View.VISIBLE);
            txt_total_number_of_people.setVisibility(View.VISIBLE);
            updateTotalPeople(1);
        }
        txt_price = view.findViewById(R.id.txt_price);
        txt_price_child = view.findViewById(R.id.txt_price_child);
        txt_payment_title = view.findViewById(R.id.txt_payment_title);
        btn_purchase = view.findViewById(R.id.btn_purchase);
        if (isCash)
            btn_purchase.setText(getString(R.string.action_done));
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(1);
        numberPickerNonMember.setMinValue(0);
        numberPickerNonMember.setMaxValue(10);
        numberPicker.setOnValueChangedListener(this);
        numberPickerNonMember.setOnValueChangedListener(this);

        numberPickerChild.setMinValue(0);
        numberPickerChild.setMaxValue(10);
        numberPickerChild.setOnValueChangedListener(this);
        txt_price.setText(getString(R.string.label_price, event.getPrice()));
        txt_price_non_member.setText(getString(R.string.label_price, "0"));
        txt_price_child.setText(getString(R.string.label_price, "0"));
        if (event.getPrice() != null && !event.getPrice().isEmpty())
            adultPrice = Long.parseLong(event.getPrice());
        /*if (event.getPriceNonMember() != null && !event.getPriceNonMember().isEmpty())
            adultPriceNonMember = Long.parseLong(event.getPriceNonMember());*/
        if (event.getType() == Constants.TYPE_PRODUCT)
            txt_payment_title.setText(getString(R.string.label_quantity));
        btn_purchase.setOnClickListener(view1 -> {
            eventTicket.setPurchaseDate(System.currentTimeMillis());
            if (numberPicker.getValue() < 1 && numberPickerNonMember.getValue() < 1 && numberPickerChild.getValue() < 1) {
                return;
            }
            if (isCash) {
                eventTicket.setPrice(adultPrice + adultPriceNonMember + childPrice);
                eventTicket.setTransactionNumber(Constants.CASH);
                eventTicket.setStatus(true);
                mListener.onPurchase(eventTicket);
                return;
            }

           /* if (event.getType() == Constants.TYPE_EVENT)
                showTicketActivity();
            else
                dismiss();*/
            eventTicket.setPrice(adultPrice + adultPriceNonMember + childPrice);
            eventTicket.setCount(numberPicker.getValue() + numberPickerNonMember.getValue() + numberPickerChild.getValue());
            String title = user.getName() + " " + user.getFamilyName();
            if (title.length() > 40)
                title = title.substring(0, 40);

            if (membersCount < 0)
                PayfortActivity.startPaymentActivity(PaymentDialogFragment.this,
                        eventTicket,
                        event.getType() == 1 ? ProductType.EVENT : ProductType.PRODUCT,
                        eventTicket.getPrice(),
                        System.currentTimeMillis() + event.getTitle().trim().replace(" ", ""),
                        event.getType() == 1 ? REF_PURCHASES : REF_ITEM_PURCHASES, title, event.getTitle());
            else {
                eventTicket.setStatus(true);
                String refKey = FirebaseDatabase.getInstance().getReference().child(REF_PURCHASES).push().getKey();
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(REF_PURCHASES).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child(refKey);
                databaseReference.setValue(eventTicket)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Utils.showToast(getContext(), "Success", Toast.LENGTH_SHORT);
                                dismiss();
                            } else {
                                Utils.showToast(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT);
                            }
                        });

            }
        });
        paymentPresenter = new PaymentPresenter(this);
        progressDialog = Utils.customizeProgressDialog(getContext(), getString(R.string.message_please_wait));
        numberPicker.setValue(1);

        if (membersCount > 0) {
            numberPicker.setMaxValue(membersCount);
            numberPicker.setValue(membersCount);
            eventTicket.setPax(membersCount);
            btn_purchase.setText(getString(R.string.action_done));
        }


    }

    private void updateTotalPeople(int i) {

        txt_total_number_of_people.setText(getString(R.string.total_coming, i));
    }

    private void showTicketActivity() {
      /*  EventTicketActivity.startActivity(getActivity(), eventTicket, pax);
        dismiss();*/
        paymentPresenter.purchaseEvent(eventTicket);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PaymentDialogInterface) {
            mListener = (PaymentDialogInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onValueChange(NumberPicker n, int i, int i1) {
        switch (n.getId()) {
            case R.id.numberPicker:
                eventTicket.setPax(i1/* + numberPickerNonMember.getValue()*/);
                adultPrice = (Long.valueOf(event.getPrice())) * i1;
                txt_price.setText(getString(R.string.label_price, String.valueOf((adultPrice))));
                /*if (event.getType() == TYPE_EVENT)
                    childPrice = (Long.valueOf(event.getChildPrice())) * numberPickerChild.getValue();*/
                // currentPrice += (Long.valueOf(event.getPrice())) * i1;
                //txt_price.setText(getString(R.string.label_price, String.valueOf((i1 * Long.valueOf(event.getPrice())) + (Long.valueOf(event.getChildPrice()) * numberPickerChild.getValue()))));
                break;
            case R.id.numberPickerNonMember:
                eventTicket.setNonMemberPax(i1/* + numberPickerNonMember.getValue()*/);
                adultPriceNonMember = (Long.valueOf(event.getPriceNonMember())) * i1;
                txt_price_non_member.setText(getString(R.string.label_price, String.valueOf((adultPriceNonMember))));
                /*if (event.getType() == TYPE_EVENT)
                    childPrice = (Long.valueOf(event.getChildPrice())) * numberPickerChild.getValue();*/
                // currentPrice += (Long.valueOf(event.getPrice())) * i1;
                //txt_price.setText(getString(R.string.label_price, String.valueOf((i1 * Long.valueOf(event.getPrice())) + (Long.valueOf(event.getChildPrice()) * numberPickerChild.getValue()))));
                break;
            case R.id.numberPickerChild:
                eventTicket.setChildPax(i1);
                childPrice = (Long.valueOf(event.getChildPrice())) * i1;
                txt_price_child.setText(getString(R.string.label_price, String.valueOf(childPrice)));
                //txt_price.setText(getString(R.string.label_price, String.valueOf((i1 * Long.valueOf(event.getChildPrice())) + ((Long.valueOf(event.getPrice()) * numberPicker.getValue())))));
                break;

        }
        updateTotalPeople(numberPicker.getValue() + numberPickerNonMember.getValue() + numberPickerChild.getValue());
      /*  txt_price.setText(getString(R.string.label_price, String.valueOf((adultPrice + childPrice))));
        txt_price_child.setText(getString(R.string.label_price, String.valueOf((adultPrice + childPrice))));*/

    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }

    @Override
    public void paymentComplete() {
        mListener.onPurchase(eventTicket);
        dismiss();
    }

    @Override
    public void paymentError(String errorMessage) {
        Utils.showToast(getContext(), errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void getPaymentSuccess(Payment payment) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PayfortActivity.PAYMENT_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    dismiss();
                    showDialog(true);
                } /*else {
                    showDialog(false);
                }*/
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showDialog(boolean success) {
        new MaterialDialog.Builder(getContext())
                .title(getString(R.string.label_payment_status))
                .content(success ? getString(R.string.label_payment_success) : getString(R.string.label_payment_error))
                .positiveText(getString(R.string.label_ok))
                .show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface PaymentDialogInterface {
        void onPurchase(EventTicket eventTicket);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getContext() instanceof PayByCashActivity) {
            ((PayByCashActivity) getContext()).onFinish();
        }
    }
}
