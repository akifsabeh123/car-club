package youcode.com.carclub.ui.payfort;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import youcode.com.carclub.BuildConfig;
import youcode.com.carclub.R;
import youcode.com.carclub.general.BaseActivity;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.PaymentResponse;
import youcode.com.carclub.utils.ProductType;

import static youcode.com.carclub.general.Constants.REF_CLIENTS;
import static youcode.com.carclub.general.Constants.REF_ITEM_PURCHASES;
import static youcode.com.carclub.general.Constants.REF_PAYMENT_STATUS;
import static youcode.com.carclub.general.Constants.REF_PURCHASES;
import static youcode.com.carclub.general.Constants.REF_USERS;


public class PayfortActivity extends BaseActivity {


    private static final String EXTRA_EVENT = "EXTRA_EVENT";
    private static final String EXTRA_MERCHANT_REFERENCE = "EXTRA_MERCHANT_REFERENCE";
    private static final String EXTRA_TYPE = "EXTRA_TYPE";
    private static final String EXTRA_COST = "EXTRA_COST";
    private static final String EXTRA_DATABASE_REF = "EXTRA_DATABASE_REF";
    private static final String EXTRA_DATE = "EXTRA_DATE";
    private static final String EXTRA_CODE = "EXTRA_CODE";
    private static final String EXTRA_CUSTOMER_NAME = "CUSTOMER_NAME";
    private static final String EXTRA_EVENT_NAME = "EXTRA_EVENT_NAME";

    private WebView webView;
    private EventTicket event;
    private String serverReference, transactionReference, itemRef;
    private String merchantReference;
    private String refKey;
    private String databaseRef;
    private ProductType productType;
    private String cost;
    private long subscriptionDate;
    public static final int PAYMENT_CODE = 9898;
    private String code;
    private ProgressBar progressBar;
    private String customerName;
    private String eventName;
    private String price;
    // private DatabaseReference analyticsRef;


    public static void startPaymentActivity(android.support.v4.app.Fragment context, EventTicket eventTicket, ProductType productType, long price, String merchantReference, String databaseRef, String customerName, String title) {
        // showComingSoonDialog(context.getContext());
        Intent intent = new Intent(context.getContext(), PayfortActivity.class);
        intent.putExtra(EXTRA_TYPE, productType);
        intent.putExtra(EXTRA_EVENT, eventTicket);
        intent.putExtra(EXTRA_MERCHANT_REFERENCE, merchantReference);
        intent.putExtra(EXTRA_DATABASE_REF, databaseRef);
        intent.putExtra(EXTRA_COST, price);
        intent.putExtra(EXTRA_CUSTOMER_NAME, customerName);
        intent.putExtra(EXTRA_EVENT_NAME, title);
        context.startActivityForResult(intent, PAYMENT_CODE);
    }

    private static void showComingSoonDialog(Context context) {
        new MaterialDialog.Builder(context)
                .title(context.getString(R.string.app_name))
                .content(context.getString(R.string.label_coming_soon))
                .positiveText(context.getString(R.string.label_ok))
                .show();
    }


    public static void startPaymentActivity(android.support.v4.app.Fragment context, ProductType productType, long price, String merchantReference, String databaseRef, long date, String code, String customerName, String eventName) {
        //showComingSoonDialog(context.getContext());
        Intent intent = new Intent(context.getContext(), PayfortActivity.class);
        intent.putExtra(EXTRA_TYPE, productType);
        intent.putExtra(EXTRA_COST, price);
        intent.putExtra(EXTRA_MERCHANT_REFERENCE, merchantReference);
        intent.putExtra(EXTRA_DATABASE_REF, databaseRef);
        intent.putExtra(EXTRA_DATE, date);
        intent.putExtra(EXTRA_CODE, code);
        intent.putExtra(EXTRA_CUSTOMER_NAME, customerName);
        intent.putExtra(EXTRA_EVENT_NAME, eventName);
        context.startActivityForResult(intent, PAYMENT_CODE);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payfort);
        event = (EventTicket) getIntent().getSerializableExtra(EXTRA_EVENT);
        webView = findViewById(R.id.webView);
        progressBar = findViewById(R.id.progress);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new MyJavaScriptInterface(this), "HtmlViewer");
        cost = (getIntent().getLongExtra(EXTRA_COST, 0) * 100) + "";
        price = (getIntent().getLongExtra(EXTRA_COST, 0))+"";
        databaseRef = getIntent().getStringExtra(EXTRA_DATABASE_REF);
        subscriptionDate = getIntent().getLongExtra(EXTRA_DATE, 0);
        customerName = getIntent().getStringExtra(EXTRA_CUSTOMER_NAME);
        eventName = getIntent().getStringExtra(EXTRA_EVENT_NAME);
        merchantReference = System.currentTimeMillis() + "";
        if (getIntent().hasExtra(EXTRA_CODE))
            code = getIntent().getStringExtra(EXTRA_CODE);
        //type = event.getEvent().getType() == 2 ? REF_ITEM_PURCHASES : REF_PURCHASES;
        productType = (ProductType) getIntent().getSerializableExtra(EXTRA_TYPE);
        switch (productType) {
            case EVENT:
                refKey = FirebaseDatabase.getInstance().getReference().child(REF_PURCHASES).push().getKey();
                serverReference = REF_PURCHASES + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + refKey + "/" + "status";
                transactionReference = REF_PURCHASES + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + refKey + "/" + "transactionNumber";
                itemRef = REF_PURCHASES + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + refKey;
                //merchantReference = event.getEvent().getEventId() + "";
                break;
            case PRODUCT:
                refKey = FirebaseDatabase.getInstance().getReference().child(REF_ITEM_PURCHASES).push().getKey();
                serverReference = REF_ITEM_PURCHASES + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + refKey + "/" + "status";
                transactionReference = REF_ITEM_PURCHASES + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + refKey + "/" + "transactionNumber";
                itemRef = REF_ITEM_PURCHASES + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + refKey;

                //merchantReference = event.getEvent().getEventId() + "";
                break;
            case SUBSCRIPTION:
                refKey = FirebaseAuth.getInstance().getCurrentUser().getUid();
                serverReference = REF_USERS + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + REF_PAYMENT_STATUS + "/" + "status";
                transactionReference = REF_USERS + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + "/" + REF_PAYMENT_STATUS + "/" + "transactionNumber";
                //merchantReference = FirebaseAuth.getInstance().getCurrentUser().getUid();
                break;
        }
        startWebView();
    }


    private void startWebView() {
        String orderDesc = serverReference + "#" + eventName;

        String generateSha = BuildConfig.SHA_PASSOWRD + "access_code=" + BuildConfig.ACCESS_CODE +
                "amount=" + cost +
                "command=PURCHASE" +
                "currency=USD" +
                "customer_email=" + "youcode.developer@youcode.co" +
                "customer_name=" + customerName +
                "language=en" +
                "merchant_identifier=" + BuildConfig.MERCHANT_IDENTIFIER +
                "merchant_reference=" + merchantReference +
                "order_description=" + orderDesc +
                BuildConfig.SHA_PASSOWRD;


        String signature = bytesToHex(hashByte(generateSha));

        String postData = "";
        try {
            postData = "command=" + URLEncoder.encode("PURCHASE", "UTF-8")
                    + "&access_code=" + URLEncoder.encode(BuildConfig.ACCESS_CODE, "UTF-8")
                    + "&merchant_reference=" + URLEncoder.encode(merchantReference, "UTF-8")
                    + "&merchant_identifier=" + URLEncoder.encode(BuildConfig.MERCHANT_IDENTIFIER, "UTF-8")
                    + "&amount=" + URLEncoder.encode(cost, "UTF-8")
                    + "&currency=" + URLEncoder.encode("USD", "UTF-8")
                    + "&language=" + URLEncoder.encode("en", "UTF-8")
                    + "&customer_name=" + URLEncoder.encode(customerName, "UTF-8")
                    + "&customer_email=" + URLEncoder.encode("youcode.developer@youcode.co", "UTF-8")
                    + "&order_description=" + URLEncoder.encode(orderDesc, "UTF-8")
                    + "&signature=" + signature;


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        byte[] data = postData.getBytes();

        webView.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage cmsg) {


                return true;

            }
        });

        WebViewClient yourWebClient = new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("cancelOperation"))
                    finish();
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                webView.loadUrl("javascript:HtmlViewer.showHTML" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        };

        webView.setWebViewClient(yourWebClient);
        /*webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
            }
        });*/
        //Load url in webView
        String url = BuildConfig.FORTURL + "FortAPI/paymentPage";
        webView.postUrl(url, data);
        updatePaymentOnFirebase();


    }

    private byte[] hashByte(String data) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] encodedhash = digest.digest(
                data.getBytes(StandardCharsets.UTF_8));

        return encodedhash;
    }

    private String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }


    class MyJavaScriptInterface {

        private Context ctx;

        MyJavaScriptInterface(Context ctx) {
            this.ctx = ctx;
        }

        @JavascriptInterface
        public void showHTML(String html) {
            System.out.println(html);
            if (html.contains("\"response_code\":\"14000\",\"response_message\":\"Success\"")) {

                FirebaseDatabase.getInstance().getReference().child(serverReference).setValue(true)
                        .addOnCompleteListener(task -> {
                        });


                //   SubscriptionStatus subscriptionStatus = new SubscriptionStatus(false, subscriptionDate < 1 ? findSubscriptionDate(System.currentTimeMillis()) : findSubscriptionDate(subscriptionDate));
                if (productType == ProductType.SUBSCRIPTION) {


                  /*  FirebaseDatabase.getInstance().getReference().child(REF_USERS).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child(REF_PAYMENT_STATUS)
                            .child("date")
                            .setValue(subscriptionDate)
                            .addOnCompleteListener(task -> {

                            });*/
                    FirebaseDatabase.getInstance().getReference().child(REF_CLIENTS).child(code)
                            .child("date")
                            .setValue(subscriptionDate)
                            .addOnCompleteListener(task -> {
                            });

                    FirebaseDatabase.getInstance().getReference().child(REF_CLIENTS).child(code)
                            .child("amountPaid")
                            .setValue(price)
                            .addOnCompleteListener(task -> {
                            });
                }
                try {
                    String response = html.substring(html.indexOf("{"), html.indexOf("}") + 2);
                    JSONObject jsonObject = new JSONObject(response);
                    String fortId = jsonObject.getString("fort_id");
                    FirebaseDatabase.getInstance().getReference().child(transactionReference).setValue(fortId)
                            .addOnCompleteListener(task -> {
                                Intent intent = new Intent();
                                setResult(Activity.RESULT_OK, intent);
                                finish();
                            });

                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else if (html.contains("returnUrlParams")) {
                String response = html.substring(html.indexOf("{"), html.indexOf("}") + 1);
                Gson gson = new Gson();
                PaymentResponse paymentResponse = gson.fromJson(response, PaymentResponse.class);
                if (paymentResponse != null && paymentResponse.getResponse_message() != null && !paymentResponse.getResponse_message().isEmpty()) {
                    if (paymentResponse.getResponse_message().equals("Success")) {
                        FirebaseDatabase.getInstance().getReference().child(transactionReference).setValue(paymentResponse.getFort_id())
                                .addOnCompleteListener(task -> {
                                    Intent intent = new Intent();
                                    setResult(Activity.RESULT_OK, intent);
                                    finish();
                                });


                    } else if (paymentResponse.getResponse_message().equals("Transaction declined")) {
                        Intent intent = new Intent();
                        setResult(Activity.RESULT_CANCELED, intent);
                        finish();
                    }
                }
            }


        }

    }

    private void updatePaymentOnFirebase() {
        if (productType == ProductType.EVENT || productType == ProductType.PRODUCT) {
            FirebaseDatabase.getInstance().getReference().child(REF_PURCHASES).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child(refKey)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (!dataSnapshot.exists()) {
                                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(databaseRef).child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .child(refKey);
                                databaseReference.setValue(event);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
        }
    }
}
