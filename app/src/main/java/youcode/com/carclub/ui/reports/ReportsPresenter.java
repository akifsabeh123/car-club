package youcode.com.carclub.ui.reports;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.models.Client;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventPayments;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.SubscriptionStatus;
import youcode.com.carclub.models.User;
import youcode.com.carclub.utils.ProductType;

import static youcode.com.carclub.general.Constants.REF_PURCHASES;

/**
 * Created by Akif
 * Since  10/24/2017
 * Company : Youcode
 */

public class ReportsPresenter {

    private ReportsInterface reportsInterface;
    private final String TAG = ReportsPresenter.class.getSimpleName();

    public ReportsPresenter(ReportsInterface reportsInterface) {
        this.reportsInterface = reportsInterface;
    }

    public void getEventsParticipation() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child(Constants.REF_EVENTS)/*.child(Constants.REF_EVENTS_PURCHASES)*/;
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Event> events = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                    events.add(dataSnapshot1.getValue(Event.class));
                getEventInfoData(events);
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @SuppressLint("CheckResult")
    public void getAllUsers() {
      /*  reportsInterface.showWait();
        getUsers().subscribeOn(Schedulers.io())
                .flatMapObservable(Observable::fromIterable)
                .flatMapSingle(user -> getSubscriptionDate(user))
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(users -> {
                    reportsInterface.removeWait();
                    reportsInterface.getUsersSuccess(users);
                }, throwable -> {
                    reportsInterface.removeWait();
                    reportsInterface.getDataFailure(throwable.getMessage());
                });*/

        getUsersV2().subscribeOn(Schedulers.io())
                .flatMapObservable(Observable::fromIterable)
                .flatMapSingle(client -> {
                    User user = new User();
                    user.setPorscheCode(client.getCode());
                    user.setMobile(client.getPhone());
                    SubscriptionStatus subscriptionStatus = new SubscriptionStatus();
                    subscriptionStatus.setDate(client.getDate());
                    user.setSubscriptionStatus(subscriptionStatus);
                    return Single.just(user);
                })
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(users -> {
                    reportsInterface.removeWait();
                    reportsInterface.getUsersSuccess(users);
                }, throwable -> {
                    reportsInterface.removeWait();
                    reportsInterface.getDataFailure(throwable.getMessage());
                });
    }

    public void getEvents(DatabaseReference databaseReference) {
        reportsInterface.showWait();
        getAllEvents(databaseReference).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(events -> {
                    reportsInterface.removeWait();
                    reportsInterface.getEventsSuccess(events);
                }, throwable -> {
                    reportsInterface.removeWait();
                    reportsInterface.getDataFailure(throwable.getMessage());
                });
    }


    private void getEventInfoData(List<Event> events) {
        reportsInterface.showWait();
        Observable.fromIterable(events)
                .flatMapSingle(event -> getEventPurchases(event))
                .toList()
                .subscribe(eventPurchases -> {
                    reportsInterface.removeWait();
                    reportsInterface.getEventDataSuccess(eventPurchases);
                }, throwable -> {
                    reportsInterface.removeWait();
                    reportsInterface.getDataFailure(throwable.getMessage());
                });

    }

    private Single<EventPayments> getEventPurchases(Event event) {
        return Single.create(e -> Single.zip(Single.just(event), getEvent(event.getEventId()), (event1, events) -> new EventPayments(event, events)).subscribe(e::onSuccess, e::onError));
    }

    private Single<List<String>> getEeventsRef(String id) {
        return Single.create(e -> FirebaseDatabase.getInstance().getReference().child(Constants.ANALYTICS)
                .child(ProductType.EVENT.toString())
                .child(id)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<String> ref = new ArrayList<>();
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                            ref.add((String) dataSnapshot1.getValue());
                        e.onSuccess(ref);


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                }));
    }


    private Single<List<EventTicket>> getEvent(String id) {


        return Single.create(e -> {
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.ANALYTICS)
                    .child(ProductType.EVENT.toString())
                    .child(id);
            databaseReference.keepSynced(true);

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        List<String> strings = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            strings.add((String) snapshot.getValue());
                        }

                        Observable.fromIterable(strings)
                                .flatMapMaybe(s -> getSingleAnalytics(s, Constants.TYPE_EVENT))
                                .toList()
                                .subscribe(new SingleObserver<List<EventTicket>>() {
                                    @Override
                                    public void onSubscribe(Disposable d) {

                                    }

                                    @Override
                                    public void onSuccess(List<EventTicket> value) {
                                        e.onSuccess(value);
                                    }

                                    @Override
                                    public void onError(Throwable t) {
                                        e.onError(t);
                                    }
                                });

                    } else {
                        List<EventTicket> eventTickets = new ArrayList<>();
                        e.onSuccess(eventTickets);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    e.onError(databaseError.toException());
                }
            });

        });




      /*  return Single.create(e ->
                databaseReference.orderByChild("status")
                        .equalTo(true)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @SuppressLint("CheckResult")
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    List<EventTicket> eventsPairs = new ArrayList<>();
                                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                        eventsPairs.add(dataSnapshot1.getValue(EventTicket.class));
                                    }
                                    e.onSuccess(eventsPairs);
                                } else
                                    e.onSuccess(eventTickets);
                                *//*  e.onSuccess(events);*//*

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                e.onError(databaseError.toException());
                            }
                        }));*/
    }

    private Single<User> getUser(String id) {

        return Single.create(e -> {
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS).child(id);
            databaseReference.keepSynced(true);
            databaseReference
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                                User user = dataSnapshot.getValue(User.class);
                                e.onSuccess(user);
                            } else {
                                FirebaseDatabase.getInstance().getReference().child(Constants.REF_CLIENTS)
                                        .child(id)
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                                                    Client client = dataSnapshot.getValue(Client.class);
                                                    client.setCode(dataSnapshot.getKey());
                                                    User user = new User();
                                                    user.setPorscheCode(client.getCode());
                                                    user.setMobile(client.getPhone());
                                                    e.onSuccess(user);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                                e.onError(databaseError.toException());
                                            }
                                        });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            e.onError(databaseError.toException());
                        }
                    });
        });

    }

    private Single<List<User>> getAllUsers(List<String> id) {
        return Observable.fromIterable(id)
                .flatMapSingle(s -> getUser(s))
                .toList();
    }

    private Single<List<Event>> getAllEvents(List<Event> id) {
        return Observable.fromIterable(id)
                .flatMapSingle(s -> getEventPayment(s.getEventId()))
                .toList();
    }

    private Single<Event> getEventPayment(String id) {
        return Single.create(e -> {
            FirebaseDatabase.getInstance().getReference().child(REF_PURCHASES)
                    .child(id)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                Event event = dataSnapshot.getValue(Event.class);
                                e.onSuccess(event);
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            e.onError(databaseError.toException());
                        }
                    });
        });
    }

    private Single<List<User>> getUsers() {
        return Single.create(e -> {
            FirebaseDatabase.getInstance().getReference().child(Constants.REF_USERS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<User> users = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren())
                        users.add(snapshot.getValue(User.class));
                    e.onSuccess(users);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    e.onError(databaseError.toException());
                }
            });
        });
    }

    private Single<List<Client>> getUsersV2() {
        return Single.create(e -> {
            FirebaseDatabase.getInstance().getReference().child(Constants.REF_CLIENTS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<Client> users = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren())
                        users.add(snapshot.getValue(Client.class));
                    e.onSuccess(users);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    e.onError(databaseError.toException());
                }
            });
        });
    }

    private Single<User> getSubscriptionDate(User user) {
        if (user.getPorscheCode() == null || user.getPorscheCode().isEmpty())
            return Single.just(user);
        return Single.create(e -> {
            DatabaseReference databaseReference =
                    FirebaseDatabase.getInstance().getReference().child(Constants.REF_CLIENTS)
                            .child(user.getPorscheCode())
                            .child("date");
            databaseReference.keepSynced(true);
            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        long date = (long) dataSnapshot.getValue();
                        SubscriptionStatus subscriptionStatus = new SubscriptionStatus();
                        subscriptionStatus.setDate(date);
                        user.setSubscriptionStatus(subscriptionStatus);
                        e.onSuccess(user);
                    } else
                        e.onSuccess(user);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    e.onError(databaseError.toException());
                }
            });
        });
    }

    private Single<List<Event>> getAllEvents(DatabaseReference databaseReference) {
        return Single.create(e -> databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Event> events = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren())
                    events.add(snapshot.getValue(Event.class));
                e.onSuccess(events);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                e.onError(databaseError.toException());
            }
        }));
    }

    public void findAnalyticForSingleEvent(Event event) {
        String ref;
        if (event.getType() == 1)
            ref = ProductType.EVENT.toString();
        else if (event.getType() == 2)
            ref = ProductType.PRODUCT.toString();
        else
            return;
        reportsInterface.showWait();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.ANALYTICS)
                .child(ref)
                .child(event.getEventId());
        databaseReference.keepSynced(true);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    List<String> ref = new ArrayList<>();
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                        ref.add((String) dataSnapshot1.getValue());
                    Observable.fromIterable(ref)
                            .flatMapMaybe(s -> getSingleAnalytics(s, event.getType()))
                            .toList()
                            .subscribe(eventTickets -> {
                                reportsInterface.removeWait();
                                EventPayments eventPayments = new EventPayments(event, eventTickets);
                                reportsInterface.getSingleEventAnalyticsSuccess(eventPayments);
                            }, throwable -> {
                                reportsInterface.removeWait();
                                reportsInterface.getDataFailure(throwable.getMessage());
                            });

                } else {
                    reportsInterface.removeWait();
                    List<EventTicket> eventTickets = new ArrayList<>();
                    EventPayments eventPayments = new EventPayments(event, eventTickets);
                    reportsInterface.getSingleEventAnalyticsSuccess(eventPayments);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                reportsInterface.removeWait();
                reportsInterface.getDataFailure(databaseError.getMessage());
            }
        });

    }

    private Maybe<EventTicket> getSingleAnalytics(String ref, int type) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(ref);
        databaseReference.keepSynced(true);
        return Maybe.create(e -> databaseReference
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            EventTicket eventTicket = dataSnapshot.getValue(EventTicket.class);
                            if (eventTicket.isStatus()) {
                                eventTicket.setId(dataSnapshot.getKey());
                                String ref = "";
                                if (type == Constants.TYPE_EVENT)
                                    ref = Constants.REF_EVENTS;
                                else if (type == Constants.TYPE_PRODUCT)
                                    ref = Constants.REF_PRODUCTS;

                                DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child(ref);
                                databaseReference1.keepSynced(true);
                                databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                                            Event event = dataSnapshot.getValue(Event.class);
                                            eventTicket.setEvent(event);
                                            getUser(eventTicket.getUserId())
                                                    .subscribeOn(Schedulers.io())
                                                    .subscribe(user -> {
                                                        eventTicket.setUser(user);
                                                        e.onSuccess(eventTicket);
                                                    }, throwable -> {
                                                        e.onComplete();
                                                    });

                                        } else e.onComplete();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        e.onError(databaseError.toException());
                                    }
                                });
                            } else e.onComplete();

                        } else
                            e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                }));
    }
}
