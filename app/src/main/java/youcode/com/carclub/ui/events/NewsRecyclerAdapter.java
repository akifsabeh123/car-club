package youcode.com.carclub.ui.events;

/**
 * Created by Desk1 on 6/13/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;

import youcode.com.carclub.general.Utils;
import youcode.com.carclub.ui.newItems.AddEventActivity;
import youcode.com.carclub.R;
import youcode.com.carclub.models.Event;

public class NewsRecyclerAdapter extends FirebaseRecyclerAdapter<Event, NewsRecyclerAdapter.MyViewHolder> {

    private NewsFragment.NewsFragmentListener mListener;

    /**
     * @param modelClass      Firebase will marshall the data at a location into an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list. You will be responsible for populating an
     *                        instance of the corresponding view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                        combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     * @param mListener
     */
    public NewsRecyclerAdapter(Class<Event> modelClass, int modelLayout, Class<MyViewHolder> viewHolderClass, Query ref, NewsFragment.NewsFragmentListener mListener) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.mListener = mListener;
    }

    @Override
    protected void populateViewHolder(MyViewHolder viewHolder, Event news, int position) {
        Glide.with(viewHolder.itemView.getContext()).applyDefaultRequestOptions(Utils.getRequestOptions()).load(news.getImgUrl()).into(viewHolder.img1);
       // viewHolder.txt_address.setText(news.getEventLocation().getAddress());
        viewHolder.txt_title.setText(news.getTitle());

        viewHolder.itemView.setOnClickListener(view -> mListener.onItemClick(news, AddEventActivity.TYPE_NEWS));
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img1;
        View fullView;
        TextView txt_title, txt_address;

        public MyViewHolder(View view) {
            super(view);
            fullView = view;
            img1 = (ImageView) view.findViewById(R.id.img1);
            txt_title = (TextView) view.findViewById(R.id.txt_title);
            txt_address = (TextView) view.findViewById(R.id.txt_address);

        }
    }


}