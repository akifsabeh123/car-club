package youcode.com.carclub.ui.notifications;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Notification;
import youcode.com.carclub.networking.NetworkOperations;

public class NewNotificationActivity extends AppCompatActivity {

    private EditText et_title, et_message;
    private Button btn_send;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_notification);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.send_notification));

        et_title = findViewById(R.id.et_title);
        et_message = findViewById(R.id.et_message);
        btn_send = findViewById(R.id.btn_send);
        progress = findViewById(R.id.progress);
        btn_send.setOnClickListener(v -> sendNotification());
    }

    private void sendNotification() {
        et_title.setError(null);
        et_message.setError(null);
        if (et_title.getText().toString().trim().isEmpty()) {
            et_title.setError(getString(R.string.error_empty_field));
            return;
        }
        if (et_message.getText().toString().trim().isEmpty()) {
            et_message.setError(getString(R.string.error_empty_field));
            return;
        }
        showWait();
        String title = et_title.getText().toString();
        String message = et_message.getText().toString();
        new NetworkOperations().sendNotification(title, message, new NetworkOperations.NetworkListener() {
            @Override
            public void onComplete() {
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.NOTIFICATIONS);
                String key = databaseReference.push().getKey();
                Calendar calendar = Calendar.getInstance();
                Notification notification = new Notification(key, title, message, calendar.getTimeInMillis());
                databaseReference.child(key).setValue(notification).addOnCompleteListener(task -> {
                    hideWait();
                    if (task.isSuccessful()) {
                        Utils.showToast(NewNotificationActivity.this, "Notification has been sent successfully", Toast.LENGTH_SHORT);
                        et_message.setText("");
                        et_title.setText("");
                    } else {
                        Utils.showToast(NewNotificationActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT);

                    }
                });
            }

            @Override
            public void onFail(String message) {
                hideWait();
                Utils.showToast(NewNotificationActivity.this, message, Toast.LENGTH_SHORT);

            }
        });

    }

    private void showWait() {
        progress.setVisibility(View.VISIBLE);
        btn_send.setEnabled(false);
    }

    private void hideWait() {
        progress.setVisibility(View.GONE);
        btn_send.setEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
