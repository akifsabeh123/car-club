package youcode.com.carclub.ui.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Payment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PaymentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PaymentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentFragment extends Fragment implements AppCompatRadioButton.OnCheckedChangeListener, PaymentInterface {

    private PaymentPresenter paymentPresenter;
    private ProgressDialog progressDialog;

    private OnFragmentInteractionListener mListener;

    private AppCompatRadioButton rb_visa;
    private AppCompatRadioButton rb_master;

    private TextView txt_name_on_card, txt_card_number, txt_cvv, txt_billing1, txt_billing2, txt_billing3, txt_po;

    public PaymentFragment() {
        // Required empty public constructor
    }


    public static PaymentFragment newInstance() {
        PaymentFragment fragment = new PaymentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        mListener.onPaymentVisible();

    }

    private void init(View view) {
        txt_name_on_card = view.findViewById(R.id.txt_name_on_card);
        txt_card_number = view.findViewById(R.id.txt_card_number);
        txt_cvv = view.findViewById(R.id.txt_cvv);
        txt_billing1 = view.findViewById(R.id.txt_billing1);
        txt_billing2 = view.findViewById(R.id.txt_billing2);
        txt_billing3 = view.findViewById(R.id.txt_billing3);
        txt_po = view.findViewById(R.id.txt_po);


        rb_visa = view.findViewById(R.id.rb_visa);
        rb_master = view.findViewById(R.id.rb_master);

        ImageView img = view.findViewById(R.id.img);
        Button btn_complete = view.findViewById(R.id.btn_payment_complete);
        Button btn_skip = view.findViewById(R.id.btn_skip);

        Glide.with(getContext()).load(R.drawable.reg_background).into(img);
        btn_complete.setOnClickListener(view1 -> updatePayment());
        btn_skip.setOnClickListener(view1 -> paymentComplete());

        rb_visa.setOnCheckedChangeListener(this);
        rb_master.setOnCheckedChangeListener(this);
        paymentPresenter = new PaymentPresenter(this);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.message_please_wait));

    }

    private void updatePayment() {
        Payment payment = new Payment();
        payment.setCardName(txt_name_on_card.getText().toString());
        payment.setCardNumber(txt_card_number.getText().toString());
        payment.setBillingAddress1(txt_billing1.getText().toString());
        payment.setBillingAddress2(txt_billing2.getText().toString());
        payment.setBillingAddress3(txt_billing3.getText().toString());
        payment.setCvv(txt_cvv.getText().toString());
        payment.setPoBox(txt_po.getText().toString());
        payment.setVisa(rb_visa.isChecked());

        if (valid(payment))
            paymentPresenter.updatePayment(payment);
    }

    private boolean valid(Payment payment) {
        boolean valid = true;

        if (Utils.isNullOrEmpty(payment.getCardName())) {
            valid = false;
            txt_name_on_card.setError(getString(R.string.error_empty_field));
        }

        if (Utils.isNullOrEmpty(payment.getCardNumber())) {
            valid = false;
            txt_card_number.setError(getString(R.string.error_empty_field));
        }

        if (Utils.isNullOrEmpty(payment.getCvv())) {
            valid = false;
            txt_cvv.setError(getString(R.string.error_empty_field));
        }


        return valid;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NewsFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            switch (compoundButton.getId()) {
                case R.id.rb_visa:
                    rb_master.setChecked(false);
                    break;
                case R.id.rb_master:
                    rb_visa.setChecked(false);
                    break;
            }
        }
    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {

        progressDialog.dismiss();

    }

    @Override
    public void paymentComplete() {
        mListener.onBillingComplete();
    }

    @Override
    public void paymentError(String errorMessage) {
        Utils.showToast(getContext(), errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void getPaymentSuccess(Payment payment) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onBillingComplete();

        void onPaymentVisible();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        paymentPresenter.onStop();
    }
}
