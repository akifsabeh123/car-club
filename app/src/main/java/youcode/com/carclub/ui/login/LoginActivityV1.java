package youcode.com.carclub.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;

import java.util.ArrayList;
import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.SpinnerAdapter;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.SharedPrefsUtils;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Client;
import youcode.com.carclub.models.Payment;
import youcode.com.carclub.models.SpinnerData;
import youcode.com.carclub.models.SubscriptionStatus;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.main.Main2Activity;

import static youcode.com.carclub.ui.login.LoginPresenter.STATE_CODE_SENT;
import static youcode.com.carclub.ui.login.LoginPresenter.STATE_DONE;
import static youcode.com.carclub.ui.login.LoginPresenter.STATE_INITIALIZED;
import static youcode.com.carclub.ui.login.LoginPresenter.STATE_SIGNIN_FAILED;
import static youcode.com.carclub.ui.login.LoginPresenter.STATE_SIGNIN_SUCCESS;
import static youcode.com.carclub.ui.login.LoginPresenter.STATE_UPDATE_USER_SUCCESS;
import static youcode.com.carclub.ui.login.LoginPresenter.STATE_VERIFY_FAILED;
import static youcode.com.carclub.ui.login.LoginPresenter.STATE_VERIFY_SUCCESS;

public class LoginActivityV1 extends AppCompatActivity implements LoginInterface, CompoundButton.OnCheckedChangeListener {

    private final int vs_child_check = 0;
    private final int vs_child_code = 1;
    private final int vs_profile = 2;
    private final int vs_payment = 3;

    private LoginPresenter loginPresenter;

    ArrayList<SpinnerData> spinnerDataArrayList = new ArrayList<>();

    private ViewFlipper viewSwitcher;
    private EditText et_porsche_id, et_mobile, et_code, et_name, et_family_name, et_email, et_username,
            et_name_on_card, et_card_number, et_cvv, et_billing1, et_billing2, et_billing3, et_po;
    private Button btn_sign_up, btn_done, btn_resend, btn_complete,
            btn_payment_complete, btn_skip;
    private FrameLayout progress;
    private AppCompatRadioButton rb_yes, rb_no, rb_visa, rb_master;
    private Spinner spinner;

    private Spinner spinner1, spinner2, spinner3;
    private String[] cars;
    private Client client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_activity_v1);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        init();

    }

    private void init() {
        spinner = findViewById(R.id.spinner);
        loginPresenter = new LoginPresenter(this, this);
        viewSwitcher = findViewById(R.id.viewSwitcher);
        et_porsche_id = findViewById(R.id.et_porsche_id);
        et_mobile = findViewById(R.id.et_mobile);
        et_code = findViewById(R.id.et_code);
        btn_resend = findViewById(R.id.btn_resend);
        btn_sign_up = findViewById(R.id.btn_sign_up);
        btn_done = findViewById(R.id.btn_done);
        et_name = findViewById(R.id.txt_name);
        et_family_name = findViewById(R.id.txt_family_name);
        et_email = findViewById(R.id.txt_email);
        et_username = findViewById(R.id.txt_username);
        btn_complete = findViewById(R.id.btn_complete);
        progress = findViewById(R.id.progress);

        rb_yes = findViewById(R.id.rb_yes);
        rb_no = findViewById(R.id.rb_no);


        et_name_on_card = findViewById(R.id.txt_name_on_card);
        et_card_number = findViewById(R.id.txt_card_number);
        et_cvv = findViewById(R.id.txt_cvv);
        et_billing1 = findViewById(R.id.txt_billing1);
        et_billing2 = findViewById(R.id.txt_billing2);
        et_billing3 = findViewById(R.id.txt_billing3);
        et_po = findViewById(R.id.txt_po);


        rb_visa = findViewById(R.id.rb_visa);
        rb_master = findViewById(R.id.rb_master);

        btn_payment_complete = findViewById(R.id.btn_payment_complete);
        btn_skip = findViewById(R.id.btn_skip);


        rb_yes.setOnCheckedChangeListener(this);
        rb_no.setOnCheckedChangeListener(this);

        btn_payment_complete.setOnClickListener(view1 -> updatePayment());
        btn_skip.setOnClickListener(view1 -> updateUI(STATE_DONE));

        rb_visa.setOnCheckedChangeListener(this);
        rb_master.setOnCheckedChangeListener(this);

        spinner1 = findViewById(R.id.spinner1);
        spinner2 = findViewById(R.id.spinner2);
        spinner3 = findViewById(R.id.spinner3);

        spinner2.setAdapter(new SpinnerAdapter(
                this,
                Utils.getSpinnerCarDates()));

        Utils.initSpinner(spinner3, spinnerDataArrayList);

        btn_sign_up.setOnClickListener(v -> {
            StringBuilder entry = new StringBuilder(et_porsche_id.getText().toString());
            while (entry.length() < 5) {
                entry.insert(0, "0");
            }
            String inputCode = getResources().getStringArray(R.array.codes)[spinner.getSelectedItemPosition()] + "" + entry;
           /* inputCode = inputCode.replaceFirst("00", "");
            inputCode = inputCode.replaceFirst("LB", "");
            inputCode = inputCode.replaceFirst("0", "");*/
            loginPresenter.checkIfUserExist(inputCode, getMobile());
        });
        btn_done.setOnClickListener(v -> loginPresenter.verifyPhone(et_code.getText().toString()));
        btn_resend.setOnClickListener(v -> loginPresenter.resendCode(getMobile()));
        btn_complete.setOnClickListener(v -> createUser());
        loginPresenter.getCars();
    }

    @NonNull
    private String getMobile() {
        String s = et_mobile.getText().toString();
        if (s.startsWith("0"))
            s = s.replaceFirst("0", "");
       /* if (s.length() > 0) {
            if (s.charAt(0) == '0')
                s = s.substring(0, s.length() - 1);
        }*/
        s = getString(R.string.label_code) + "" + s;
        return s;
    }

    private void updatePayment() {
        Payment payment = new Payment();
        payment.setCardName(et_name_on_card.getText().toString());
        payment.setCardNumber(et_card_number.getText().toString());
        payment.setBillingAddress1(et_billing1.getText().toString());
        payment.setBillingAddress2(et_billing2.getText().toString());
        payment.setBillingAddress3(et_billing3.getText().toString());
        payment.setCvv(et_cvv.getText().toString());
        payment.setPoBox(et_po.getText().toString());
        payment.setVisa(rb_visa.isChecked());

        loginPresenter.updatePayment(payment);
    }

    private void createUser() {
        User user = new User();
        user.setId(FirebaseAuth.getInstance().getCurrentUser().getUid());
        user.setAutoRenew(rb_yes.isChecked());
        user.setName(et_name.getText().toString());
        user.setFamilyName(et_family_name.getText().toString());
        user.setEmail(et_email.getText().toString());
        user.setUsername(et_username.getText().toString());
        user.setCarName(cars[spinner1.getSelectedItemPosition()]);
        user.setCarModel(Utils.getSpinnerCarDates()[spinner2.getSelectedItemPosition()]);
        user.setCarColor(spinnerDataArrayList.get(spinner3.getSelectedItemPosition()).getText());
        if (client != null) {
            user.setPorscheCode(client.getCode());
            SubscriptionStatus subscriptionStatus = new SubscriptionStatus();
            subscriptionStatus.setStatus(true);
            subscriptionStatus.setDate(client.getDate());
            user.setSubscriptionStatus(subscriptionStatus);
        } else {
            user.setPorscheCode(getResources().getStringArray(R.array.codes)[spinner.getSelectedItemPosition()] + "" + et_porsche_id.getText().toString());
        }
        user.setMobile(getMobile());

        loginPresenter.updateUser(user);
    }

    @Override
    public void showWait() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void loginSuccess() {

    }

    @Override
    public void loginFailure(String errorMessage) {
        Utils.showToast(this, errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void checkIfUserExistComplete(boolean exist) {
        if (exist) {
            loginPresenter.startVerification(getMobile());
        } else {
            new MaterialDialog.Builder(this)
                    .content(getString(R.string.error_no_user_found))
                    .positiveText(getString(R.string.label_ok))
                    .show();
            // Utils.showToast(this, getString(R.string.error_no_user_found), Toast.LENGTH_LONG);
        }


    }

    @Override
    public void checkIfUserExistError(String message) {
        Utils.showToast(this, message, Toast.LENGTH_LONG);
    }

    @Override
    public void updateUI(int uiState) {
        updateUI(uiState, FirebaseAuth.getInstance().getCurrentUser(), null);
    }

    @Override
    public void showMobileError(String error) {
        et_mobile.setError(error);
    }

    @Override
    public void showError(String error) {
        removeWait();
        Snackbar.make(findViewById(android.R.id.content), error,
                Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void updateUI(int uiState, FirebaseUser user) {
        updateUI(uiState, user, null);
    }

    @Override
    public void updateUI(int uiState, PhoneAuthCredential cred) {
        updateUI(uiState, null, cred);
    }

    @Override
    public void showCodeError(String error) {
        et_code.setError(error);
    }

    @Override
    public void getCarsSuccess(List<String> list) {
        this.cars = list.toArray(new String[list.size()]);
        spinner1.setAdapter(new SpinnerAdapter(
                this,
                cars));
    }

    @Override
    public void getClientSuccess(Client client) {
        this.client = client;
    }

    private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
                break;
            case STATE_CODE_SENT:
                // Code sent state, show the verification field, the
                viewSwitcher.setDisplayedChild(vs_child_code);
                break;
            case STATE_VERIFY_FAILED:
                //viewSwitcher.setDisplayedChild(vs_child_code);
                break;
            case STATE_VERIFY_SUCCESS:
                // Verification has succeeded, proceed to firebase sign in
                viewSwitcher.setDisplayedChild(vs_profile);

                break;
            case STATE_SIGNIN_FAILED:
                break;
            case STATE_SIGNIN_SUCCESS:
                viewSwitcher.setDisplayedChild(vs_profile);
                break;
            case STATE_DONE:
                SharedPrefsUtils.setBooleanPreference(this,Constants.USER_SUBSCRIBED, true);
                startActivity(new Intent(this, Main2Activity.class));
                finish();
                break;
            case STATE_UPDATE_USER_SUCCESS:
                SharedPrefsUtils.setBooleanPreference(this,Constants.USER_SUBSCRIBED, true);
                startActivity(new Intent(this, Main2Activity.class));
                finish();
                break;
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean b) {
        if (b) {
            switch (buttonView.getId()) {
                case R.id.rb_yes:
                    rb_no.setChecked(false);
                    break;
                case R.id.rb_no:
                    rb_yes.setChecked(false);
                    break;
                case R.id.rb_visa:
                    rb_master.setChecked(false);
                    break;
                case R.id.rb_master:
                    rb_visa.setChecked(false);
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (viewSwitcher.getDisplayedChild() == vs_child_code)
            viewSwitcher.setDisplayedChild(vs_child_check);
        else
            super.onBackPressed();
    }
}
