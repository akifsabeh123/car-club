package youcode.com.carclub.ui.events;

/**
 * Created by Desk1 on 6/13/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.ui.newItems.AddEventActivity;

public class ProductsRecyclerAdapter extends FirebaseRecyclerAdapter<Event, ProductsRecyclerAdapter.MyViewHolder> {
    private ProductsFragment.ProductFragmentListener listener;


    /**
     * @param modelClass      Firebase will marshall the data at a location into an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list. You will be responsible for populating an
     *                        instance of the corresponding view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                        combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     * @param listener
     */
    public ProductsRecyclerAdapter(Class<Event> modelClass, int modelLayout, Class<MyViewHolder> viewHolderClass, Query ref, ProductsFragment.ProductFragmentListener listener) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.listener = listener;
    }

    @Override
    protected void populateViewHolder(MyViewHolder viewHolder, Event product, int position) {
        Glide.with(viewHolder.itemView.getContext()).applyDefaultRequestOptions(Utils.getRequestOptions()).load(product.getImgUrl()).into(viewHolder.img1);
       // viewHolder.txt_address.setText(product.getEventLocation().getAddress());
        viewHolder.txt_price.setText(viewHolder.itemView.getContext().getString(R.string.label_price, String.valueOf(product.getPrice())));

        viewHolder.itemView.setOnClickListener(view -> listener.onItemClick(product, AddEventActivity.TYPE_PRODUCT));
        viewHolder.btn_details.setOnClickListener(view -> listener.onItemClick(product, AddEventActivity.TYPE_PRODUCT));
       // viewHolder.btn_buy.setOnClickListener(view -> listener.onBuyClicked(product));

    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img1;
        View fullView;
        TextView txt_address, txt_price;
        Button btn_details;
        Button btn_buy;

        public MyViewHolder(View view) {
            super(view);
            fullView = view;
            img1 = (ImageView) view.findViewById(R.id.img1);
            txt_address = (TextView) view.findViewById(R.id.txt_address);
            txt_price = (TextView) view.findViewById(R.id.txt_price);
            btn_details = (Button) view.findViewById(R.id.btn_details);
            btn_buy = (Button) view.findViewById(R.id.btn_buy);

        }
    }

}