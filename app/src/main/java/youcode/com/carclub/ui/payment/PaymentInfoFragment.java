package youcode.com.carclub.ui.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.Payment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PaymentInfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PaymentInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentInfoFragment extends Fragment implements PaymentInterface {

    private ProgressDialog progressDialog;

    private TextView txt_name_on_card, txt_card_number, txt_cvv, txt_billing1, txt_billing2, txt_billing3, txt_po, txt_payment_type, txt_payment_info;

    private OnFragmentInteractionListener mListener;

    private PaymentPresenter paymentPresenter;

    public PaymentInfoFragment() {
        // Required empty public constructor
    }

    public static PaymentInfoFragment newInstance() {
        PaymentInfoFragment fragment = new PaymentInfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener.onPaymentVisible();
        init(view);
    }

    private void init(View view) {
        ImageView img = view.findViewById(R.id.img);
        Glide.with(getContext()).load(R.drawable.reg_background).into(img);
        txt_name_on_card = view.findViewById(R.id.txt_name_on_card);
        txt_card_number = view.findViewById(R.id.txt_card_number);
        txt_cvv = view.findViewById(R.id.txt_cvv);
        txt_billing1 = view.findViewById(R.id.txt_billing1);
        txt_billing2 = view.findViewById(R.id.txt_billing2);
        txt_billing3 = view.findViewById(R.id.txt_billing3);
        txt_po = view.findViewById(R.id.txt_po);
        txt_payment_type = view.findViewById(R.id.txt_payment_type);
        txt_payment_info = view.findViewById(R.id.txt_payment_info);
        txt_payment_info.setOnClickListener(view1 -> mListener.showEditPayment());


        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.message_please_wait));

        paymentPresenter = new PaymentPresenter(this);
        paymentPresenter.getPayment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }

    @Override
    public void paymentComplete() {

    }

    @Override
    public void paymentError(String errorMessage) {
        Utils.showToast(getContext(), errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void getPaymentSuccess(Payment payment) {
        init(payment);

    }

    private void init(Payment payment) {
        if (getContext() != null) {
            txt_name_on_card.setText(getString(R.string.label_name_on_card, payment.getCardName()));
            txt_card_number.setText(getString(R.string.label_card_number, payment.getCardNumber()));
            txt_cvv.setText(getString(R.string.label_cvv, payment.getCvv()));
            String cardType = payment.isVisa() ? getString(R.string.label_visa) : getString(R.string.label_master);
            txt_payment_type.setText(getString(R.string.label_card_type, cardType));
            txt_billing1.setText(getString(R.string.label_billing, 1, payment.getBillingAddress1()));
            txt_billing2.setText(getString(R.string.label_billing, 2, payment.getBillingAddress2()));
            txt_billing3.setText(getString(R.string.label_billing, 3, payment.getBillingAddress3()));
            txt_po.setText(getString(R.string.label_po, payment.getPoBox()));
        }
    }


    public interface OnFragmentInteractionListener {
        void onPaymentVisible();

        void showEditPayment();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        paymentPresenter.onStop();
    }
}
