package youcode.com.carclub.ui.login;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;

import java.util.List;

import youcode.com.carclub.general.GeneralInterface;
import youcode.com.carclub.models.Client;

/**
 * Created by user on 8/22/2017.
 */

public interface LoginInterface extends GeneralInterface {

    void loginSuccess();

    void loginFailure(String errorMessage);

    void checkIfUserExistComplete(boolean exist);

    void checkIfUserExistError(String message);

    void updateUI(int stateCodeSent);

    void showMobileError(String error);

    void showError(String error);

    void updateUI(int stateSigninSuccess, FirebaseUser user);

    void updateUI(int stateSigninSuccess, PhoneAuthCredential credential);

    void showCodeError(String error);

    void getCarsSuccess(List<String> list);

    void getClientSuccess(Client client);
}
