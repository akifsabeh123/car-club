package youcode.com.carclub.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import youcode.com.carclub.R;
import youcode.com.carclub.general.BaseActivity;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.ui.main.Main2Activity;
import youcode.com.carclub.ui.payment.PaymentFragment;
import youcode.com.carclub.ui.register.RegisterFragment;

public class LoginActivity extends BaseActivity implements LoginFragment.OnFragmentInteractionListener, RegisterFragment.OnFragmentInteractionListener,
        PaymentFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportFragmentManager().beginTransaction().replace(R.id.content, LoginFragment.newInstance()).commit();
    }

    @Override
    public void loginSuccess() {
        startActivity(new Intent(LoginActivity.this, Main2Activity.class));
    }

    @Override
    public void createAccount() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content, RegisterFragment.newInstance()).commit();
    }

    @Override
    public void loginFailure(String errorMessage) {
        Utils.showToast(getApplicationContext(), errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void onRegisteringComplete() {
        startActivity(new Intent(LoginActivity.this, Main2Activity.class));
    }

    @Override
    public void onRegisterVisible() {

    }

    @Override
    public void onBillingComplete() {
        startActivity(new Intent(LoginActivity.this, Main2Activity.class));
    }

    @Override
    public void onPaymentVisible() {

    }
}
