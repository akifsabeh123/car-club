package youcode.com.carclub.ui.profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import org.apache.commons.lang3.tuple.Triple;

import java.util.Calendar;
import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.SharedPrefsUtils;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.payfort.PayfortActivity;
import youcode.com.carclub.ui.splash.SplashScreen;
import youcode.com.carclub.utils.DateUtils;
import youcode.com.carclub.utils.ProductType;

import static youcode.com.carclub.utils.DateUtils.LONG_DATE_SLASH;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment implements ProfileInterface {

    private ProgressDialog progressDialog;

    private ProfilePresenter profilePresenter;

    private OnFragmentInteractionListener mListener;

    private TextView txt_name, txt_family_name, txt_email, txt_mobile, txt_username, txt_auto_renew, txt_car_type, txt_car_model, txt_car_color, txt_basic_info, txt_porsche_id, txt_expiry_date, txt_renew_fees, txt_next_date;
    private ImageView img;
    private LinearLayout ll_sign_out;
    private Button btn_renew_membership;
    private User user;
    private long expiryDate;

    public static final String TAG = ProfileFragment.class.getSimpleName();

    private void init(View view) {
        txt_name = view.findViewById(R.id.txt_name);
        txt_family_name = view.findViewById(R.id.txt_family_name);
        txt_email = view.findViewById(R.id.txt_email);
        txt_mobile = view.findViewById(R.id.txt_mobile);
        txt_username = view.findViewById(R.id.txt_username);
        txt_auto_renew = view.findViewById(R.id.txt_auto_renew);
        txt_car_type = view.findViewById(R.id.txt_car_type);
        txt_car_model = view.findViewById(R.id.txt_car_model);
        txt_car_color = view.findViewById(R.id.txt_car_color);
        txt_basic_info = view.findViewById(R.id.txt_basic_info);
        txt_porsche_id = view.findViewById(R.id.txt_porsche_id);
        ll_sign_out = view.findViewById(R.id.ll_sign_out);
        img = view.findViewById(R.id.img);
        btn_renew_membership = view.findViewById(R.id.btn_renew_membership);
        txt_expiry_date = view.findViewById(R.id.txt_expiry_date);
        txt_renew_fees = view.findViewById(R.id.txt_renew_fees);
        txt_next_date = view.findViewById(R.id.txt_next_date);

        txt_basic_info.setOnClickListener(view1 -> mListener.showEditProfile());
        btn_renew_membership.setOnClickListener(view1 -> renewSubscription());

        ll_sign_out.setOnClickListener(v -> signOut());

        Glide.with(getContext()).load(R.drawable.reg_background).into(img);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.message_please_wait));

        profilePresenter = new ProfilePresenter(this);
        profilePresenter.getUser();
    }


    private void signOut() {
        new MaterialDialog.Builder(getContext())
                .title(getString(R.string.action_sign_out))
                .content(getString(R.string.label_are_you_sure_signout))
                .positiveText(getString(R.string.label_yes))
                .negativeText(getString(R.string.action_cancel))
                .onPositive((dialog, which) -> {
                    SharedPrefsUtils.setBooleanPreference(getContext(), Constants.USER_SUBSCRIBED, false);
                    profilePresenter.signOutUser();
                    Intent i = new Intent(getContext(), SplashScreen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }).show();

    }

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        mListener.onProfileVisible();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }

    @Override
    public void getUserSuccess(User user) {
        init(user);
    }

    private void init(User user) {
        if (getContext() != null) {
            this.user = user;
            txt_name.setText(Utils.isNullOrEmpty(user.getName()) ? "" : user.getName());
            txt_family_name.setText(Utils.isNullOrEmpty(user.getFamilyName()) ? "" : user.getFamilyName());
            txt_email.setText(Utils.isNullOrEmpty(user.getEmail()) ? "" : user.getEmail());
            txt_mobile.setText(user.getMobile());
            txt_username.setText(user.getUsername());
            txt_auto_renew.setText(user.isAutoRenew() ? getString(R.string.label_yes) : getString(R.string.label_no));
            txt_car_type.setText(user.getCarName());
            txt_car_model.setText(user.getCarModel());
            txt_car_color.setText(user.getCarColor());
            txt_porsche_id.setText(Utils.isNullOrEmpty(user.getPorscheCode()) ? "" : user.getPorscheCode());
          /*  if (user.getSubscriptionStatus() != null) {
                txt_expiry_date.setText(DateUtils.formatTime(user.getSubscriptionStatus().getDate(), LONG_DATE_SLASH));
            } else {
                txt_expiry_date.setText(DateUtils.formatTime(SubscriptionUtils.findClosestYear(), LONG_DATE_SLASH));

            }*/
        }
    }

    @Override
    public void getUserFailure(String errorMessage) {
        Utils.showToast(getContext(), errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void updateUserComplete() {

    }

    @Override
    public void getDateSuccess(long date) {
        this.expiryDate = date;
        txt_expiry_date.setText(DateUtils.formatTime(date, LONG_DATE_SLASH));

        Triple<Long, Integer, Integer> triple = getAmmountAndDate();

        txt_renew_fees.setText(getString(R.string.price_dollar, triple.getMiddle()));
        txt_next_date.setText(DateUtils.formatTime(triple.getLeft(), LONG_DATE_SLASH));
    }

    @Override
    public void getCarsSuccess(List<String> list) {

    }

    private Triple<Long, Integer, Integer> getAmmountAndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(expiryDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);


        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        int numOfMonthsToCharge = 0;
        month += 1;
        currentMonth += 1;
        int finalYear;

        if (year < currentYear) {
            long diff = Calendar.getInstance().getTimeInMillis() - calendar.getTimeInMillis();
            if (diff >= 41472000000L) {
                if (month > 8) {
                    finalYear = currentYear + 1;
                    numOfMonthsToCharge = (12 - month) + 12;
                } else {
                    finalYear = currentYear;
                    numOfMonthsToCharge = 12 - month;
                }
            } else {

                if (currentMonth > 8) {
                    finalYear = currentYear + 1;
                    numOfMonthsToCharge = 12 + (12 - currentMonth);
                } else {
                    finalYear = currentYear;
                    numOfMonthsToCharge = 12;
                }
            }
        } else if (year == currentYear) {
            if (month > 8) {
                finalYear = currentYear + 1;
                numOfMonthsToCharge = (12 - month) + 12;
            } else {
                finalYear = currentYear;
                numOfMonthsToCharge = 12 - month;
            }
        } else {
            if (month > 8) {
                finalYear = year + 1;
                numOfMonthsToCharge = (12 - month) + 12;
            } else {
                finalYear = year;
                numOfMonthsToCharge = 12 - month;

            }
        }


        int price = numOfMonthsToCharge * 25;
        calendar.set(Calendar.DAY_OF_MONTH, 31);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.YEAR, finalYear);

        long newExpiryDate = calendar.getTimeInMillis();
        int finalNumOfMonthsToCharge = numOfMonthsToCharge;
        return new Triple<Long, Integer, Integer>() {
            @Override
            public Long getLeft() {
                return newExpiryDate;
            }

            @Override
            public Integer getMiddle() {
                return price;
            }

            @Override
            public Integer getRight() {
                return finalNumOfMonthsToCharge;
            }
        };
    }

    private void renewSubscription() {
        String title = user.getName() + " " + user.getFamilyName();
        if (title.length() > 40)
            title = title.substring(0, 40);


        if (expiryDate == 0)
            return;

        Triple<Long, Integer, Integer> triple = getAmmountAndDate();
        long newExpiryDate = triple.getLeft();
        long price = triple.getMiddle();
        long numOfMonthsToCharge = triple.getRight();
        Log.i(TAG, "renewSubscription: " + numOfMonthsToCharge + "  " + price + "  " + newExpiryDate);

        PayfortActivity.startPaymentActivity(this, ProductType.SUBSCRIPTION, price, System.currentTimeMillis() + "",
                Constants.REF_CLIENTS, newExpiryDate, user.getPorscheCode(), title, "Renew Membership");

    }


    public interface OnFragmentInteractionListener {
        void onProfileVisible();

        void showEditProfile();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        profilePresenter.onStop();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PayfortActivity.PAYMENT_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    profilePresenter.getUser();
                    showDialog(true);
                } else
                    showDialog(false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showDialog(boolean success) {
        new MaterialDialog.Builder(getContext())
                .title(getString(R.string.label_payment_status))
                .content(success ? getString(R.string.label_subscription_payment_success) : getString(R.string.label_payment_error))
                .positiveText(getString(R.string.label_ok))
                .show();
    }
}
