package youcode.com.carclub.ui.events.ui;

import youcode.com.carclub.general.GeneralInterface;

/**
 * Created by Akif
 * Since  11/29/2017
 * Company : Youcode
 */

public interface EventInterface extends GeneralInterface {

    void isAttending(boolean isAttending);

    void checkFailure(String message);
}
