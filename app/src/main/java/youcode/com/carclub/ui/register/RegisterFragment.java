package youcode.com.carclub.ui.register;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.ArrayList;
import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.SpinnerAdapter;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.SpinnerData;
import youcode.com.carclub.models.User;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment implements AppCompatRadioButton.OnCheckedChangeListener, RegisterInterface {
    ArrayList<SpinnerData> spinnerDataArrayList = new ArrayList<>();
    private Spinner spinner1, spinner2, spinner3;

    private AppCompatRadioButton rb_yes;
    private AppCompatRadioButton rb_no;

    private EditText txt_name, txt_family_name, txt_email, txt_mobile, txt_username, txt_password;

    private RegisterPresenter registerPresenter;
    private OnFragmentInteractionListener mListener;

    private ProgressDialog progressDialog;
    private String[] cars;

    public RegisterFragment() {
        // Required empty public constructor
    }

    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        registerPresenter = new RegisterPresenter(this);
        registerPresenter.getCars();
    }

    private void init(View view) {
        ImageView img = (ImageView) view.findViewById(R.id.img);
        Glide.with(getContext()).load(R.drawable.reg_background).into(img);
        spinner1 = (Spinner) view.findViewById(R.id.spinner1);
        spinner2 = (Spinner) view.findViewById(R.id.spinner2);
        spinner3 = (Spinner) view.findViewById(R.id.spinner3);
        spinner2.setAdapter(new SpinnerAdapter(
                getContext(),
                Utils.getSpinnerCarDates()));

        Utils.initSpinner(spinner3, spinnerDataArrayList);
        Button btn_complete = (Button) view.findViewById(R.id.btn_complete);
        btn_complete.setOnClickListener(view1 -> {
            if (validate())
                registerUser();
        });

        rb_yes = (AppCompatRadioButton) view.findViewById(R.id.rb_yes);
        rb_no = (AppCompatRadioButton) view.findViewById(R.id.rb_no);


        rb_yes.setOnCheckedChangeListener(this);
        rb_no.setOnCheckedChangeListener(this);
        mListener.onRegisterVisible();

        txt_name = (EditText) view.findViewById(R.id.txt_name);
        txt_family_name = (EditText) view.findViewById(R.id.txt_family_name);
        txt_email = (EditText) view.findViewById(R.id.txt_email);
        txt_mobile = (EditText) view.findViewById(R.id.txt_mobile);
        txt_username = (EditText) view.findViewById(R.id.txt_username);
        txt_password = (EditText) view.findViewById(R.id.txt_password);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.label_creating_user));

        RxTextView.textChanges(txt_name).subscribe(this::onNameChanged);
        RxTextView.textChanges(txt_family_name).subscribe(this::onFamilyNameChanged);

    }

    private void onNameChanged(CharSequence s) {
        if (txt_name.getText().length() >= 2)
            txt_username.setText(txt_name.getText().toString().substring(0, 2) + "" + txt_family_name.getText().toString());
        else
            txt_username.setText(s + "" + txt_family_name.getText().toString());
    }

    private void onFamilyNameChanged(CharSequence s) {
        if (txt_name.getText().length() >= 2)
            txt_username.setText(txt_name.getText().toString().substring(0, 2) + "" + s);
        else
            txt_username.setText(txt_name.getText().toString() + "" + s);
    }


    private void registerUser() {

        User user = new User(txt_name.getText().toString(),
                txt_family_name.getText().toString(),
                txt_email.getText().toString(),
                txt_mobile.getText().toString(),
                txt_username.getText().toString(),
                txt_password.getText().toString(),
                cars[spinner1.getSelectedItemPosition()],
                Utils.getSpinnerCarDates()[spinner2.getSelectedItemPosition()],
                spinnerDataArrayList.get(spinner3.getSelectedItemPosition()).getText(),
                rb_yes.isChecked()
        );
        registerPresenter.registerUser(user);

    }

    private boolean validate() {
        boolean valid = true;

        if (Utils.isNullOrEmpty(txt_name.getText().toString())) {
            valid = false;
            txt_name.setError(getString(R.string.error_empty_field));
        }
        if (Utils.isNullOrEmpty(txt_family_name.getText().toString())) {
            valid = false;
            txt_family_name.setError(getString(R.string.error_empty_field));
        }
        if (!isEmailValid(txt_email.getText().toString())) {
            valid = false;
            txt_email.setError(getString(R.string.error_email));
        }

        if (Utils.isNullOrEmpty(txt_mobile.getText().toString())) {
            valid = false;
            txt_mobile.setError(getString(R.string.error_empty_field));
        }

        if (Utils.isNullOrEmpty(txt_username.getText().toString())) {
            valid = false;
            txt_username.setError(getString(R.string.error_empty_field));
        }
        if (Utils.isNullOrEmpty(txt_password.getText().toString()) || txt_password.getText().toString().length() < 6) {
            valid = false;
            txt_password.setError(getString(R.string.error_password));
        }
        return valid;
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NewsFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showWait() {
        progressDialog.show();
    }

    @Override
    public void removeWait() {
        progressDialog.dismiss();
    }


    @Override
    public void registerFailure(String error) {
        Utils.showToast(getContext(), error, Toast.LENGTH_SHORT);
    }

    @Override
    public void createUserTableSuccess() {
        mListener.onRegisteringComplete();
    }

    @Override
    public void createUserTableFailure(String message) {
        Utils.showToast(getContext(), message, Toast.LENGTH_SHORT);
    }

    @Override
    public void getCarsSuccess(List<String> list) {
        this.cars = list.toArray(new String[list.size()]);
        spinner1.setAdapter(new SpinnerAdapter(
                getContext(),
                cars));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onRegisteringComplete();

        void onRegisterVisible();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            switch (compoundButton.getId()) {
                case R.id.rb_yes:
                    rb_no.setChecked(false);
                    break;
                case R.id.rb_no:
                    rb_yes.setChecked(false);
                    break;
            }
        }
    }
}
