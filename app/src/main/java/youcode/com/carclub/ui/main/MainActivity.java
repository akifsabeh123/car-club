package youcode.com.carclub.ui.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.List;

import youcode.com.carclub.R;
import youcode.com.carclub.general.BaseActivity;
import youcode.com.carclub.models.Event;
import youcode.com.carclub.models.EventTicket;
import youcode.com.carclub.models.User;
import youcode.com.carclub.ui.edit.EditPaymentActivity;
import youcode.com.carclub.ui.edit.EditProfileActivity;
import youcode.com.carclub.ui.events.EventsFragment;
import youcode.com.carclub.ui.events.FragmentTabbedEvents;
import youcode.com.carclub.ui.events.NewsFragment;
import youcode.com.carclub.ui.events.ProductsFragment;
import youcode.com.carclub.ui.events.ui.EventActivity;
import youcode.com.carclub.ui.membership.MembershipFragment;
import youcode.com.carclub.ui.payment.PaymentDialogFragment;
import youcode.com.carclub.ui.payment.PaymentInfoFragment;
import youcode.com.carclub.ui.profile.ProfileFragment;
import youcode.com.carclub.ui.profile.ProfileInterface;
import youcode.com.carclub.ui.profile.ProfilePresenter;
import youcode.com.carclub.ui.register.SignUpFragment;
import youcode.com.carclub.ui.reports.ReportsActivity;
import youcode.com.carclub.ui.splash.SplashScreen;
import youcode.com.carclub.ui.ticket.EventTicketDialog;
import youcode.com.carclub.ui.ticket.TicketsFragment;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, ProfileFragment.OnFragmentInteractionListener, PaymentInfoFragment.OnFragmentInteractionListener, EventsFragment.EventFragmentListener, MembershipFragment.OnFragmentInteractionListener,
        SignUpFragment.OnFragmentInteractionListener, FragmentTabbedEvents.OnFragmentInteractionListener, ProductsFragment.ProductFragmentListener, NewsFragment.NewsFragmentListener, ProfileInterface, PaymentDialogFragment.PaymentDialogInterface,
        TicketsFragment.TicketFragmentInteractionListener {


    private DrawerLayout drawer;
    private NavigationView navigationView;
    private String currentFragment;
    ProfilePresenter profilePresenter;

    private final int NAV_PROFILE = 0;
    private final int NAV_PAYMENT = 1;
    private final int NAV_EVENTS = 2;
    private final int NAV_MEMBERSHIP = 3;
    private final int NAV_TICKETS = 4;
    private final int NAV_REPORTS = 5;

    private int NAV_CURRENT = NAV_EVENTS;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //handler = new Handler();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(2).setChecked(true);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, FragmentTabbedEvents.newInstance()).commit();
        currentFragment = FragmentTabbedEvents.class.getSimpleName();

        getCurrentUser();


    }

    private void getCurrentUser() {
        if (profilePresenter == null)
            profilePresenter = new ProfilePresenter(this);

        profilePresenter.getUser();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (isEventFragment()) {
            if (!getCurrentFragment().moveTabToFirst()) {
            } else
                finish();
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_event) {
            showEvents();
        } else if (id == R.id.nav_membership) {
            showMembership();
        } else if (id == R.id.nav_register) {
            showRegister();
        } else if (id == R.id.nav_payment) {
            showPayment();
        } else if (id == R.id.nav_tickets) {
            showTickets();
        } else if (id == R.id.nav_signout) {
            signout();
        } else if (id == R.id.nav_reports) {
            showReportsActivity();
        }
        drawer.closeDrawer(GravityCompat.START);
        invalidateOptionsMenu();
        return true;
    }

    private void showReportsActivity() {
        startActivity(new Intent(this, ReportsActivity.class));
    }

    private void signout() {
        profilePresenter.signOutUser();
        Intent intent = new Intent(this, SplashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private void showPayment() {
        if (!currentFragment.equals(PaymentInfoFragment.class.getSimpleName())) {
            checkBackStack();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame, PaymentInfoFragment.newInstance()).addToBackStack(PaymentInfoFragment.class.getSimpleName()).commit();
            currentFragment = PaymentInfoFragment.class.getSimpleName();
            NAV_CURRENT = NAV_PAYMENT;
        }

    }


    private void showRegister() {
        if (!currentFragment.equals(ProfileFragment.class.getSimpleName())) {
            checkBackStack();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame, ProfileFragment.newInstance()).addToBackStack(ProfileFragment.class.getSimpleName()).commit();
            currentFragment = ProfileFragment.class.getSimpleName();
            NAV_CURRENT = NAV_PROFILE;
        }

    }

    private void showEvents() {
        if (!currentFragment.equals(FragmentTabbedEvents.class.getSimpleName())) {
            getSupportFragmentManager().popBackStack(null, getSupportFragmentManager().POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction().add(R.id.frame, FragmentTabbedEvents.newInstance()).addToBackStack(FragmentTabbedEvents.class.getSimpleName()).commit();
            currentFragment = FragmentTabbedEvents.class.getSimpleName();
            NAV_CURRENT = NAV_EVENTS;
        }


    }

    private void showMembership() {
        if (!currentFragment.equals(MembershipFragment.class.getSimpleName())) {
            checkBackStack();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame, MembershipFragment.newInstance("", "")).addToBackStack(MembershipFragment.class.getSimpleName()).commit();
            currentFragment = MembershipFragment.class.getSimpleName();
            NAV_CURRENT = NAV_MEMBERSHIP;
        }

    }

    private void showTickets() {
        if (!currentFragment.equals(TicketsFragment.class.getSimpleName())) {
            checkBackStack();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame, TicketsFragment.newInstance()).addToBackStack(TicketsFragment.class.getSimpleName()).commit();
            currentFragment = TicketsFragment.class.getSimpleName();
            NAV_CURRENT = NAV_TICKETS;
        }

    }

    @Override
    public void onPaymentVisible() {
        navigationView.getMenu().getItem(1).setChecked(true);
    }

    @Override
    public void showEditPayment() {

    }


    @Override
    public void onFragmentTabbedEventsVisible() {
        navigationView.getMenu().getItem(2).setChecked(true);
    }

    @Override
    public void isAdmin(boolean isAdmin) {
        navigationView.getMenu().getItem(NAV_REPORTS).setVisible(isAdmin);
    }

    @Override
    public void onMembershipFragmentVisible() {
        navigationView.getMenu().getItem(3).setChecked(true);
    }

    private void checkBackStack() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 4) {

            getSupportFragmentManager().popBackStack();
        }
    }


    private boolean isEventFragment() {
        int index = getSupportFragmentManager().getBackStackEntryCount() - 1;
        if (index > -1) {
            String tag = getSupportFragmentManager().getBackStackEntryAt(index).getName();
            return tag.equals("FragmentTabbedEvents");
        }
        return false;
    }

    private FragmentTabbedEvents getCurrentFragment() {
        FragmentTabbedEvents currentFragment = (FragmentTabbedEvents) getSupportFragmentManager()
                .findFragmentById(R.id.frame);
        return currentFragment;
    }

    @Override
    public void onItemClick(Event event, int type) {
        EventActivity.startActivity(this, event, type);
    }

    @Override
    public void onBuyClicked(Event event) {
        PaymentDialogFragment paymentDialogFragment = PaymentDialogFragment.newInstance(event,false);
        paymentDialogFragment.show(getSupportFragmentManager(), PaymentDialogFragment.class.getSimpleName());
    }

    @Override
    public void onProfileVisible() {
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    @Override
    public void showEditProfile() {

    }

    @Override
    public void showWait() {

    }

    @Override
    public void removeWait() {

    }

    @Override
    public void getUserSuccess(User user) {
        TextView txt_name = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txt_name);
        if (user != null) {
            txt_name.setText(user.getUsername());
        }

    }

    @Override
    public void getUserFailure(String errorMessage) {

    }

    @Override
    public void updateUserComplete() {

    }

    @Override
    public void getDateSuccess(long date) {

    }

    @Override
    public void getCarsSuccess(List<String> list) {

    }


    @Override
    public void onPurchase(EventTicket eventTicket) {
        showEventTicket(eventTicket);
    }

    private void showEventTicket(EventTicket eventTicket) {
        EventTicketDialog eventTicketDialog = EventTicketDialog.newInstance(eventTicket, 1);
        eventTicketDialog.show(getSupportFragmentManager(), EventTicketDialog.class.getSimpleName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        switch (NAV_CURRENT) {
            case NAV_PAYMENT:
            case NAV_PROFILE:
                menu.findItem(R.id.menu_item_edit).setVisible(true);
                break;
            default:
                menu.findItem(R.id.menu_item_edit).setVisible(false);

        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_edit:
                switch (NAV_CURRENT) {
                    case NAV_PROFILE:
                        startActivity(new Intent(MainActivity.this, EditProfileActivity.class));
                        return true;
                    case NAV_PAYMENT:
                        startActivity(new Intent(MainActivity.this, EditPaymentActivity.class));
                        return true;
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        profilePresenter.onStop();
    }

    @Override
    public void onTicketClick(EventTicket eventTicket, int ticketNumber) {
        showEventTicket(eventTicket, ticketNumber);
    }

    private void showEventTicket(EventTicket eventTicket, int ticketNumber) {
        EventTicketDialog eventTicketDialog = EventTicketDialog.newInstance(eventTicket, ticketNumber);
        eventTicketDialog.show(getSupportFragmentManager(), EventTicketDialog.class.getSimpleName());
    }
}

