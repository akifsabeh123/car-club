package youcode.com.carclub.ui.main;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import youcode.com.carclub.R;
import youcode.com.carclub.general.Constants;
import youcode.com.carclub.general.SharedPrefsUtils;
import youcode.com.carclub.general.Utils;
import youcode.com.carclub.models.FireBaseItem;
import youcode.com.carclub.models.Notification;
import youcode.com.carclub.ui.cancel_event_participations.CancelUserSubscriptionActivity;
import youcode.com.carclub.ui.cash.PayByCashActivity;
import youcode.com.carclub.ui.details.DetailsAvtivity;
import youcode.com.carclub.ui.newItems.AddEventActivity;
import youcode.com.carclub.ui.notifications.NewNotificationActivity;
import youcode.com.carclub.ui.notifications.NotificationsRecyclerAdapter;
import youcode.com.carclub.ui.reports.ReportsActivity;
import youcode.com.carclub.views.DividerItemDecoration;

public class Main2Activity extends AppCompatActivity implements MainAdapter.MainAdapterInterface, MainInterface {

    private RecyclerView recyclerView;
    private MainAdapter mainAdapter;
    private MainPresenter mainPresenter;

    private FloatingActionButton fab_add_event, fab_reports, fab_cash, fab_remove;
    private boolean isAdmin = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        /*Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/
        init();
        findViewById(R.id.img_notifications).setOnClickListener(v -> startNotifications());
        checkNotification();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("new-notification"));


    }


    @SuppressLint("CheckResult")
    private void init() {
        fab_add_event = findViewById(R.id.fab_add_event);
        fab_reports = findViewById(R.id.fab_reports);
        fab_cash = findViewById(R.id.fab_cash);
        fab_remove = findViewById(R.id.fab_remove);
        recyclerView = findViewById(R.id.recyclerView);
        mainAdapter = new MainAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider_white)));
        recyclerView.setAdapter(mainAdapter);

        fab_add_event.setOnClickListener(view1 -> startActivity(new Intent(this, AddEventActivity.class)));
        // fab_add_event.setOnClickListener(view1 -> changeData());
        fab_reports.setOnClickListener(view1 -> startActivity(new Intent(this, ReportsActivity.class)));
        fab_cash.setOnClickListener(view1 -> startActivity(new Intent(this, PayByCashActivity.class)));
        fab_remove.setOnClickListener(view1 -> startActivity(new Intent(this, CancelUserSubscriptionActivity.class)));
        mainPresenter = new MainPresenter(this);
        mainPresenter.getData();
        mainPresenter.checkIfAdmin();
        mainPresenter.checkIfShouldUpdate();
        mainPresenter.getUser();
        boolean notification = getIntent().getBooleanExtra("notification", false);
        Completable.timer(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    if (notification) {
                        startNotifications();
                    }
                });
        // changeData();
    }

    private void changeData() {


    }

    @Override
    public void showWait() {

    }

    @Override
    public void removeWait() {

    }

    @Override
    public void getItemsSuccess(List<FireBaseItem> fireBaseItemList) {
        mainAdapter.addItems(fireBaseItemList);
    }

    @Override
    public void getItemsFailure(String errorMessage) {
        Utils.showToast(this, errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void isAdmin(boolean isAdmin) {
        fab_add_event.setVisibility(isAdmin ? View.VISIBLE : View.GONE);
        fab_reports.setVisibility(isAdmin ? View.VISIBLE : View.GONE);
        fab_cash.setVisibility(isAdmin ? View.VISIBLE : View.GONE);
        fab_remove.setVisibility(isAdmin ? View.VISIBLE : View.GONE);

        FirebaseMessaging.getInstance().subscribeToTopic(Constants.TOPIC_EVENTS);
        this.isAdmin = isAdmin;
        SharedPrefsUtils.setBooleanPreference(this,Constants.IS_ADMIN,isAdmin);
    }

    @Override
    public void shouldUpdate(String url) {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.app_name))
                .content(getString(R.string.old_version))
                .positiveText(getString(R.string.action_update))
                .cancelable(false)
                .onPositive((dialog, which) -> {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                })
                .show();
    }

    @Override
    public void getDateSuccess(long date) {
        SharedPrefsUtils.setLongPreference(this, Constants.EXPIRY_DATE, date);
    }


    @Override
    public void onItemClick(View view, FireBaseItem fireBaseItem, String query) {
        DetailsAvtivity.startDetailsActivity(this, view, fireBaseItem, query);
    }


    private void startNotifications() {

        View view = getLayoutInflater().inflate(R.layout.activity_all_notifications, null);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        Query query = FirebaseDatabase.getInstance().getReference().child(Constants.NOTIFICATIONS);
        NotificationsRecyclerAdapter
                notificationsRecyclerAdapter = new NotificationsRecyclerAdapter(Notification.class, R.layout.notification_item, NotificationsRecyclerAdapter.MyViewHolder.class, query, notification -> {

        });
        recyclerView.setAdapter(notificationsRecyclerAdapter);

        ImageButton img_add = view.findViewById(R.id.img_add);
        img_add.setVisibility(isAdmin ? View.VISIBLE : View.GONE);
        img_add.setOnClickListener(v -> startCreateNotification());


        new MaterialDialog.Builder(this)
                .customView(view, false)
                .show();

        SharedPrefsUtils.setBooleanPreference(this, Constants.NOTIFICATION_AVAILABLE, false);
        checkNotification();
    }

    private void startCreateNotification() {
        startActivity(new Intent(this, NewNotificationActivity.class));
    }

    private void checkNotification() {
        boolean isNotification = SharedPrefsUtils.getBooleanPreference(this, Constants.NOTIFICATION_AVAILABLE, false);
        findViewById(R.id.notification_available).setVisibility(isNotification ? View.VISIBLE : View.GONE);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkNotification();
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }
}
