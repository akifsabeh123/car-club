package youcode.com.carclub.ui.newItems;

import youcode.com.carclub.general.GeneralInterface;

/**
 * Created by user on 8/23/2017.
 */

public interface CreateEventInterface extends GeneralInterface {
    void createSuccess();

    void createFailure(String errorMessage);

    void uploadMax(int max);

    void uploadProgress(int progress);

    void uploadStarted();

    void uploadFinished();
}
