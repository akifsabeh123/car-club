package youcode.com.carclub.ui.notifications;

/**
 * Created by Desk1 on 6/13/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;

import youcode.com.carclub.R;
import youcode.com.carclub.models.Notification;

public class NotificationsRecyclerAdapter<T extends RecyclerView.ViewHolder> extends FirebaseRecyclerAdapter<Notification, NotificationsRecyclerAdapter.MyViewHolder> {
    private NotificationsRecyclerAdapterInterface mListener;

    /**
     * @param modelClass      Firebase will marshall the data at a location into an instance of a class that you provide
     * @param modelLayout     This is the layout used to represent a single item in the list. You will be responsible for populating an
     *                        instance of the corresponding view with the data from an instance of modelClass.
     * @param viewHolderClass The class that hold references to all sub-views in an instance modelLayout.
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                        combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public NotificationsRecyclerAdapter(Class<Notification> modelClass, int modelLayout, Class<MyViewHolder> viewHolderClass, Query ref, NotificationsRecyclerAdapterInterface mListener) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.mListener = mListener;


    }

    @Override
    protected void populateViewHolder(MyViewHolder viewHolder, Notification notification, int position) {
        viewHolder.txt_title.setText(notification.getTitle());
        viewHolder.txt_message.setText(notification.getMessage());


        CharSequence relativeTimeStr = android.text.format.DateUtils.getRelativeTimeSpanString(notification.getDate(),
                System.currentTimeMillis(), android.text.format.DateUtils.SECOND_IN_MILLIS, android.text.format.DateUtils.FORMAT_ABBREV_RELATIVE);

        viewHolder.txt_date.setText(relativeTimeStr);
        viewHolder.itemView.setOnClickListener(v -> mListener.onNotificationClick(notification));


    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_title, txt_message, txt_date;

        public MyViewHolder(View view) {
            super(view);
            txt_title = view.findViewById(R.id.txt1);
            txt_message = view.findViewById(R.id.txt2);
            txt_date = view.findViewById(R.id.txt_date);


        }
    }

    public interface NotificationsRecyclerAdapterInterface {
        void onNotificationClick(Notification notification);
    }

}