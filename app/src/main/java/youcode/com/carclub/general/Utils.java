package youcode.com.carclub.general;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import youcode.com.carclub.CustomSpinnerAdapter;
import youcode.com.carclub.R;
import youcode.com.carclub.models.LatLng;
import youcode.com.carclub.models.SpinnerData;
import youcode.com.carclub.views.ImagePicker;

import static youcode.com.carclub.general.Constants.DIRECTORY;
import static youcode.com.carclub.general.Constants.DIRECTORY_REPORTS;

/**
 * Created by Desk1 on 8/21/2017.
 */

public class Utils {

    public static String getError(String message) {
        message = message.replaceAll("email address", Constants.LABEL_USERNAME);
        return message;
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.trim().length() == 0;
    }

    public static void showToast(Context context, String error, int length) {
        if (context != null)
            Toast.makeText(context, error, length).show();
    }

    public static void showSnack(View view, String error, int length) {
        if (view != null)
            Snackbar.make(view, error, length).show();
    }

    public static String getDateFromMillis(long millis, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        return formatter.format(new Date(millis));
    }

    public static String getPathToSaveImage() {

        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + DIRECTORY);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return Environment.getExternalStorageDirectory() + File.separator + DIRECTORY + File.separator + System.currentTimeMillis();
    }

    public static String getLocationImage(LatLng latLng) {
        String location = "https://maps.googleapis.com/maps/api/staticmap?center=" + latLng.getLatitude() + "," + latLng.getLongitude() + "&zoom=17&size=400x400" + "&markers=color:red%7Clabel:S%7C" + latLng.getLatitude() + "," + latLng.getLongitude() + "&key=" + Constants.MAP_IMAGE_API_KEY;
        return location;

    }

    public static void startCrop(Activity activity, int resultCode, Intent data) {
        Uri uri = ImagePicker.getImageUri(activity, resultCode, data);
        UCrop.Options options = new UCrop.Options();
        options.setHideBottomControls(true);
        options.setToolbarColor(ContextCompat.getColor(activity, R.color.primary_grey));
        options.setStatusBarColor(ContextCompat.getColor(activity, R.color.primary_grey));
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(100);
        options.withMaxResultSize(1024, 720);
        UCrop.of(uri, Uri.fromFile(new File(Utils.getPathToSaveImage())))
                .withOptions(options)
                .withAspectRatio(16, 9)
                .start(activity);
    }

    public static void startCrop(Activity activity, int resultCode, String string) {
        Uri uri = Uri.parse(string);
        UCrop.Options options = new UCrop.Options();
        options.setHideBottomControls(true);
        options.setToolbarColor(ContextCompat.getColor(activity, R.color.primary_grey));
        options.setStatusBarColor(ContextCompat.getColor(activity, R.color.primary_grey));
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(100);
        options.withMaxResultSize(1024, 720);
        UCrop.of(Uri.fromFile(new File(string)), Uri.fromFile(new File(Utils.getPathToSaveImage())))
                .withOptions(options)
                .withAspectRatio(16, 9)
                .start(activity);
    }


    public static long getCurrentTime() {
        return System.currentTimeMillis();
    }

    public static RequestOptions getRequestOptions() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.porsche_cars);
        requestOptions.error(R.drawable.porsche_cars);
        return requestOptions;
    }

    public static ProgressDialog customizeProgressDialog(Context activity, String string) {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(string);
        return progressDialog;
    }


    public static String[] getSpinnerCarDates() {
        Calendar calendar = Calendar.getInstance();
        int MAX_DATE = calendar.get(Calendar.YEAR);
        int MIN_DATE = 1937;

        List<String> dates = new ArrayList<>();


        for (int i = MAX_DATE; i > MIN_DATE; i--) {
            dates.add(i + "");
        }
        String[] dateArray = new String[dates.size()];
        return dates.toArray(dateArray);
    }

    public static String[] getCarColors() {
        String[] colors = new String[6];
        colors[0] = "Black";
        colors[1] = "White";
        colors[2] = "Blue";
        colors[3] = "Grey";
        colors[4] = "Red";
        colors[5] = "Yellow";
        return colors;
    }

    public static void initSpinner(Spinner spinner, ArrayList<SpinnerData> spinnerDataArrayList) {
        spinnerDataArrayList.add(new SpinnerData("Black", "#000000"));
        spinnerDataArrayList.add(new SpinnerData("White", "#ffffff"));
        spinnerDataArrayList.add(new SpinnerData("Blue", "#0000FF"));
        spinnerDataArrayList.add(new SpinnerData("Grey", "#FFA2A3A4"));
        spinnerDataArrayList.add(new SpinnerData("Red", "#e20117"));
        spinnerDataArrayList.add(new SpinnerData("Yellow", "#E6B85C"));
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(spinner.getContext(),
                R.layout.custom_spinner_item, R.id.txt, spinnerDataArrayList);
        spinner.setAdapter(adapter);
    }

    public static int findPosition(String carModel, String[] array) {
        int position = 0;
        for (int i = 0; i < array.length; i++)
            if (array[i].equals(carModel)) {
                position = i;
                return position;
            }
        return position;

    }

    public static String getExcelFilePath() {
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + DIRECTORY_REPORTS);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return Environment.getExternalStorageDirectory() + File.separator + DIRECTORY_REPORTS + File.separator + Utils.getDateFromMillis(System.currentTimeMillis(), Constants.FORMAT_DAY) + System.currentTimeMillis() + ".xls";
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
    }


}
