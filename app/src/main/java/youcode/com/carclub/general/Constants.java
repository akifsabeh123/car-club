package youcode.com.carclub.general;

import android.Manifest;
import android.content.Context;

/**
 * Created by Desk1 on 8/21/2017.
 */

public class Constants {

    public static final String PREF_USER_SIGNED_IN = " pref_user_signed_in";
    public static final String EMAIL_COMPLETION = "@youcode.co";
    public static final String REF_USERS = "users";
    public static final String REF_EVENTS = "events";
    public static final String REF_PRODUCTS = "products";
    public static final String REF_NEWS = "news";
    public static final String REF_PAYMENT = "payments";
    public static final String REF_PAYMENT_STATUS = "subscriptionStatus";

    public static final String LOCATION_API_KEY = "AIzaSyBOb3v0vJ6kmUSF_kEdEA8KTVRHYZzEhh0";
    public static final String MAP_IMAGE_API_KEY = "AIzaSyB3fcVsQB-MwYeOyDHs82WdqqZH3N9xsQ4";
    public static final String FORMAT_FULL_DATE = "EEE MMM dd HH:mm yyyy";
    public static final String FORMAT_HALF_DATE = "EEE MMM dd yyyy";
    public static final String FORMAT_MINI_DATE = "dd/MM/yy";
    public static final String DIRECTORY = "Car-club";
    public static final String DIRECTORY_REPORTS = "Car-club/Reports";
    public static final String DIRECTORY_SCREEN_SHOTS = DIRECTORY + "Screenshots";
    public static final String LABEL_USERNAME = "username";

    public static final String[] CAMERA_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static final int RC_CAMERA_AND_LOCATION = 3;
    public static final String TOPIC_EVENTS = "events";
    public static final String REF_TOKEN = "token";
    public static final String REF_IMAGES = "images";
    public static final String SERVER_KEY = "AAAAPOCVhW8:APA91bELKWvQHOCfIfMGbns2THTbF1qq5ZTckrj9CGmDiTp-LBvn6wGNL9IAU_G6v1zmEe7xjSsd56Zm0mdluZktM-dUQBAif_GbEWSzGBrXRqDoBSHM5YEOrm_XhD6Vz-KqDPOj71Ts";
    public static final String PATH_FCM_NOTIFICATION = "fcm/send";
    public static final String EVENT_NOTIFICATION = "A new Event has been added!";
    public static final String PRIORITY_HIGH = "high";
    public static final String PATH_TOPIC = "/topics/" + TOPIC_EVENTS;
    public static final String REF_DATE = "date";
    public static final String REF_INVERTED_DATE = "invertedDate";
    public static final String REF_ADMIN = "admin";
    public static final String REF_USER_NAME = "username";
    public static final String FORMAT_DAY = "EEE";
    public static final String FORMAT_TIME = "HH:mm a";
    public static final String REF_PURCHASES = "purchases";
    public static final String REF_ITEM_PURCHASES = "item_purchases";
    public static final String REF_EVENTS_PURCHASES = "events_purchases";
    public static final String REF_STATISTICS = "statistics";
    public static final int TYPE_EVENT = 1;
    public static final int TYPE_PRODUCT = 2;
    public static final int TYPE_NEWS = 3;
    public static final int TYPE_HISTORY = 4;
    public static final int TYPE_PROFILE_BILLING = 5;
    public static final String REF_CLIENTS = "clients";
    public static final String REF_ANDROID = "android";
    public static final String REF_VERSION = "version";
    public static final String REF_APP_VERSION = "AppVersion";
    public static final String REF_URL = "url";
    public static final String REF_CODE = "code";
    public static final String ANALYTICS = "analytics";
    public static final String CASH = "CASH";

    public static final String TOPIC_ANDROID = "/topics/events";
    public static final String TOPIC_IOS = "/topics/events-ios";
    public static final String NOTIFICATIONS = "Notifications";
    public static final String NOTIFICATION_AVAILABLE = "NOTIFICATION_AVAILABLE";
    public static final String REF_CARS = "Cars";
    public static final String EXPIRY_DATE = "EXPIRY_DATE";
    public static final String USER_SUBSCRIBED = "USER_SUBSCRIBED";
    public static final String IS_ADMIN = "IS_ADMIN";
    public static final String EXTRA_IS_EDIT = "EXTRA_IS_EDIT";
    public static final String EXTRA_EVENT = "EXTRA_EVENT";
}
