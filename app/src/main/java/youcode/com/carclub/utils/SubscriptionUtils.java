package youcode.com.carclub.utils;

import java.util.Calendar;

/**
 * Created by Akif sabeh on 11/30/2018.
 */
public class SubscriptionUtils {

    public static boolean isSubscriptionExpired(long date) {
        Calendar subscriptionCalendar = Calendar.getInstance();
        subscriptionCalendar.setTimeInMillis(date);

        int subscriptionYear = subscriptionCalendar.get(Calendar.YEAR);

        Calendar nowCalendar = Calendar.getInstance();

        int nowYear = nowCalendar.get(Calendar.YEAR);

        return subscriptionYear < nowYear;

    }

    public static boolean canPayForSubscription(long date) {
        Calendar subscriptionCalendar = Calendar.getInstance();
        subscriptionCalendar.setTimeInMillis(date);

        int subscriptionYear = subscriptionCalendar.get(Calendar.YEAR);

        Calendar nowCalendar = Calendar.getInstance();

        int nowYear = nowCalendar.get(Calendar.YEAR);

        return subscriptionYear <= nowYear;

    }

    public static long findSubscriptionDate(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.add(Calendar.YEAR, 1);
        calendar.set(Calendar.MONTH, 10);
        calendar.set(Calendar.DAY_OF_MONTH, 30);
        return calendar.getTimeInMillis();
    }

    public static long findClosestYear() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 10);
        calendar.set(Calendar.DAY_OF_MONTH, 30);
        return calendar.getTimeInMillis();
    }
}
