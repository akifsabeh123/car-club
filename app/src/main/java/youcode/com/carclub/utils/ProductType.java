package youcode.com.carclub.utils;

/**
 * Created by Akif sabeh on 11/30/2018.
 */
public enum ProductType {
    SUBSCRIPTION,
    EVENT,
    PRODUCT
}
