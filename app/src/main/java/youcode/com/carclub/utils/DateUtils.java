package youcode.com.carclub.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtils {


    public static final String LONG_DATE_1 = "dd-MM-yyyy";
    public static final String LONG_DATE_SLASH = "dd/MM/yyyy";
    public static final String LONG_DATE_2 = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String LONG_DATE_3 = "EEEE dd MMMM',' yyyy 'at' hh:mm a";
    public static final String LONG_DATE_6 = "EEEE dd MMMM',' yyyy 'at' hh:mm";
    public static final String LONG_DATE_4 = "EEEE dd MMMM',' yyyy";
    public static final String LONG_DATE_5 = "HH:mm";


    public static String formatTime(long timeInMillis, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        return dateFormat.format(timeInMillis);
    }


    public static String formatTime(String strCurrentDate, String strCurrentFormat, String outputFormat) {
        if (strCurrentDate == null || strCurrentDate.trim().isEmpty())
            return "";

        SimpleDateFormat sdf = new SimpleDateFormat(strCurrentFormat);
        SimpleDateFormat output = new SimpleDateFormat(outputFormat);
        Date d = null;
        try {
            d = sdf.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        String formattedTime = output.format(d);
        return formattedTime;

    }

    public static long convertStringDateToMilli(String dateString, String format) {
        long startDate = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = sdf.parse(dateString);

            startDate = date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public static long convertStringDateToMilliNoTimeZone(String dateString, String format) {
        long startDate = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date date = sdf.parse(dateString);

            startDate = date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public static long calculateDaysBetweenTwoDates(long date1, long date2) {
        long msDiff = date1 - date2;
        return 120 - TimeUnit.MILLISECONDS.toDays(msDiff);
    }


}
