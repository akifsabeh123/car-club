package youcode.com.carclub;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import youcode.com.carclub.models.SpinnerData;

/**
 * Created by Desk1 on 6/16/2017.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<SpinnerData> {
    int groupid;
    Context context;
    ArrayList<SpinnerData> list;
    LayoutInflater inflater;

    public CustomSpinnerAdapter(Context context, int groupid, int id, ArrayList<SpinnerData>
            list) {
        super(context, id, list);
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid = groupid;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupid, parent, false);
        ImageView img = itemView.findViewById(R.id.img);
        GradientDrawable bgShape = (GradientDrawable) img.getBackground().getCurrent();
        bgShape.setColor(Color.parseColor(list.get(position).getColor()));
        TextView textView = itemView.findViewById(R.id.txt);
        textView.setText(list.get(position).getText());


        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup
            parent) {
        return getView(position, convertView, parent);

    }
}
