package youcode.com.carclub.application;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.google.firebase.database.FirebaseDatabase;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by user on 8/23/2017.
 */

public class ApplicationClass extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        MultiDex.install(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

}
